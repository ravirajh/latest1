//USEUNIT ApplicationFunction
//USEUNIT FunctionLibrary
//USEUNIT GlobalVariables
//USEUNIT Reports
//USEUNIT ExcelUtil
//USEUNIT PDAUtil 

function Driver(){ 
  try{
    Indicator.Hide(); //Hide testcomplete indicator while execution 
    //Retrieve data from Config file
    var sFilePath      =  g_sProjPath+"\Stores\\Files\\Config.xml" //Config path
    var sResultPath      =  g_sProjPath+"\Stores\\Files\\TCResult.txt"
    var strResult = "Failed"
    KillExcel(); //Function call to kill excel processes
    var ConfigArry = new Array();
    var ReturnValue = "failed";
    ReadConfigXml(sFilePath) //Read data from xml file
    g_strAppname      =  GetDicParam("ApplicationName"); //Application Name
    g_strWebAppURL    =  GetDicParam("WebApplicationURL"); //Web Application URL
    g_strBrowserName  =  GetDicParam("BrowserName"); //Browser Name 
    g_mailURL         =  GetDicParam("MailURL");
    g_strBrowser =  GetBrowserName(g_strBrowserName); //Get Test Complete specific Browser name
    g_DBUserName      =  trim(GetDicParam("DBUserName")); 
    g_DBPassword      =  trim(GetDicParam("DBPassword")); 
    g_DBServiceName = GetDicParam("DBServiceName"); 
    g_DBPort = GetDicParam("DBPort");
    g_DBHost = GetDicParam("DBHost");
    g_DBTablename = GetDicParam("DBTable");
    g_strTestSuiteName = Trim(GetDicParam("TestSuiteName"));
    g_strRegion = Trim(GetDicParam("Region"));
    g_strModule = Trim(GetDicParam("Module"));
    g_strExecutionMode = Trim(GetDicParam("ExecutionMode"));
    g_ExcelScriptReadMode = Trim(GetDicParam("ExcelScriptReadMode")); //Get test scripts read mode either 'GROUP' or 'SINGLE'
    g_strFrameWorkChkSum = GetDicParam("FrameworkChecksum");
    g_CreateZipForTestResults = GetDicParam("CreateZipForTestResults");
    getbrowser_OSversion();
    fnOpenHtmlFile(g_strAppname); //Open HTML file
    
    //Open TestInventory file
    g_objXLApp =  Sys.OleObject("Excel.Application"); //create object for excel application
    g_objXLApp.DisplayAlerts = false; //Excel will not display alerts  
    g_objXLApp.WindowState = -4137 //Window state Maximized
    g_objXLApp.WindowState = -4140 //Window state minimized
    g_objXLApp.Visible = true;  //Making excel visible/invisible    
    g_objInventoryBook =  g_objXLApp.WorkBooks.Open(g_sTestDataPath) ;
    g_objInventorySheet  =  g_objInventoryBook.Sheets("TestCases"); //Open TestCases sheet in excel workbook
      
    ReadObjectXml(); //Function call to read all the object from excel
    StartExcelUtil();   //Load necessary objects

    var intMaxRowCnt  =  g_objInventorySheet.UsedRange.Rows.Count; //Variable to store maximum used row count in excel sheet
    g_intColCnt     =  g_objInventorySheet.UsedRange.Columns.Count; //Variable to store maximum used column count in excel sheet
    Log.Link(g_sSummaryFilePath,"Execution Summary File");
    
    for (intRowIteration = 2; intRowIteration <= intMaxRowCnt; intRowIteration++){
      var intKeywordCounter = 0; //Variable to store the keyword count    
			g_sAutomationTestCase = g_objInventorySheet.cells(intRowIteration, c_intTestCaseIdCol).value; //Variable to store test case ID
      execute_status = g_objInventorySheet.cells(intRowIteration, c_intExecuteStatusCol).value; //Variable to store execute status
      g_sTestCaseName = g_objInventorySheet.cells(intRowIteration, c_intTestCaseNameCol).value; //Variable to store testcase name
      sTestCaseDesc = g_objInventorySheet.cells(intRowIteration, c_intTestCaseDescCol).value; //Variable to store testcase description
      g_intFailCnt = 0; //Variable to store fail count of keyword
      
      if (execute_status == "Y") {
        if (g_ExcelScriptReadMode.toUpperCase() == "GROUP") {
          g_objTestScriptBook = g_objInventoryBook;
          g_TestDataSheetName = Trim(g_objInventorySheet.cells(intRowIteration, c_intTestCaseNumCol).value); //Variable to store testdata sheet name
          g_objTestScriptSheet  =  g_objTestScriptBook.Sheets(g_TestDataSheetName); //Open TestCases sheet which is in the same workbook    
        }else{
  				g_sTestScript = Trim(g_sTestScriptPath + g_sAutomationTestCase +".xlsx");//Open TestCases sheet which is in the different workbook    
  				g_objTestScriptBook = g_objXLApp.WorkBooks.Open(g_sTestScript);
          g_TestDataSheetName = "Sheet1";
  				g_objTestScriptSheet = g_objTestScriptBook.Sheets(g_TestDataSheetName); //Open TestCases sheet in excel workbook
        }
        fnInsertSection (sTestCaseDesc); //Function call to insert Test Case Details in the detailed report
        Log.AppendFolder(g_sAutomationTestCase); //Create a Log for the test
        Log.Message("------- Started execution for ------- " + g_TestDataSheetName);        
        intMaxRowCnt_TC  =  g_objTestScriptSheet.UsedRange.Rows.Count+1; //Variable to store maximum used row count in excel sheet
        var iRowIndex;
        var sStepContent;
        for(iRowIndex = intMaxRowCnt_TC; iRowIndex > 1; iRowIndex--){
            sStepContent = "";
            sStepContent = sStepContent + ReadExcel(iRowIndex,c_intObjNameCol);
            sStepContent = sStepContent + ReadExcel(iRowIndex,c_intInputValCol);
            sStepContent = sStepContent + ReadExcel(iRowIndex,c_intActionCol);
            sStepContent = sStepContent + ReadExcel(iRowIndex,c_intInputVal1Col);
            sStepContent = sStepContent + ReadExcel(iRowIndex,c_intStepCol);
            if (Trim(sStepContent).length > 0){
              intMaxRowCnt_TC = iRowIndex +1;
              break;
            }
        }
        
        g_RunTimeData.RemoveAll() //Clear the RunTime Data stored in Previous Test
        var g_LastStepNo = ReadExcel(intMaxRowCnt_TC-1,c_intStepCol);  //Reads the step from c_intStepCol
        g_TotalSteps = GetStepNumber(g_LastStepNo);
        
        for (intRowIteration_TC = 2; intRowIteration_TC <= intMaxRowCnt_TC; intRowIteration_TC++){   
        
          strKeywordName = ReadExcel(intRowIteration_TC,1);
          if ((intKeywordCounter == 1) && (intRowIteration_TC == intMaxRowCnt_TC )) {
            g_intStrtRow = row_num
            g_intEndRow = intMaxRowCnt_TC-1;  
            eval(g_strFuncCall+"()");
            intKeywordCounter= 0
            i = row_num
          }
            
          if ((strKeywordName) != null && (strKeywordName) != "") { 
            intKeywordCounter = intKeywordCounter + 1
            if (intKeywordCounter == 1) {
              row_num = intRowIteration_TC
              g_strFuncCall = strKeywordName 
            }
            if (intKeywordCounter == 2) {
              lastrow_num = intRowIteration_TC
              keyword1 = strKeywordName
            }
            if ((intKeywordCounter == 2) && (g_strFuncCall != null)) {
              g_intStrtRow = row_num 
              g_intEndRow =  lastrow_num - 1
              intRowIteration_TC = g_intEndRow
              eval(g_strFuncCall+"()");
              intKeywordCounter = 0
            }
          } 
             
        }//Keyword Loop ends
        
        fnInsertSummaryResult ();
        fnCloseHtml(); 
        CloseAllBrowsers();
        Log.Message("------- Ended execution for ------- " + g_TestDataSheetName);
        Log.PopLogFolder();
        Log.PopLogFolder();//popup twice.
        
        if (g_ExcelScriptReadMode.toUpperCase() != "GROUP") {
            g_objTestScriptBook.Close();    
        }
                
      }  
  
    } //end of test case for loop       

    if (g_strExecutionMode.toUpperCase() == "BATCH"){
        CopyFolders(g_sResultsPath,g_sProjPath) 
    }
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{
      fnCloseSummaryHtml(); //Close HTML Summary file      
      CloseAllBrowsers();
      if (g_iTestcaseStatusFail_Count == 0){ 
        ReturnValue = "Passed";                   
        strResult = "Passed";
        WriteResultData(sResultPath, strResult);
      }else{
        ReturnValue = "Failed";
        strResult = "Failed";
        WriteResultData(sResultPath, strResult);
      }
      StopExcelUtil();
      KillExcel(); //Function call to kill excel processes  
  }
    return ReturnValue;
} 
     
    
  

