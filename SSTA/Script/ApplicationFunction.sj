//USEUNIT FunctionLibrary
//USEUNIT GlobalVariables
//USEUNIT Reports
//USEUNIT ExcelUtil 

//*************************************************************************************************
// Function Name        : Login
// Function Description : To login to the ShareSource portal                   
// Inputs               : Username,Password      
// Returns              : None
//*************************************************************************************************
function Login(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strUserName ;      //Variable for storing UserName
    var strStep = "";      //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var strLoginUser;
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        if(intIterCnt == intIniRwNo){
          strInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);  //Reads the input value from c_intInputValCol
          strLoginUser = strInputValue1 + "(" +strInputValue + ")";
        }
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);  
      }
      if (ContainsText(strAction.toUpperCase(),"CLICK")){
          WaitForPageSync();
          Wait(3);
          AcceptTermsAndConditions(); 
     }
    }       
  }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }finally{
    if (intStepCounter>0){
      Log.Error("Login was not successful.");
      StopCurrentTest();
    }
    g_stepnum = strStep;    
 }
}
//*************************************************************************************************
// Function Name        : Logout
// Function Description : To logout of the application                   
// Inputs               : None     
// Returns              : None
//*************************************************************************************************
function Logout(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";      //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol 
        oPageObject = WaitForObject(g_PageObject, true);         
        ConfirmWindow(oPageObject, true);//To click on Confirm window ok button
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
        ConfirmWindow(oPageObject, true);//To click on Confirm window ok button          
      }
    }  
    g_stepnum = strStep;
 }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
} 

//*************************************************************************************************
// Function Name        : ClickBtn 
// Function Description : To click on link,button and image                 
// Inputs               : None  
// Returns              : None     
//************************************************************************************************
function ClickBtn(){
  try{
    var intIniRwNo ;       //Variable for storing initial row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strStep = "";           //Variable for storing step number
    var strInputValue ;    //Variable for storing input value 
    var strAction;         //Variable for storing strAction 
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var objnotexist = 0;   //Variable for storing whether the object exists or not
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword       
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    if (strObjectName != null){
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name    
      strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strAction = ReadExcel(intIniRwNo,c_intActionCol);  //Reads the action from c_intActionCol   
      strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel 
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
    }    
    
      if ((intStepCounter == 0)&&(objnotexist==0)) {
        Log.Message(strStep + " - " + objName + " clicked successfully.");
        fnInsertResult ("ClickBtn ",strStep,strObjectName+" shall be clicked",strObjectName+" is clicked","PASS",strStepType);
      } 
      else{
        Log.Error("ClickBtn function is failed");
        fnInsertResult ("ClickBtn ",strStep,strObjectName +" shall be clicked",strObjectName+" is not clicked","FAIL",strStepType);
        g_intFailCnt = g_intFailCnt + 1;
      }  
         
      g_stepnum = strStep;
     }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//'*************************************************************************************************
//' Function Name        : AccountProtection
//' Function Description : To verify Account protection information in Terms and Conditions screen                 
//' Inputs               : None
//' Returns              : None            
//'*************************************************************************************************
function AccountProtection(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
      
      strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol   
      strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
      strAction = ReadExcel(intIniRwNo,c_intActionCol);  //Reads the action from c_intActionCol  
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));        //Build object 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep); 
      }   
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{
    g_stepnum = strStep;
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
  } 
}

//*************************************************************************************************
// Function Name        : PrimaryDeviceProgram
// Function Description : To Verify whether a paricular device program is primary device program             
// Inputs               : Device Program name  
// Returns              : None      
//************************************************************************************************
function PrimaryDeviceProgram(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var strObjectName;     //Variable for storing object name 
    var strInputValue;     //Variable for storing input value  
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    var objnotexist = 0;
      if (strObjectName != null){     
        strInputValue = ReadExcel(intIniRwNo, c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol
        strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
        ObjName = BuildWebObj(g_dicColNames_obj.item(strObjectName));       
        var DPTableObject = WaitForObject(ObjName, true);               
        for (intRow = 2; intRow <= 5; intRow++){   
           // Compare the input value with the call data      
           if (aqString.Compare(trim(DPTableObject.Cell(intRow, 1).contentText),trim(strInputValue), false) == 0){
             var intRowIndex = DPTableObject.Cell(intRow, 1).RowIndex  ;  //Get the rowindex of the given device program name                               
             var strCheckPrimary = aqString.Compare(trim(DPTableObject.Cell(intRowIndex, 4).TextNode(0).contentText),trim("Primary"), false) == 0; //Variable to store Primary text exists
             var strCheckActive = aqString.Compare(trim(DPTableObject.Cell(intRowIndex, 4).TextNode(0).contentText),trim("Active"), false) == 0; //Variable to store Primary text exists
             var strCheckEdit = aqString.Compare(trim(DPTableObject.Cell(intRowIndex, 4).Link(1).contentText), "Edit",false) == 0;  //Variable to store Edit link exists
             var strCheckView = aqString.Compare(trim(DPTableObject.Cell(intRowIndex, 4).Link(0).contentText),"View",false) == 0;  //Variable to store View link exists
             if (strCheckEdit && strCheckView && (strCheckActive || strCheckPrimary)){
               intStepCounter = 0;
               break;
             }                         
             }           
            }
          }
     
    //Result generation
      if ((intStepCounter == 0)&&(objnotexist==0)) {
        strLogMessage = strStep +" - "+strInputValue+" is the Primary device program"
        Log.Message  (strLogMessage)
        //insert result
        fnInsertResult ("PrimaryDeviceProgram",strStep,""+strInputValue + "shall be the primary device program,Edit/View shall be enabled,Primary shall be disabled ",""+strInputValue + " is the primary device program ,Edit/View is enabled,Primary is disabled ","PASS",strStepType);
      } 
      else{
        strLogMessage = strStep +" - "+strInputValue+" is not the Primary device program"
        Log.Message  (strLogMessage)
        fnInsertResult ("PrimaryDeviceProgram",strStep,""+strInputValue + "shall be the primary device program,Edit/View shall be enabled,Primary shall be disabled ",""+strInputValue + "is not the primary device program ,Edit/View is not enabled,Primary is enabled","FAIL",strStepType);
        g_intFailCnt = g_intFailCnt + 1;    
      }    
    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  } 
}

//*************************************************************************************************
// Function Name        : PrimaryMenuNavigation
// Function Description : Navigate to Primary menu               
// Inputs               : Primary menu name  
// Returns              : None        
//************************************************************************************************
function PrimaryMenuNavigation(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    objnotexist = 0;   //Variable to verify if the object exist
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var StepStatus = "FAIL";
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    var ObjectArry = new Array();
    var strStepType;    // variable for storing Step type
    
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
    strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol        
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType        
    ObjectArry = BuildWebObj(g_dicColNames_obj.item(strObjectName));  
    var ExpectedResult = "PrimaryMenuNavigation to "+strInputValue+" shall be successful";
    var ActualResult = "PrimaryMenuNavigation to "+strInputValue+" is not successful";
    var Object = WaitForObject(ObjectArry, true);      
      if(IsExists(Object)){
          ChildObj = Object.FindAllChildren("objectType","Link",50);  //Find all the child objects of the type Link
          ChildObj = new VBArray(ChildObj).toArray();
          for (intChildCnt = 0 ; intChildCnt <= ChildObj.length ; intChildCnt++){ 
             if (aqString.Compare((ChildObj[intChildCnt].ContentText), strInputValue, false) == 0){
              ChildObj[intChildCnt].Click();    //Click on the primary menu
              oPageObject = WaitForObject(g_PageObject, true);
              ConfirmWindow(oPageObject, true);//To click on Confirm window ok button                   
              WaitForPageSync();//To wait for the page to load
              Wait(3);
              WaitForLoadingSpinner();
              intStepCounter=0;        
              break;
             }
          } 
      }else{
         Log.Message("Object does not exist");
      }
   
      //  Result Generation   
      if ((intStepCounter == 0) &&(objnotexist==0)){
        strLogMessage = strStep +" - User is navigated to "+strInputValue+" primary menu";
        Log.Message(strLogMessage);
        StepStatus = "PASS";
        ActualResult = "PrimaryMenuNavigation to "+strInputValue+" is successful";                
      }else{
        strLogMessage = strStep +" - User is not navigated to "+strInputValue+"";
        Log.Message(strLogMessage);
        g_intFailCnt = g_intFailCnt + 1;
      }
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    ActualResult=g_strFuncCall+" failed due to the exception "+e.description
  }finally{
    if(StepStatus == "FAIL"){      
      StopCurrentTest();
    }
  }
    fnInsertResult("PrimaryMenuNavigation",strStep,ExpectedResult,ActualResult,StepStatus,strStepType);
    g_stepnum = strStep; 
    return intStepCounter;
}

          
//*************************************************************************************************
// Function Name        : SecondaryMenuNavigation
// Function Description : To Navigate to any functionmenu                  
// Inputs               : Secondaru menu name     
// Returns              : None
//*************************************************************************************************
function SecondaryMenuNavigation(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing Action
    var intStepCounter = 1;    //Variable for storing flag
    var objnotexist = 0;   //Variable to verify if the object exist
    var strStepType;         //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
    //Read object name from excel 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);
    //Read browser input from excel
    strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);
    strAction = ReadExcel(intIniRwNo,c_intActionCol);
    //Read strStep from excel
    strStep = ReadExcel(intIniRwNo,c_intStepCol);     
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    //Build object
    ArryObject = BuildWebObj(g_dicColNames_obj.item(strObjectName));
    var Object = WaitForObject(ArryObject, true);    
    if (IsExists(Object)){
      ChildObj = Object.FindAllChildren("objectType","Link",50);  //Find all the child objects of the type Link
      ChildObj = new VBArray(ChildObj).toArray();
      for (intCounter = 0; intCounter <= ChildObj.length;intCounter++){  
        //Check the secondary menu name which is given as inputvalue            
        if (aqString.Compare((ChildObj[intCounter].contentText), strInputValue, false) == 0){
          ChildObj[intCounter].Click();   //Click on the secondary menu 
          intStepCounter=0;      
          Wait(3);                        
          break;               
        }
      }     
            
      oPageObject = WaitForObject(g_PageObject, true);
      ConfirmWindow(oPageObject, true);//To click on Confirm window ok button 
       Wait(3);
       WaitForPageSync(); 
    }
    else{
      Log.Message("Object does not exist");
      intStepCounter = 1;         
    }
     Wait(3);
     var Expected = ReadExcel(intIniRwNo,c_VerifnPointExpected);  //Read expected result from excel
    
      if ((intStepCounter == 0) &&(objnotexist==0)){
      if(aqString.Find(Expected,"Forbidden")!= -1) {
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo);
      }  
        else{ 
        strLogMessage = strStep +" - User is navigated to "+strInputValue+"";
        Log.Message(strLogMessage);
        //insert result
        fnInsertResult("SecondaryMenuNavigation",strStep,"SecondaryMenuNavigation to "+strInputValue+" shall be successful","SecondaryMenuNavigation to "+strInputValue+" is successful","PASS",strStepType);    
      }     
      }     
      else{
        strLogMessage = strStep +" - User is not navigated to "+strInputValue+"";
        Log.Message(strLogMessage);
        fnInsertResult("SecondaryMenuNavigation",strStep,"SecondaryMenuNavigation to "+strInputValue+" shall be successful","SecondaryMenuNavigation to "+strInputValue+" is not successful","FAIL",strStepType);
        g_intFailCnt = g_intFailCnt + 1;
      }
     g_stepnum = strStep;
     return intStepCounter;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//************************************************************************************************
// Function Name        : ViewDeviceSetting
// Function Description : To click on ViewDeviceSettings link and navigate to device settings page               
// Inputs               : None     
// Returns              : None
//**************************************************************************************************
function ViewDeviceSetting(){
  try{
    var intIniRwNo;       //Variable for storing initial row number
    var intEndRwNo;       //Variable for storing end row number
    var strObjectName;    //Variable for storing object name
    var objName;          //Variable for storing object 
    var strInputValue;    //Variable for storing input value 
    var strAction;        //Variable for storing action 
    var strUserName;      //Variable for storing UserName
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)     
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword  
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);//Reads input value from c_intInputValCol 
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol  
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol    
        if (strObjectName != null){
          //Build object
          VerificationObj = BuildWebObj(g_dicColNames_obj.item(strObjectName));        
        }         
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,VerificationObj,strInputValue,strObjectName,strStep); 
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      } 
     g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
} 
//*************************************************************************************************
// Function Name        : SearchOrAddPatient
// Function Description : To search Patient a patient if not exists then create a new patient               
// Inputs               : Patient Name,New patient details if patient does not exist      
// Returns              : None
//**************************************************************************************************
function SearchOrAddPatient(){
  try{
    var intIniRwNo;       //Variable for storing initial row number
    var intEndRwNo;       //Variable for storing end row number
    var strObjectName;    //Variable for storing object name
    var objName;          //Variable for storing object 
    var strInputValue;    //Variable for storing input value 
    var strAction;        //Variable for storing action 
    var intStepCounter = 1  //Variable for storing pass/failcounter for search patient
    var TotalStepCounter = 0;  //Variable for storing result for total number of steps in the keyword

    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    var ExistingPatientName = ReadExcel(intIniRwNo,c_intInputValCol);
        for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
            strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
            strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value rom c_intInputValCol    
            strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
            strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol              
            strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
            
            if (strObjectName != null){     
                objName = BuildWebObj(g_dicColNames_obj.item(strObjectName)); //Build object
               if (CompareText(strObjectName,"PatientSearch_SearchPatientName")){
                    ExpectedResult = strInputValue + " shall be displayed in the Search Result if it is present.";
                    objName = WaitForObject(objName, false);
                    ObjectText = GetText(objName);
                    if (CompareText(ObjectText,strInputValue)){
                        intStepCounter = 0;
                        StepStatus = "PASS";
                        intIterCnt = intEndRwNo; //Need not process any more rows
                        ActualResult = strInputValue + " is displayed in the Search Result";
                        WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheetName,ExistingPatientName);  //If the patient name is existing,write the patient name in test data sheet
                    }else if (!IsNullorUndefined(objName)){
                        //Either some other 'Some other Patient' or 'No Record found' message is displayed.
                        intStepCounter = 0;//Making it Pass and proceed with Add Patient
                        StepStatus = "PASS";
                        ActualResult = strInputValue + " is not displayed in the Search Result, so creating a new Patient.";
                    }else{
                        intStepCounter = 1;
                        StepStatus = "FAIL";;
                        ActualResult = "Patient Search Result is not displayed"
                    }
                    strLogMessage = strStep +" - "+ActualResult;
                    if(intStepCounter == 0){
                      Log.Message(strLogMessage);
                    } else {
                      Log.Error(strLogMessage);
                    } 
                    fnInsertResult(g_strFuncCall,strStep,ExpectedResult,ActualResult,StepStatus,strStepType);
                    
                }else{
                    if(ContainsText(strObjectName,"PrimaryMenu")){  //If the object name contains the text "Primary" call PrimaryMenuNavigation
                        g_intStrtRow = intIterCnt;            
                        intStepCounter = PrimaryMenuNavigation();
                    }else if (ContainsText(strObjectName,"SecondaryMenu")) {  //If the object name contains the text "SubMenu" call PrimaryMenuNavigation
                        g_intStrtRow = intIterCnt;
                        intStepCounter = SecondaryMenuNavigation();
                    }else{
                        if(ContainsText(strObjectName,"AddPatient_FirstName")) {  //If the object name is AddPatient_FirstName write the generated new patient name in excel sheet
                          WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheetName,strInputValue); //Write Patient First name in the first row of the Keyword.
                        } 
                    
                    //This function performs the strActions specified in the excel 
                    intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
                    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
                  } 
                }
                
                TotalStepCounter = TotalStepCounter + intStepCounter; //Add the intStepCounter for all steps 
            }
            }
      
      //After completing write the patient details back to Excel.      
      WriteBaxterpatientId(intEndRwNo);//Write Baxter Patient ID in the last row of the Keyword.
   }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   }finally{
      UpdateSummaryStatus(TotalStepCounter) ;
      g_stepnum = strStep;
   } 
}

//*************************************************************************************************
// Function Name        : SelectDevice
// Function Description : To Select a device(Vivia1.0/Vivia2.0)                   
// Inputs               : Device name     
// Returns              : None
//*************************************************************************************************
function SelectDevice(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";       //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var TotalStepCounter = 0;  //Variable for storing result for total number of steps in the keyword
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= (intEndRwNo - 1); intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      //Build object
       var ObjDeviceTable = Sys.Browser(g_strBrowser).Page("*").FindChild("idStr","programmedDevicesTable",100);
       if(intIterCnt ==  intIniRwNo + 1){
          if(IsExists(ObjDeviceTable)){
             intIterCnt = intEndRwNo;
          } 
       }  
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      //Build object
      VerificationObj = BuildWebObj(g_dicColNames_obj.item(strObjectName));
      strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol      
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol    
      //This function performs the strActions specified in the excel
      
      intStepCounter = DataEntry(strAction,VerificationObj,strInputValue,strObjectName,strStep);        
      
      //After clicking on go button, verify if the PAC is displayed on the screen
      if(intIterCnt == intEndRwNo || intIterCnt == intEndRwNo-1) {
           intStepCounter = 1;
           ExpectedMessage = "Go button shall be clicked and PAC shall be displayed on the screen";
           ActualMessage = "Go button is clicked and PAC is not displayed on the screen";  
           isPACFound = WritePatientActivationCode(intEndRwNo);
           if (isPACFound){
              intStepCounter = 0;  
              ActualMessage = "Go button is clicked and PAC is displayed on the screen";  
           } 
      }     
      TotalStepCounter = TotalStepCounter + intStepCounter; //Add the intStepCounter for all steps  
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt); 
    }  
  }
  catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{
    UpdateSummaryStatus(TotalStepCounter) ;
    g_stepnum = strStep;
  } 
}
//*************************************************************************************************
// Function Name        : AddOrUpdateSetting
// Function Description : To update/create/view Device Program/System Settings/Patient Settings as per given input data                  
// Inputs               : Device Program/System Settings/Patient Settings values       
// Returns              : None  
//************************************************************************************************
function AddOrUpdateSetting(){ 
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Read object name from excel 
      g_strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (g_strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(g_strObjectName));  //Build object from the given object name  
        if (aqString.Find(g_strObjectName,"&&")!= -1){
          objName = BuildWebObj(g_strObjectName);  //Build object from the given object name
        }
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        //Read strStep from excel
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
        //Read strAction from excel
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
        if (((aqString.Find(g_strObjectName,"PIN")) != -1)||((aqString.Find(g_strObjectName,"Clncl_ADP_DevPgmNme")) != -1)){  //If the object name contains the text "PIN" 
          WriteExcel(intIterCnt,c_intInputVal1Col,g_TestDataSheetName,strInputValue)  //Write the randomly generated PIN to the strInputValue1 column
        }   
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,g_strObjectName,strStep);
        var SettingsPopupNo = BuildWebObj(g_dicColNames_obj.item("CancelPopUpYesBtn"));
        var Object = WaitForObject(SettingsPopupNo, false);
        if ((IsExists(Object))&&(Object.VisibleOnScreen)){
          Object.Click();
        }    
        var SettingsPopupNo1= BuildWebObj(g_dicColNames_obj.item("Clncl_ADP_CrteWD_Yes"));
        var Object = WaitForObject(SettingsPopupNo1, false);
        if ((IsExists(Object))&&(Object.VisibleOnScreen)){
          Object.Click();
        }   
	      var PatientEditablePopUp = BuildWebObj(g_dicColNames_obj.item("PopUpYesBtn"));
        Object = WaitForObject(PatientEditablePopUp, false);
        if ((IsExists(Object))&&(Object.VisibleOnScreen)){
          Object.Click();
        } 
    
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
        g_strObjectName = "";
      }
    } 
    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : VerifyLabel
// Function Description : To Verify labels on a particular page               
// Inputs               : Labels to Verify   
// Returns              : None       
//************************************************************************************************
function VerifyLabel(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    //var strStepType;         //variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        //Read input value from excel
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        //Read strStep from excel
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
        //Read strAction from excel
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol  
        //This function performs the strActions specified in the excel
        
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
	
	
      }
    }  
    g_stepnum = strStep;
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : AddOrCreateProgram
// Function Description : To click on Add Programme/Create New link of Device Program/System Settings/Patient Settings               
// Inputs               : None  
// Returns              : None        
//************************************************************************************************
function AddOrCreateProgram(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var objnotexist = 0;   //Variable to verify if the object exist
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol  
    if (strObjectName != null){        
      //Read input value from excel
      strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol    
      //Read strStep from excel
      strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType    
      
      //Build object
      for (intRow = 2; intRow <= 5; intRow++){    
      var strLink  = BuildWebObj(g_dicColNames_obj.item(strObjectName))+".Cell("+ intRow +", 1).Link(0)";          
      var objAddProgramLink = WaitForObject(strLink, false);   //Get add program link in the tabl
       if(IsExists(objAddProgramLink)){
        //Check if the link name is Add Program          
        if (aqString.Compare((objAddProgramLink.ContentText),strInputValue, false) == 0){
          objAddProgramLink.Click();    //Click the Add Program link 
          intStepCounter = 0; 
          break;       
         }
       } 
      }         
    }
    if((intStepCounter == 0) &&(objnotexist==0)){
      strLogMessage = strStep +" - "+strInputValue+" is Clicked";
      Log.Message(strLogMessage);
      fnInsertResult("AddOrCreateProgram",strStep,"User shall be able to click on Add Program/Create New Link sucessfully ","User is able to click on Add Program/Create New Link sucessfully ","PASS",strStepType);
    }
    else{
      strLogMessage = strStep +" - "+strInputValue+" is not Clicked"
      Log.Message(strLogMessage);
      fnInsertResult("AddOrCreateProgram",strStep,"User shall be able to click on Add Program/Create New Link sucessfully ","User is not able to click on Add Program/Create New Link sucessfully ","FAIL",strStepType);
      g_intFailCnt = g_intFailCnt + 1;
    }
    g_stepnum = strStep;
  }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : VerifyInactiveUser
// Function Description : To Verify whether a user is logged out of the application after 30 minutes of inactivity       
// Inputs               : User name/surname/Email   
// Returns              : None      
//************************************************************************************************
function VerifyInactiveUser() {
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number 
    var intStepCounter = 1;    //Variable for storing flag
    var objnotexist = 0;
    
    //Reads initial row and end row for(a keyword
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intIniRwNo ; intIterCnt++){
      //Read object name from excel 
     strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        //Build object 
       objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));
        //Read input value from excel
       strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
        //Read strAction from excel
       strAction = ReadExcel(intIniRwNo,c_intActionCol);  //Reads the action from c_intActionCol
        //Read strStep from excel
       strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
        //This function performs the strActions specified in the excel
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo);
     }
    }
    g_stepnum = strStep;
 }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//************************************************************************************************
// Function Name        : VerifyEmailSentConfirmation
// Function Description : Verify email sent confirmation message                   
// Inputs               : Confirmation message   
// Returns              : None         
//************************************************************************************************* 
function VerifyEmailSentConfirmation() {
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    //Click on Accept invalid SSL link if it appears on the screen
    AcceptInvalidSSL();  
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        Object = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        ObjectArray = Object.split("*");
        objName = ObjectArray[0] + g_dicColNames_obj.item(strObjectName);
        strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
        strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol    
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol      
        
        
        //This function perForms the strActions specIfied in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      } 
    } 
    
    g_stepnum = strStep;
  }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : LoginToWebmail
// Function Description : To login to the Webmail and click on the activation link                  
// Inputs               : Username,Password,SearchString and VerifyString     
// Returns              : None
//*************************************************************************************************
function LoginToWebmail() {
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var strtext;             //Variable for storing the text in the mail body 
    var URLToLaunch;            //Variable for storing the url present in the mail body
    var SearchKeyword;		   //Variable for storing username	
    var MailBodyObject;      //Variable for storing mailbody object
    var arrayfirst = new Array();       //Variable for storing array values in mailbody
    var arraysecond = new Array();      //Variable for storing array values in mailbody
  
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;   //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    Wait(2*60); //Wait for 2 minutes to email arrive
    
    //close existing process
    CloseProcess("iexplore");
    
    //Launch the URL
    var BrowserObject = Browsers.Item("iexplore");
    BrowserObject.Run(g_strWebAppURL);
    var BrowserProcess = Sys.WaitBrowser("iexplore");
    BrowserProcess.BrowserWindow(0).Maximize();    
    BrowserObject.Navigate(g_mailURL);
    g_TreatmentFileUpload = true; 
    SwitchBrowser("Secondary");     
    
    //Launch the url
    BuildObjectProperty = BuildWebObj(g_dicColNames_obj.item("Outlook_UserName"));
    for (RetryCount = 0 ; RetryCount < 5 ; RetryCount++){
        Sys.Refresh();
        UserNameObject = WaitForObject(BuildObjectProperty, false);
        if (IsExists(UserNameObject)){
            var OutlookUserNameValue = ReadExcel(intIniRwNo,c_intInputValCol);
            UserNameObject.Keys(OutlookUserNameValue);
            var OutlookNextButton = BuildWebObj(g_dicColNames_obj.item("Outlook_NextButton"));
            OutlookNextButton = WaitForObject(OutlookNextButton, true);
            OutlookNextButton.Click();
            Wait(3);
            WaitForPageSync();
            break;
        }else{
            SignOutFromMail();
            CloseProcess("iexplore"); // Closing the browser to log out completely from Office365
            BrowserObject.Run("about:blank");    
            BrowserProcess = Sys.WaitBrowser("iexplore")
            BrowserProcess.BrowserWindow(0).Maximize(); 
            BrowserObject.Navigate(g_strWebAppURL);   
            BrowserObject.Navigate(g_mailURL);  
        }
    }  
     
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
        strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol 
        if(intIterCnt == intIniRwNo + 5){
          SearchKeyword = strInputValue; 
        }
        if (ContainsText(strObjectName,"MailLogin_")){ //In Windows 10 machines, the page property is different.
            objName  = aqString.Replace(objName, "http", ""); //Changing Page("http*") to Page("*")
        } 
        
        //This function performs the strActions specified in the excel
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        if(ContainsText(strObjectName,"SearchBox")){
          Wait(1);
          //If not entered username in previous attempt
          SearchMailBox(SearchKeyword);
          
          //Select the first email, so its content is displayed in the reading pane.
          BuildObject = BuildWebObj(g_dicColNames_obj.item("FirstMail"));
          FirstMailObject = WaitForObject(BuildObject, true);
          FirstMailObject.Click();
          Wait(1);
          BuildObject = BuildWebObj(g_dicColNames_obj.item("MailBody"));
          MailBodyObject = WaitForObject(BuildObject, true); 
          strtext = GetText(MailBodyObject);
          Log.Message(strtext);
          CleanedTextFromEMail = aqString.ToLower(CleanUpText(strtext));
          SearchKeyword = aqString.Find(aqString.ToLower(CleanedTextFromEMail),aqString.ToLower(SearchKeyword))
          if((aqString.Find(strtext,"Use the password reset link listed below to login to Sharesource:") != -1) && (SearchKeyword != -1)){
            arrayfirst = strtext.split("Use the password reset link listed below to login to Sharesource:")
            arraysecond = arrayfirst[1].split("If you did not initiate this action or believe you are receiving this message in error")
            URLToLaunch = arraysecond[0];
            Log.Message(URLToLaunch);
            if (!ContainsText(URLToLaunch.toUpperCase(),g_strWebAppURL.toUpperCase())){
                Log.Error("URL is pointing to different environment");
                URLToLaunch = null;  
            } 
            DeleteMail();
            SignOutFromMail();
            if(URLToLaunch != null){
              intStepCounter = 0;
            }else{
              ExpectedMessage = "Reset Password link shall be clicked after searching mail";
              ActualMessage = "Reset Password link is not clicked after searching mail";  
              Log.Error(ActualMessage);
              intStepCounter = 1;     
            }
          }else if((aqString.Find(strtext,"Welcome to Sharesource. Click the link below to activate your account:") != -1) && (SearchKeyword != -1)){
            arrayfirst = strtext.split("Welcome to Sharesource. Click the link below to activate your account:")
            arraysecond = arrayfirst[1].split("If you have any questions, please contact your")
            URLToLaunch = arraysecond[0];
            if (!ContainsText(URLToLaunch,g_strWebAppURL)){
                Log.Error("URL is pointing to different environment");
                URLToLaunch = null;  
            } 
            DeleteMail();
            SignOutFromMail();
            if(URLToLaunch != null){
              intStepCounter = 0;
            }else{
              ExpectedMessage = "Account Activation link shall be clicked after searching mail";
              ActualMessage = "Account Activation link is not clicked after searching mail";  
              Log.Error(ActualMessage);
              intStepCounter = 1;                  
            }
          }else{
            Log.Error("The required string in mail body does not exist")
            ExpectedMessage = "Reset Password/Account Activation link shall be clicked after searching mail";
            ActualMessage = "The message Use the password reset link listed below to login to Sharesource/click the link below to activate your account/the given username is not displayed in the mail body";
            intStepCounter = 1;   
          } 
        }
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);  
    }    
    g_stepnum = strStep;
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{
    g_TreatmentFileUpload = false; 
    SwitchBrowser("Primary"); 
    OpenPassswordResetURL(URLToLaunch); 
    if(g_strBrowser != "iexplore"){
        CloseProcess("iexplore"); 
    }
  } 
}
//*************************************************************************************************
// Function Name        : SearchMailBox
// Function Description : To verify 
// Inputs               : Object            
// Returns              : Returns true if object is enabled, false in other condition
//************************************************************************************************* 
function SearchMailBox(strUsername) {
    try {
        BuildObject = BuildWebObj(g_dicColNames_obj.item("Mail_SearchBox"));
        MailSearchBox = WaitForObject(BuildObject, true);
        var IsObjectHidden = !IsVisible(MailSearchBox,"SearchEmail");
        var IsKeywordNotEntered = !ContainsText(GetText(MailSearchBox),strUsername);
        //If the Search Keyword is not entered
        if (IsObjectHidden || IsKeywordNotEntered){
            BuildObject = BuildWebObj(g_dicColNames_obj.item("Mail_SearchBtn"));
            MailSearchButton = WaitForObject(BuildObject, true);
            if (IsExists(MailSearchButton)){
                MailSearchButton.Click();
            } 
            BuildObject = BuildWebObj(g_dicColNames_obj.item("Mail_SearchBox"));
            MailSearchBox = WaitForObject(BuildObject, true);
            if (IsExists(MailSearchButton)){
                MailSearchBox.SetText(strUsername);
                MailSearchBox.Keys ("[Enter]");
                Log.Warning("Search Keyword Re-Enetered")
            }
        }
    }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
}

//*************************************************************************************************
// Function Name        : ResetPassword
// Function Description : To reset the password of a clinic user                   
// Inputs               : None     
// Returns              : None
//*************************************************************************************************
function ResetPassword(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol    
        //This function performs the strActions specified in the excel 
        
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);      
        
      }
    }  
    g_stepnum = strStep;
  }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : ResetPasswordFromMail
// Function Description : To reset password by clicking on reset password link from the mailbox/Reset Password Link                      
// Inputs               : None     
// Returns              : None
//*************************************************************************************************
function ResetPasswordFromMail(){
  try{
    Log.Message("Starting - ResetPasswordFromMail");
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    //Click on Accept invalid SSL link if it appears on the screen
    AcceptInvalidSSL();  
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
        if (strObjectName == "LogoutLink"){
          Object = BuildWebObj(g_dicColNames_obj.item(strObjectName));  
        }
        else{
          ObjName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
          ObjectArray = ObjName.split("*");
          Object = ObjectArray[0] + g_dicColNames_obj.item(strObjectName);               
        }      
        if(intIterCnt == intIniRwNo){
          var strGeneratedPwd = strInputValue
          WriteExcel(intIterCnt,c_intInputVal1Col,g_TestDataSheetName,strGeneratedPwd)
        }            
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol   
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,Object,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      }
    } 
    AcceptTermsAndConditions();
    g_stepnum = strStep; 
  }catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }finally{
   Log.Message("Ending - ResetPasswordFromMail");
 } 
}

//*************************************************************************************************
// Function Name        : SearchUser
// Function Description : To Search a user with name/surname/email                                  
// Inputs               : Name/surname/email of the user     
// Returns              : None
//*************************************************************************************************
function SearchUser(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol    
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);     
      }
    }  
    
    g_stepnum = strStep;
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : VerificationPoint
// Function Description : To check whether step number given in the testdata sheet is verification point and insert the step in html report           
// Inputs               : Function name, step number, stepcount and rownumber
// Returns              : None         
//************************************************************************************************ 
function VerificationPoint(g_strFuncCall,StepNumber,intStepCount,rowNumber){
  
	var Expected;
	var Actual;
	var StepType;
	var CurrentRowNumber = rowNumber;
	try{

		if(g_RowNumObjNotFound > 0){
			CurrentRowNumber = g_RowNumObjNotFound;
		}
		StepType = trim(ReadExcel(CurrentRowNumber,c_intStepType)); //Reads the step type from c_intStepType

		//Check if the step number has VP
		if(aqString.Find(StepType,"Proving")==0){    
			Expected = ReadExcel(CurrentRowNumber,c_VerifnPointExpected);  //Read expected result from excel
			Actual = ReadExcel(CurrentRowNumber,c_VerifnPointActual);  //Read actual pass result from excel   
			if (!ContainsText(Actual,"&")){ //Actual should always contain &
				Log.Warning(StepNumber+" - Actual Result from excel is not correct.");
			}            
			if((intStepCount == 0) && (CurrentRowNumber < 1000)){
				fnInsertResult(g_strFuncCall,StepNumber,Expected,Actual,"PASS",StepType);//Insert result in html report
			}else{
				if(CurrentRowNumber >= 1000){
					Expected = ExpectedMessage;
					Actual = ActualMessage;
				}
				g_intFailCnt = g_intFailCnt + 1;
				fnInsertResult(g_strFuncCall,StepNumber,Expected,Actual,"FAIL",StepType);
			}
		}else if(CurrentRowNumber!=g_intEndRow) {
			if((intStepCount == 0) && (CurrentRowNumber < 1000)){
				fnInsertResult(g_strFuncCall,StepNumber,ExpectedMessage,ActualMessage,"PASS",StepType);
			}else{
				g_intFailCnt = g_intFailCnt + 1;
				fnInsertResult(g_strFuncCall,StepNumber,ExpectedMessage,ActualMessage,"FAIL",StepType);
			}    
		}else if(CurrentRowNumber==g_intEndRow) {
			Expected = ReadExcel(CurrentRowNumber,c_VerifnPointExpected);  //Read expected result from excel
			Actual = ReadExcel(CurrentRowNumber,c_VerifnPointActual);  //Read actual pass result from excel
			if (!IsEmpty(Actual) && !ContainsText(Actual,"&")){ //Actual should always contain &
				Log.Warning(StepNumber+" - Actual Result from excel is not correct.");
			}      
			if(IsEmpty(Expected)){
				Expected=ExpectedMessage;
			}
			if(IsEmpty(Actual)){
				Actual=ActualMessage;
			}
			if((intStepCount == 0) && (CurrentRowNumber < 1000)){
				fnInsertResult(g_strFuncCall,StepNumber,Expected,Actual,"PASS",StepType);
			}else{
				g_intFailCnt = g_intFailCnt + 1;
				fnInsertResult(g_strFuncCall,StepNumber,Expected,Actual,"FAIL",StepType);
			}
		}
		ExpectedMessage="";
		ActualMessage="";
	}catch(e){
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}     
}

//*************************************************************************************************
// Function Name        : FooterVerification
// Function Description : To Click / verify the Footer Links
// Inputs               : None     
// Returns              : None
//*************************************************************************************************

function FooterVerification(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strUserName ;      //Variable for storing UserName
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
        Sys.Keys("[Down]")
        Sys.Keys("[Down]")
        Sys.Keys("[Down]")
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt); 
             
      }
    }  
    
    g_stepnum = strStep; 
  }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }  
}

//*************************************************************************************************
// Function Name        : IdlesessionTime
// Function Description : To be idle for the specified duration                     
// Inputs               : None      
// Returns              : None
//*************************************************************************************************
function IdlesessionTime(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strInputValue;     //Variable for storing input value 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 0;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intIniRwNo ; intIterCnt++){           
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
      Delay(strInputValue);
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
    } 

    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }   
}

//*************************************************************************************************
// Function Name        : CloseTab
// Function Description : Closes the active tab of the browser when multiple tabs are open  
// Inputs               : None  
// Returns              : None  
//************************************************************************************************
function CloseTab(){
  var intIniRwNo;
  var strStep = "";
  try{
  var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;
    strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
    strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
    Sys.Browser(g_strBrowser).Page("*").Close();
    fnInsertResult("CloseTab",strStep,"Browser Tab shall be closed","Browser Tab is closed","PASS",strStepType);
    g_stepnum = strStep;
  }catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }  
}

//'*********************************************************************************************
//' Function Name        : DeleteMail
//' Function Description : To delete mail from inbox                   
//' Inputs               : None 
//' Returns              : None             
//'*********************************************************************************************
function DeleteMail(){
  try{
    var objName = BuildWebObj(g_dicColNames_obj.item("DeleteEmail"));  //Build object from the given object name  
    objName = WaitForObject(objName,true);
    objName.Click();//Click on Delete button to move the email out from the Inbox  
  }catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }  
}

//'*********************************************************************************************
//' Function Name        : SignOutFromMail
//' Function Description : To signout from mail                   
//' Inputs               : None 
//' Returns              : None             
//'*********************************************************************************************
function SignOutFromMail(){  
  try{
    //Click on the Profile Image
    var objProfileProperty = BuildWebObj(g_dicColNames_obj.item("SignoutProfileIcon"));  //Build object from the given object name  
    var objProfile = WaitForObject(objProfileProperty,true);
    if (IsNullorUndefined(objProfile)){//If the profile image is not displayed, wait and click again
      Wait(5);
      objProfile = WaitForObject(objProfileProperty,true);  
    }
    objProfile.Click();//Click on Office365 Profile icon
    Wait(1);
    
    //Click on the Signout Link
    var objSignoutProperty = BuildWebObj(g_dicColNames_obj.item("SignoutEmail"));  //Build object from the given object name  
    var objSignout = WaitForObject(objSignoutProperty,true);
    if (IsNullorUndefined(objSignout)){ // If Profile icon not clicked due to loading issue, click again
      Wait(5); // first time profile icon not clicked successfully, hence wait.
      objProfile = WaitForObject(objProfileProperty,true);  
      objProfile.Click();//Click on Office365 Profile icon
      Wait(1);
      objSignout = WaitForObject(objSignoutProperty,true);         
    }
    objSignout.Click();//Click on signout button 
    
  }catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }  
}

//*************************************************************************************************
// Function Name        : WriteBaxterpatientId
// Function Description : To write the created patient name and Baxter patient id in testdata sheet            
// Inputs               : None
// Returns              : None         
//************************************************************************************************ 
function WriteBaxterpatientId(intRow){
 try{
    //Build object
    objName = BuildWebObj(g_dicColNames_obj.item("PatientInfoTable"));  //Build object from the given object name
    var tableObj = WaitForObject(objName, false); 
    if (!IsExists(tableObj)){
      Log.Error("Unable to fetch Baxter Patient ID.")
    } 
    var strBaxterPID = "Baxter Patient ID: " + tableObj.Cell(1, 2).contentText;  //Get the Baxter patient ID
    WriteExcel(intRow,5,g_TestDataSheetName,strBaxterPID);  //Write Baxter patient id in testdata sheet
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}
//*************************************************************************************************
// Function Name        : WritePatientActivationCode
// Function Description : To write the  patient unique id in testdata sheet            
// Inputs               : None
// Returns              : None         
//************************************************************************************************ 
function WritePatientActivationCode(intRow){
 try{
    var PatientAct;
    var objName;  
    var ReturnValue = false;
    //Build object
    objName = BuildWebObj(g_dicColNames_obj.item("PatientActivationCode"));  //Build object from the given object name
    PatientAct = WaitForObject(objName, false); 
    if(!IsNullorUndefined(PatientAct)){
      var PatientUniqueID = PatientAct.ContentText;//Get Patient Activation Code
      WriteExcel(intRow,5,g_TestDataSheetName,PatientUniqueID);  //Write Baxter patient id in testdata sheet 
      ReturnValue = true;
    }else{
      Log.Error("Unable to fetch Patient Activation Code");
    } 
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  return ReturnValue;
}

//*************************************************************************************************
// Function Name        : ClickSubmitBtn
// Function Description : To Click on Submit button after creating/updating device program               
// Inputs               : None   
// Returns              : None      
//************************************************************************************************
function ClickSubmitBtn(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol   
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      //Read strStep from excel
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
      //Read strAction from excel
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol     
      
      if (strObjectName != null){
        //Build object
        VerificationObj = BuildWebObj(g_dicColNames_obj.item(strObjectName));        
      }
      
     
     intStepCounter = DataEntry(strAction,VerificationObj,strInputValue,strObjectName,strStep); 
     if (ContainsText(strAction.toUpperCase(),"CLICK")){
          WaitForPageSync();
          Wait(3);
     }
        var PatientEditablePopUp = BuildWebObj(g_dicColNames_obj.item("PopUpYesBtn"));
        Object = WaitForObject(PatientEditablePopUp, false);
        if ((IsExists(Object))&&(Object.VisibleOnScreen)){
        Object.Click();
        }         
       var PopUpBtn = BuildWebObj(g_dicColNames_obj.item("Clncl_ADP_WD_Yes"));
        Object = WaitForObject(PopUpBtn, false);
        if ((IsExists(Object))&&(Object.VisibleOnScreen)){
        Object.Click();
        }
        
        var NoticePopUpBtn = BuildWebObj(g_dicColNames_obj.item("NoticePopUpYesBtn"));
        Object = WaitForObject(NoticePopUpBtn, false);
        if ((IsExists(Object))&&(Object.VisibleOnScreen)){
        Object.Click();
        }
        
        var YesBtn = BuildWebObj(g_dicColNames_obj.item("Clncl_ADP_CrteWD_Yes"));
        Object = WaitForObject(YesBtn, false);
        if ((IsExists(Object))&&(Object.VisibleOnScreen)){
        Object.Click();
        }


     VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
    }  
    g_stepnum = strStep;   
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }   
}

//*************************************************************************************************
// Function Name        : ClickConfirmSubmitBtn
// Function Description : To Click on Confirm functionmit button after creating/updating device program               
// Inputs               : None 
// Returns              : None         
//************************************************************************************************
function ClickConfirmSubmitBtn(){ 
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol   
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol      
      if (strObjectName != null){
        VerificationObj = BuildWebObj(g_dicColNames_obj.item(strObjectName));          //Build object      
      }     
     intStepCounter = DataEntry(strAction,VerificationObj,strInputValue,strObjectName,strStep);
     VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt); 
    }
    g_stepnum = strStep;
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}
//*************************************************************************************************
// Function Name        : VerifyAsterisk
// Function Description : To Verify asterick symbol in updated values of Device Program/System Settings/Patient Settings                  
// Inputs               : Updated Values of Device Program/System Settings/Patient Settings
// Returns              : None        
//************************************************************************************************
function VerifyAsterisk(){
 try{
    var intIniRwNo;       //Variable for storing initial row number
    var intEndRwNo;       //Variable for storing end row number
    var strObjectName;    //Variable for storing object name
    var objName ;         //Variable for storing object 
    var strInputValue;    //Variable for storing input value 
    var strAction;        //Variable for storing action 
    var intStepCounter = 1;   //Variable for storing flag
    //Reads initial row and end row for a keyword
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Read object name from excel sheet
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        //Read input value from excel
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        //Read strStep from excel
        strStep = ReadExcel(intIterCnt,c_intStepCol);//Reads the step number from c_intStepCol
        //Building object
        ConfirmTable = BuildWebObj(g_dicColNames_obj.item(strObjectName));
        var Object = WaitForObject(ConfirmTable, true);   
        if(Object.Exists){
          ChildObj =  Object.FindAllChildren("objectType","Cell",50);//find all child objects
          ChildObj = new VBArray(ChildObj).toArray();
          for (IntChildCnt = 2; IntChildCnt <= ChildObj.length - 1; IntChildCnt++){
          //Compares the input value with the cell value
          if (aqString.Compare((ChildObj[IntChildCnt].contentText),trim(strInputValue),false) == 0){ 
              var Asterisk = Object.Cell(ChildObj[IntChildCnt].RowIndex,0).panel(0)         
              ScrollIntoView(Asterisk); 
              ExpectedMessage =   "Asterisk symbol shall appear for "+strInputValue ;    
              if (Asterisk.Exists){  //check if asterisk exists for the specified device setting
                strLogMessage = strStep +" - Asterisk symbol appears for "+strInputValue+"";
                ActualMessage =   "Asterisk symbol appears for "+strInputValue ;
                intStepCounter = 0;
                Log.Message  (strLogMessage);
                break;
               }       
              else{
                strLogMessage = strStep +" - Asterisk symbol does not appear for "+strInputValue+"";
                ActualMessage =   "Asterisk symbol does not appear for "+strInputValue;
                Log.Message  (strLogMessage);
                intStepCounter = 1;
                break;
              }
             }     
           }      
         }
         else{
       Log.Message("Object does not exist");
       intStepCounter = intStepCounter + 1;
      }
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
     }  
    }    
    g_stepnum = strStep;
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : VerifyCancelbtn
// Function Description : To Verify the functionality of cancel button              
// Inputs               : None   
// Returns              : None       
//************************************************************************************************
function VerifyCancelbtn(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        //Read input value from excel
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        //Read strStep from excel
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
        //Read strAction from excel
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
        
      }
    } 
    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}
//*************************************************************************************************
// Function Name        : DeleteDeviceProgram
// Function Description : To delete particular device program given in the test data                   
// Inputs               : Device Program name     
// Returns              : None
//*************************************************************************************************
function DeleteDeviceProgram(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol    
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      }
    } 
    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
} 
//************************************************************************************************
// Function Name        : VerifyNotExist
// Function Description : To verify that the object does not exist            
// Inputs               : None  
// Returns              : None      
//************************************************************************************************ 
function VerifyNotExist(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1; //Variable to store pass/fail count
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
        
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    if (strObjectName != null){
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol     
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
      
      var Objectverify = WaitForObject(objName, false);  
      if(IsExists(Objectverify)){
        intStepCounter = 1;         
      }else{
        intStepCounter = 0;       
      }               
    }    
  VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
  }
  g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : CancelPopupConfirmation 
// Function Description : To Verify cancel pop up functinalities                   
// Inputs               : None    
// Returns              : None       
//************************************************************************************************
function CancelPopupConfirmation(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        //Read input value from excel
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        //Read strStep from excel
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
        //Read strAction from excel
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
        //This function performs the strActions specified in the excel 
        
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
         var SettingsPopupNo1= BuildWebObj(g_dicColNames_obj.item("Clncl_ADP_CrteWD_Yes"));
         var Object = WaitForObject(SettingsPopupNo1, false);
        if (IsExists(Object)){
          Object.Click();
        }    
        var PatientEditablePopUp = BuildWebObj(g_dicColNames_obj.item("PopUpYesBtn"));
        Object = WaitForObject(PatientEditablePopUp, false);
        if (IsExists(Object)){
          Object.Click();
        }  

        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      }
    } 
    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : PwdConfirmation
// Function Description : To validate the confirm password pop up after updation/creation of DeviceProgram/System Settings/Patient Settings              
// Inputs               : Password    
// Returns              : None      
//************************************************************************************************
function PwdConfirmation(){
try{
  var intIniRwNo;        //Variable for storing initial row number
  var intEndRwNo;        //Variable for storing end row number
  var strObjectName;     //Variable for storing object name
  var objName;           //Variable for storing object 
  var strInputValue;     //Variable for storing input value 
  var strAction;         //Variable for storing action 
  var strStep = "";           //Variable for storing step number
  var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
  intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
  intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
  for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
    //Read object name from excel 
    strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol   
    strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
    //Read strStep from excel
    strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
    //Read strAction from excel
    strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol     
    if (strObjectName != null){
      //Build object
      VerificationObj = BuildWebObj(g_dicColNames_obj.item(strObjectName));        
    }  
   intStepCounter = DataEntry(strAction,VerificationObj,strInputValue,strObjectName,strStep); 
   if (strObjectName == "PasswordPopup_Submitbutton"){
          Wait(5);
   } 
   VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
   
  } 
  g_stepnum = strStep;
  }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}
//*************************************************************************************************
// Function Name        : ClickViewLink
// Function Description : Click on view link in DeviceSettings page                  
// Inputs               : Device Program name  
// Returns              : None        
//************************************************************************************************
function ClickViewLink(){ 
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    //Read object name from excel 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
     if (strObjectName != null){
      strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strInputValue1 = ReadExcel(intIniRwNo,c_intInputVal1Col);
      //Read strStep from excel
      strStep = ReadExcel(intIniRwNo,c_intStepCol);
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
      //function call
      ListTable(strObjectName,strInputValue,0,strInputValue1,strStep,strStepType);
     }
     //Function Call
     VerifyLinkClick();
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : XMLFileUpdate
// Function Description : To update xml file                   
// Inputs               : None     
// Returns              : None
//*************************************************************************************************
function XMLFileUpdate(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var strStepType;       //Variable for storing step type    
    var DeviceXMLFile;     //Variable to store XML path
    var strTime = aqString.Replace(aqDateTime.Time(), ":", "_");   //Variable to store timestamp
    var RenamedFile;  //Variable to store the renamed file inside bitmap folder
    var FileExist = false;  //Variable to check whether the required xml file exists 
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol
    strStepType = ReadExcel(intIniRwNo, c_intStepType);  //Reads the step from c_intStepType
    strInputValue = ReadExcel(intIniRwNo, c_intInputValCol);  //Reads the input value from c_intInputValCol
    DeviceXMLFile = gStoresPath+"\\"+strInputValue;  
    if(aqFileSystem.Exists(DeviceXMLFile)){    
      FileExist = true;
      RenamedFile = g_bitmap+ "\\"+ g_TestDataSheetName+"-"+strTime+"-"+strInputValue;  
      aqFileSystem.CopyFile(DeviceXMLFile,RenamedFile); 
      var xmlObject = Sys.OleObject("Msxml2.DOMDocument.6.0");       
      xmlObject.load(RenamedFile);
      SettingsRequestxmlfilename = g_RelativePathToBitmap+ "\\"+g_TestDataSheetName+"-"+strTime+"-"+strInputValue;  //relative path of the xml file
      fnInsertResult("XMLFileUpdate",strStep,"The XML file " +strInputValue + " shall exist","The XML file " +strInputValue + " exists","PASS",strStepType);
    }else{      
      FileExist = false;   
      SettingsRequestxmlfilename = g_bitmap+ "\\"+g_TestDataSheetName+"-"+strTime+"-"+"Error.txt";
      aqFile.Create(SettingsRequestxmlfilename);
      aqFile.WriteToTextFile(SettingsRequestxmlfilename, "The XML file " +strInputValue + " does not exist in the project folder", aqFile.ctUTF8);
      SettingsRequestxmlfilename = g_RelativePathToBitmap+ "\\"+g_TestDataSheetName+"-"+strTime+"-"+"Error.txt";
      fnInsertResult("XMLFileUpdate",strStep,"The XML file " +strInputValue + " shall exist","The XML file " +strInputValue + " does not exist","FAIL",strStepType);
    } 
    
    for (intIterCnt = intIniRwNo+1 ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      strInputValue1 = ReadExcel(intIterCnt, c_intInputVal1Col);  //Reads the action from c_intActionCol      
      if(strObjectName == "StartTime"){
        TodayDate =((strInputValue/86400)+25569+(5/24))
        WriteExcel(intIterCnt,c_intInputVal1Col,g_TestDataSheetName,TodayDate)        
      }  
      ExpectedMessage = strObjectName+" shall be updated as " +strInputValue+" in the XML file";    
      if(FileExist){          
        if((IsNullorUndefined(strInputValue1))||(strInputValue1=='0')){
          //Update XML for Treatment file upload
          var obj=xmlObject.selectNodes("//"+strObjectName);
          obj(aqConvert.StrToInt(strInputValue1)).text=aqConvert.DateTimeToFormatStr(strInputValue,"%Y-%m-%d")+"T00:00:00.000";
          ActualMessage = strObjectName+" is updated as " +strInputValue+" in the XML File";
          intStepCounter = 0;
        }else{
          //Update PAC in xml file
          xmlObject.selectSingleNode("//"+strObjectName).text=strInputValue;          
          ActualMessage = strObjectName+" is updated as " +strInputValue+" in the XML File";
          intStepCounter = 0;
        }
      }else{
        ActualMessage = "The XML file " +strInputValue + " does not exist";   
      }       
      xmlObject.save(DeviceXMLFile);
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
    }  
      
  }catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
     ActualMessage = g_strFuncCall+" failed due to the exception "+e.description;
  }finally{
    SettingsRequestxmlfilename = "";
    g_stepnum = strStep;
    if(intStepCounter == 1){
      StopCurrentTest();
    }
  }   
}


//*************************************************************************************************
// Function Name        : ClickFlagIcon
// Function Description : To Click on flag icon in Clinical Dashboard page                  
// Inputs               : Patient Name
// Returns              : None        
//************************************************************************************************
function ClickFlagIcon(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var strObjectName;     //Variable for storing object name 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;   
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword   
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol       
    strInputValue = ReadExcel(intIniRwNo, c_intInputValCol);  //Reads the input value from c_intInputValCol
    strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol  
    
    Object = BuildWebObj(g_dicColNames_obj.Item(strObjectName));   
    var TableObject = WaitForObject(Object, true);
    IntRows=TableObject.ChildCount;
    for (intColCnt = 0 ; intColCnt < IntRows; intColCnt++){
      if (TableObject.Child(intColCnt).Panel(0).Panel(0).Panel("flag_*").Exists ){
         TableObject.Child(intColCnt).Panel(0).Panel(0).Panel("flag_*").Click();  //Click on the flag icon in the table
         intStepCounter = 0;
      break;
      }
    else{
         Log.Message("Flag icon does not exist in the table")      
    }
    }                                                                  
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo); 
 }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
 g_stepnum = strStep;
}
//************************************************************************************************
// Function Name        : SelectReportType
// Function Description : To select report from dropdown          
// Inputs               : ReportType
// Returns              : None         
//***********************************************************************************************
function SelectReportType(){
  try{
     var intIniRwNo;        //Variable for storing initial row number
     var intEndRwNo;        //Variable for storing end row number
     var strObjectName;     //Variable for storing object name
     var objName;           //Variable for storing object 
     var strInputValue;     //Variable for storing input value 
     var strAction;         //Variable for storing action 
     var strStep = "";           //Variable for storing step number
     var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
     intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
     intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
     for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        //Read input value from excel
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        //Read strStep from excel
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
        //Read strAction from excel
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
        //This function performs the strActions specified in the excel 
        
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
        
        
      }
    }  
    Sys.Browser(g_strBrowser).Page("*").Keys("[Down]");
    Sys.Browser(g_strBrowser).Page("*").Keys("[Down]");
    Sys.Browser(g_strBrowser).Page("*").Keys("[Down]"); 
    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}
//*************************************************************************************************
// Function Name        : GetPatient 
// Function Description : To select patient from patient search table for(report generation                   
// Inputs               : Patient Name    
// Returns              : None        
//************************************************************************************************
function GetPatient() {
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;               //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var intStepCounter = 1;    //Variable for storing flag
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword    
    for(intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Reading object name
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        //Building object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));
        //Reading input value
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        //Reading Action
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
        //Read strStep from excel
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
        //This function will perform action specified in testdata
        
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      }
    }
    
    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}
//*************************************************************************************************
// Function Name        : EnterDate
// Function Description : To enter date (number) from date pick control                   
// Inputs               : Date specified in testdata     
// Returns              : None    
//*************************************************************************************************
function EnterDate(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strResult;         //Variable for storing result
    var Arrayinput = new Array();  //Arry to store date
    var objnotexist = 0;   //Variable to verify if the object exist
    var intStepCounter = 1;  //Variable for storing pass/fail count
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    //Read object name from excel 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    if (strObjectName != null){    
      //Read input value from excel
      strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
      var d = new Date(strInputValue);
      var n = d.toString(); 
      //to get the date in expected format
      Arrayinput = n.split(" ");
      var inputnum = Arrayinput[2]; 
      var month=Arrayinput[1];
      var year= Arrayinput[5];
      var con_month;
      switch (month){
        case "Jan"  : 
          con_month="January";
          break;
        case "Feb"  :  
          con_month="February";
          break;
        case "Mar"  : 
          con_month="March";
          break;
        case "Apr"  : 
          con_month="April";
          break;
        case "May"  :  
          con_month="May";
          break;
        case "Jun"  :
          con_month="June";
          break;
        case "Jul"  : 
          con_month="July";
          break;
        case "Aug"  : 
          con_month="August";
          break;
        case "Sep"  : 
          con_month="September";
          break;
        case "Oct"  : 
          con_month="October";
          break;
        case "Nov"  : 
          con_month="November";
          break;
        case "Dec"  :  
          con_month="December";
          break;
      } 
      var  month=con_month+" "+year;
      SelectDate(month);      
      //Read strStep from excel
      strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
      //Read strAction from excel
      strAction = ReadExcel(intIniRwNo,c_intActionCol);  //Reads the action from c_intActionCol     
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
      //find ChildObject using finall method
      strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);
      objArry = BuildWebObj(g_dicColNames_obj.item(strObjectName))     
      objName = WaitForObject(objArry, true);
      ChildObj = objName.FindAllChildren("objectType","Cell", 30);
      ChildObj = (new VBArray(ChildObj)).toArray();
      //Click on a particular date
      for(var intCounter = 0; intCounter <= (ChildObj.length)- 1; intCounter++){
        if((Trim(ChildObj[intCounter].contentText) == Trim(inputnum))){
          ChildObj[intCounter].Click();
          intStepCounter = 0;
          break;          
          }
        }
      }   
      var inputdate=inputnum+"-"+con_month+"-"+year 
      if ((intStepCounter == 0)&&(objnotexist==0)) {    
      
      fnInsertResult("EnterDate",strStep,"User shall be able to enter a date to generate report","User is entered a date of "+inputdate+" to generate report","PASS",strStepType);
      }
      else{    
        fnInsertResult("EnterDate ",strStep,"User shall be able to enter a date to generate report","User is not entered a date of "+inputdate+" to generate report","FAIL",strStepType);
        g_intFailCnt = g_intFailCnt + 1; 
      }
      g_stepnum = strStep; 
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
} 
//*************************************************************************************************
// Function Name        : GenerateReport 
// Function Description : To Generate report                  
// Inputs               : Generate button  
// Returns              : None          
//************************************************************************************************
function GenerateReport() {
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var intStepCounter  = 0;    //Variable for storing result
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword    
    for(intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
     strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        //Building object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));
        //Reading input value from testdata
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        //Reading action from testdata
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
        //Read strStep from excel
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
        //This function performs the action specified in testdata 
        
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
        
      }
    }  
    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : SaveReport
// Function Description : To work on window objects                   
// Inputs               : None
// Returns              : None         
//'*************************************************************************************************
function SaveReport(){
  try{
    //Declaration of variables
    var arrayobj ;         //variable for storing array of object
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name 
    var strInputValue;    //Variable for storing input value 
    var strAction;        //Variable for storing action 
    var objName;          //Variable for storing builded web object 
    var intStepCounter = 1;    //Variable for storing flag
    var LocalBrowserName = Trim(g_strBrowserName.toUpperCase());
    var LocalBrowser = Trim(g_strBrowser.toUpperCase());
    //Reads initial row and end row for a keyword
    intIniRwNo = g_intStrtRow;
    intEndRwNo = g_intEndRow;
    isInteractingOnDesktopObject = true; //SaveReport interacts with Desktop Object and so each step will wait for 60 seconds if this is not set.
    Wait(3); //Wait for the popup to appear
    if(CompareText(LocalBrowserName,"IEXPLORE") || CompareText(LocalBrowser,"IEXPLORE") ){ 
      Sys.Browser("iexplore").Dialog("*").FindChild("Caption","Save as", 5000).Click();  
    }else if(CompareText(LocalBrowserName,"FIREFOX") || CompareText(LocalBrowser,"FIREFOX") ){
      Sys.Browser("firefox").UIPage("*").FindChild("idStr","save", 5000).Click();
      Wait(1);
      Sys.Browser("firefox").UIPage("*").FindChild("ObjectIdentifier","OK", 5000).Click();
    }else if(CompareText(LocalBrowserName,"EDGE") || CompareText(LocalBrowser,"EDGE") ){   
      Wait(3); 
      Sys.Refresh();
      Sys.Browser("edge").BrowserWindow(0).FindChild("ObjectIdentifier","Split_button_more_options",5000).Click(); //Click on the Up arrow at the side of Save
      Wait(1);
      Sys.Browser("edge").BrowserWindow(0).FindChild("ObjectIdentifier","Save_as",5000).Click(); //Click Save as
    }
    
    for(intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      var strParent = "Sys.Browser("+chr(34)+g_strBrowser+chr(34)+").Dialog("+chr(34)+ "*"+chr(34)+")";
      if(CompareText(LocalBrowserName,"EDGE") || CompareText(LocalBrowser,"EDGE")){
        var strParent = "Sys.Process("+chr(34)+"PickerHost"+chr(34)+").Dialog("+chr(34)+ "*"+chr(34)+")";
      }
      arrayfirst = strObjectName.split("&&")
      var prop=arrayfirst[0];
      var prop_value=arrayfirst[1];
      var prop1="FindChild("+prop+","+prop_value+","+" 1000"+")";
      objName=strParent+"."+prop1      
      if (strObjectName != null){
        //Build object   
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
        if(aqString.Compare(strAction,"KeyInData",0)==0){
          if (aqString.Find(strInputValue,".csv") != -1){
            strDate=aqConvert.DateTimeToFormatStr(aqDateTime.Today(),"%#d-%b-%Y");
            strTime = aqString.Replace(aqDateTime.Time(), ":", "_");
            csvfilename = g_RelativePathToBitmap+"\\"+strDate+strTime+strInputValue;
            csvfilename1 = g_bitmap+"\\"+strDate+strTime+strInputValue;
            strInputValue=csvfilename1;
          }else{
            strInputValue=g_sProjPath+"\Stores\\Files\\"+strInputValue+".csv";
          }
        }
               
        if(aqFileSystem.Exists(strInputValue)){
          aqFileSystem.DeleteFile(strInputValue); //Delete the file if a file of same name exists
        }
                
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);   
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt); 
    }
    }
    
      //Close the Notification bar
      if(CompareText(LocalBrowserName,"IEXPLORE") || CompareText(LocalBrowser,"IEXPLORE")){
        //Close the Notification bar
        var NotificationBar = Sys.Browser("iexplore").BrowserWindow(0).FindChild("Caption", "Notification", 1000);
        if (NotificationBar.Exists && NotificationBar.Visible){
            NotificationBar.FindChild("Caption","Close",5000).Click()
        }   
      }else if(CompareText(LocalBrowserName,"EDGE") || CompareText(LocalBrowser,"EDGE")){ 
          if(Sys.Browser("edge").BrowserWindow(0).FindChild("ObjectIdentifier","Dismiss",500).Exists){
            Sys.Browser("edge").BrowserWindow(0).FindChild("ObjectIdentifier","Dismiss",500).Click()        
          }
      }
         
    g_stepnum = strStep; 
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{
    isInteractingOnDesktopObject = false;
  } 
}
//*************************************************************************************************
// Function Name        : CompareCSVFiles
// Function Description : To Compare two CSV files                  
// Inputs               : File Paths  
// Returns              : None        
//************************************************************************************************
function CompareCSVFiles(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number 
    var strInputValue ;    //Variable for storing input value 
    var strStep = "";      //Variable for storing step number
    var intStepCounter = 1;  
    var FileContent;   //Variable to store filecontent
    var FileExist = false;  //Variable to store whether file exist or not
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword  
    strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol
    
    strInputValue = ReadExcel(intIniRwNo, c_intInputValCol);  //Reads the input value from c_intInputValCol
    ExpectedMessage= strInputValue+" file shall exist";
    
    if(aqFileSystem.Exists(gStoresPath+strInputValue)){  
      FileContent = SplitTextFile(strInputValue); 
      FileExist = true; 
      ActualMessage = strInputValue+" file exists";
      VerificationPoint(g_strFuncCall,strStep,0,intIniRwNo);
    }else{   
      FileExist = false;
      ActualMessage = strInputValue+" file does not exist";
      //Create a dummy text file when file does not exist
      BitmapFile = g_bitmap+ "\\"+g_TestDataSheetName+"-"+strTime+"-"+"Error.txt";
      aqFile.Create(BitmapFile);
      aqFile.WriteToTextFile(BitmapFile, "The generated csv file does not exist", aqFile.ctUTF8);
      csvfilename = g_RelativePathToBitmap+ "\\"+g_TestDataSheetName+"-"+strTime+"-"+"Error.txt";    
      VerificationPoint(g_strFuncCall,strStep,1,intIniRwNo);
    }     
    for (intIterCnt = intIniRwNo+1 ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol      
      ExpectedMessage = "Text "+strInputValue+" shall be present in the CSV file";
      if(FileExist){        
        if(ContainsText(FileContent,strInputValue)){
          intStepCounter = 0;
          ActualMessage = "Text "+strInputValue+" is present in the CSV file"; 
        }else{
          intStepCounter = 1;
          ActualMessage = "Text "+strInputValue+" is not present in the CSV file";          
        }
      }else{
         intStepCounter = 1;          
         ActualMessage = "CSV file does not exist";   
      } 
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);     
    }       
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{
    csvfilename="";
    csvfilename1="";
    g_stepnum = strStep;   
  } 
}

//************************************************************************************************
// Function Name        : ComparePDFFiles
// Function Description : To Compare two PDF files                  
// Inputs               : Downloaded File Path and Input file path  
// Returns              : None        
//************************************************************************************************
function ComparePDFFiles(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var strInputValue ;    //Variable for storing input value 
    var strStep = "";      //Variable for storing step number
    var PDFContentCopied = false; //Variable to check whether the pdf file content is copied
    var strStepType;       //Variable for storing step type   
    var TextFileContent;   //Variable to store the pdf content 
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword  
    strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol
    strStepType = ReadExcel(intIniRwNo, c_intStepType);  //Reads the step from c_intStepType
    strInputValue = ReadExcel(intIniRwNo, c_intInputValCol);  //Reads the input value from c_intInputValCol
    ExpectedMessage = strInputValue+" file shall be exist for comparison";   
    TextFileContent = SplitPDFFiles(strInputValue);  
    if (!CompareText(TextFileContent,"PDF content is not copied") && g_report==0){      
      PDFContentCopied = true;
      ActualMessage= strInputValue+" file exists for comparison";
      VerificationPoint(g_strFuncCall,strStep,0,intIniRwNo);
    }else{   
      PDFContentCopied = false;
      ActualMessage= strInputValue+" file does not exist for comparison"; 
      VerificationPoint(g_strFuncCall,strStep,1,intIniRwNo);
    } 
    for (intIterCnt = intIniRwNo+1 ; intIterCnt <= intEndRwNo ; intIterCnt++){   
      //Read input value from excel
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
      strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
      ExpectedMessage = strInputValue+" shall be matched with the PDF content";
      if(PDFContentCopied){   
        if(IsEmpty(strAction) || IsNullorUndefined(strAction)){ //to compare with the PDF          
          if(aqString.Find(TextFileContent,strInputValue)==-1){
            intStepCounter = 1; 
            ActualMessage = strInputValue+" is not matched with the PDF content";
          }else{
            ActualMessage = strInputValue+" is  matched with the PDF content";
            intStepCounter = 0; 
          }
        }else{ //to close the pdf 
          strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameColstrObjectName = readexcel
          objName = g_dicColNames_obj.item(strObjectName);  //Build object from the given object name    
          intStepCounter =  DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        }            
      }else{
        ActualMessage= "PDF content is not copied "; 
        intStepCounter = 1;      
      } 
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
     }
     
     //To Close the tab which got opened during PDF downloading 
      g_strBrowserName = GetDicParam("BrowserName");   //Browser Name from Config file
      switch (trim(g_strBrowserName)){
        case "IE":
          Sys.Browser("iexplore").BrowserWindow(0).Panel("Navigation Bar").FindChild("Caption","Close *", 1000).Click();
          break;  
        case "Chrome":
          Sys.Browser(g_strBrowserName).Page(g_strWebAppURL+"*").Close();
          break;
        case "Edge": 
          Sys.Refresh();
          Sys.Browser("edge").BrowserWindow(0).FindChild("ObjectIdentifier","Close_tab_2",1000).Click(); 
          break;
      }     
      
      
  } 
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{
    g_stepnum = strStep; 	
    GeneratedPDFReportTextFile = "";
    CloseAdobeAcrobat();
  }
}


//************************************************************************************************
// Function Name        : CopyPDFData
// Function Description : Copy PDF Data to notepad application                  
// Inputs               : PDF Data  
// Returns              : None        
//************************************************************************************************
function CopyPDFData(){	
  try{
  		var intIniRwNo = g_intStrtRow;
  		g_report=1;   //Is equal to 1 when report content is not copied 
  		Sys.Clipboard="";
  		var intStepCounter=1;  //initializing stepcounter to fail
  		var PDFName;  //Variable to store the pdf name
  		var RenamedFile;  //Variable to store the renamed file inside bitmap folder
  		var strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);
  		var strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
  		var TimeStamp = aqConvert.DateTimeToFormatStr(aqDateTime.Now(),"%d-%b-%Y_%H-%M-%p");  
  		var PDFFileName = g_TestDataSheetName+"_"+TimeStamp+"_reports.pdf";
  		var PDFFilePath = g_bitmap+"\\"+PDFFileName;
  		var ErrorFileName = g_TestDataSheetName+"-"+TimeStamp+"-"+"error.txt";
  		var ErrorFilePath = g_bitmap+"\\"+ErrorFileName;
  		ExpectedMessage="PDF shall be generated";
  		ActualMessage="PDF is not generated";

  		//In few PC's IE would show a popup to open or save. There is no settings in IE to configure this and it is different for Windows 7 and Windows 8
  		if(CompareText(g_strBrowser,"iexplore")){ 
  				var FileOpenObject = Sys.Browser(g_strBrowser).WaitDialog("*",0);
  				if(FileOpenObject.Exists){
  						FileOpenObject = FileOpenObject.FindChild("Caption","Open", 2000);
  						if (IsExists(FileOpenObject)){
  								FileOpenObject.Click();
  								Wait(5);   
  						}
  				}
  		}

  		//If the second tab exists in browser perform save operation in pdf,open pdf in adobe reader and copy the contents and paste in txt file
  		if(Sys.Browser(g_strBrowser).BrowserWindow(0).Exists){      
  				ActualMessage="PDF is generated";
  				var isFileDownloaded = SaveFile(PDFFilePath);
  				if (isFileDownloaded){
  						Sys.OleObject("WScript.Shell").Run("AcroRd32.exe"+ " " +aqString.Quote(PDFFilePath));//Add Quote, so the path works even if there is space in the path
              Wait(2); //Wait for the Adobe reader window to open
              Sys.Refresh();//Refresh to include to newly opened adobe process
  						var AdobeProcess = Sys.WaitProcess("AcroRd32",5000, 2);
  						if(!AdobeProcess.WaitWindow("AcrobatSDIWindow", "*reports.pdf*", 1, 5000).Exists){  
  								AdobeProcess = Sys.Process("AcroRd32", 4);
  						}
              //**************
              Wait(5);
    				  if (IsExists(AdobeProcess.WaitDialog("Make Adobe Reader my default PDF application.", 5000))){
                //Select the Checkbox so that the popup is not displayed again.
                AdobeProcess.Dialog("Make Adobe Reader my default PDF application.").FindChild("ObjectIdentifier","Do not show this message again",10).Click();
                //Click cancel so the OS does not attempt to set Adobe as the default app. This causes problem in Windows 10 machines.
                AdobeProcess.Dialog("Make Adobe Reader my default PDF application.").FindChild("ObjectIdentifier","Cancel",10).Click();
              }

						//If Adobe reader is opened
						var AdobeWindow = AdobeProcess.WaitWindow("AcrobatSDIWindow", "*reports.pdf*", 1, 5000);
						if(AdobeWindow.Exists){
							//Activate the Adobe Window
							AdobeWindow.Activate();
							AdobeWindow.Keys("[Enter]");  
							AdobeWindow.Keys("~vpc"); //Pressing Alt + VPC, to view enable scrolling view
							Wait(1)
							var ClipBoardText = "";
							var DoCount = 0;
							do {               //Copy the contents of PDF and paste it in text file
								AdobeWindow.Click();
								AdobeWindow.Keys("^a");          
								Wait(2);
								AdobeWindow.Keys("^c");
								ClipBoardText = Sys.Clipboard;
								DoCount++;
							}while(ClipBoardText.length==0 && DoCount<5) 
							//Close adobe reader
							AdobeWindow.Close(5000);
							//Rename any existing file in the Bitmap folder which was created by previous test case.
							if(aqFileSystem.Exists(g_bitmap+"\\"+strInputValue)){ 
									var OldFile = aqConvert.DateTimeToFormatStr(aqDateTime.Now(),"%d-%b-%Y_%H-%M-%S-%p");
									aqFileSystem.RenameFile(strInputValue,g_bitmap+"\\"+OldFile+strInputValue); 
							}
							strInputValue = g_bitmap+"\\"+strInputValue;
							var fso = Sys.OleObject("Scripting.FileSystemObject");  
							var ExtractedTextFile = fso.OpenTextFile(strInputValue, ForWriting = 2,true);  //open file in write mode
							if(Sys.Clipboard!=null){
									var ExtractedTextContent = Sys.Clipboard;
									ExtractedTextContent = ExtractedTextContent.replace(/(\r\n|\n|\r)/gm," ");  //Replaces all the line breaks
									ExtractedTextFile.Write(ExtractedTextContent);   
									g_report=0;   //Is equal to 0 when report content is copied 
									intStepCounter=0;
									ExtractedTextFile.Close();
							}else{
									ActualMessage = "Text is not copied from the Adobe Reader window.";
									aqFile.Create(ErrorFilePath);
									aqFile.WriteToTextFile(ErrorFilePath, "Text from generated PDF report is is not available for comparison, since " + ActualMessage, aqFile.ctUTF8);
									Log.Error(ActualMessage);
							}
						}else{
								ActualMessage = "Downloaded Report is not opened in Adobe Reader.";
								aqFile.Create(ErrorFilePath);
								aqFile.WriteToTextFile(ErrorFilePath, "Text from generated PDF report is is not available for comparison, since " + ActualMessage, aqFile.ctUTF8);
								Log.Error(ActualMessage);
						}
  				}else{
  					ActualMessage  = "Generated PDF Report was not downloaded.";
  					aqFile.Create(ErrorFilePath);
  					aqFile.WriteToTextFile(ErrorFilePath, "Text from generated PDF report is is not available for comparison, since " + ActualMessage, aqFile.ctUTF8);
  					Log.Error(ActualMessage);
  				}
  		}else{
  				ActualMessage  = "PDF Report was not generated.";
  				aqFile.Create(ErrorFilePath);
  				aqFile.WriteToTextFile(ErrorFilePath, "Text from generated PDF report is is not available for comparison, since " + ActualMessage, aqFile.ctUTF8);
  				Log.Error(ActualMessage);
  		}  	
	
  }catch(e){
		ActualMessage = g_strFuncCall+" failed due to the exception "+e.description;
		aqFile.Create(ErrorFilePath);
		aqFile.WriteToTextFile(ErrorFilePath, "Text from generated PDF report is is not available for comparison, since " + ActualMessage, aqFile.ctUTF8);
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{    
  		VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo); //Desktop Screenshot will be used. 
  		if (intStepCounter == 0){
  				GeneratedPDFReportTextFile = g_RelativePathToBitmap+ "\\"+PDFFileName; //Generated PDF as the screenshot   
  		}else{
  				GeneratedPDFReportTextFile = g_RelativePathToBitmap+ "\\"+ErrorFileName; //error.txt as the screenshot   
  		} 
  		g_stepnum = strStep; 
  }
}


//*************************************************************************************************
// Function Name        : SelectDate
// Function Description : To navigate to expected month and year of a datePick control                  
// Inputs               : Month,Year  
// Returns              : None  
//**************************************************************************************************
function SelectDate(month){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strUserName ;      //Variable for storing UserName
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    //Read object name from excel 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    //Build object    
    var ObjImage = BuildWebObj(g_dicColNames_obj.item("Prev_calimg"));
    var ObjYearMonth = BuildWebObj(g_dicColNames_obj.item("ymobj"));
    YearMonthObject = WaitForObject(ObjYearMonth, true);
    strInputValue=month;      
    //Read strStep from excel
    strStep = ReadExcel(intIniRwNo,c_intStepCol);
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    var CalendarMonthObj = BuildWebObj(g_dicColNames_obj.item("CalendarMonth"));
    var CalendarMonth = WaitForObject(CalendarMonthObj, true);      
    var CalendarYearObj = BuildWebObj(g_dicColNames_obj.item("CalendarYear"));
    var CalendarYear = WaitForObject(CalendarYearObj, true);      
    var strMonthYear = CalendarMonth.contentText + " " + CalendarYear.contentText;
    //navigating to month year until expected month yera will not visible in date control      
    if (aqString.Compare(trim(strMonthYear),trim(strInputValue), false) == 0){
    //Do nothing
    }
    else{
      do {
        var PreviousObject = WaitForObject(ObjImage, true);
        CalendarMonth = WaitForObject(CalendarMonthObj, true);
        PreviousObject.Click();   //Click the previous arrow
        strMonthYear = CalendarMonth.contentText + " " + CalendarYear.contentText;;
      }
      while(aqString.Compare(trim(strMonthYear),trim(strInputValue), false) != 0);      
    }
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}
//*************************************************************************************************
// Function Name        : VerifyLinkClick
// Function Description : To Verify Edit/MakePrimary/View link click is successful              
// Inputs               : Edit/MakePrimary/View link 
// Returns              : None       
//************************************************************************************************
function VerifyLinkClick(){
  try{
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    var objnotexist = 0;
    //Read object name from excel 
    strObjectName = ReadExcel(intEndRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    if (strObjectName != null){
      //Build object
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
      //Read input value from excel
      strInputValue = ReadExcel(intEndRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
      //Read strStep from excel
      strStep = ReadExcel(intEndRwNo,c_intStepCol);  //Reads the step from c_intStepCol
      //Read strAction from excel
      strAction = ReadExcel(intEndRwNo,c_intActionCol);  //Reads the action from c_intActionCol     
      
      //This function performs the strActions specified in the excel 
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intEndRwNo);
      
    } 
   g_stepnum = strStep;
   }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}
//*************************************************************************************************
// Function Name        : SplitPDFFiles
// Function Description : Split Timestamp from the notepad application                 
// Inputs               : None  
// Returns              : None        
//************************************************************************************************  
function SplitPDFFiles(TextFileName){
  try{
    var TextFilePath=g_bitmap+"\\"+TextFileName;
    var TextFileContent = "";
    if(aqFileSystem.Exists(TextFilePath)){      //Check if the file exists  
      oTextFile = aqFile.OpenTextFile(TextFilePath, aqFile.faRead ,aqFile.ctANSI);  //Open the text file
      if (oTextFile.FileLength()!=0){     //Check if the file is not empty    
        TextFileContent = oTextFile.ReadAll();  //Read contents from a file
        oTextFile.Close(); 
        //After reading the content rename the file, so it not impact the next test case.
        var OldFile = aqConvert.DateTimeToFormatStr(aqDateTime.Now(),"%d-%b-%Y_%H-%M-%S-%p");
        aqFileSystem.RenameFile(TextFilePath,g_bitmap+"\\"+OldFile+TextFileName);         
      }else{
        TextFileContent="File is empty";
        Log.Error(TextFileContent);
      }
    }else{
      TextFileContent="File not found";
      Log.Error(TextFileContent);
    }
    return  TextFileContent;
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : SplitTextFile
// Function Description : Split Timestamp from the text file                 
// Inputs               : Input file path and generated file path  
// Returns              : True/False        
//************************************************************************************************  
function SplitTextFile(fileName1, fileName2){
  try{
    var ForReading = 1;  //Initialize variable to open text file in readonly mode
    // Creates the FileSystemObject object
    var fso = Sys.OleObject("Scripting.FileSystemObject");
    fileName1 = g_sProjPath+"\Stores\\Files\\"+fileName1;
    if(aqFileSystem.Exists(fileName1)){
      var file1 = fso.OpenTextFile(fileName1, ForReading);
      // Reads the contents of the text file
      var fileText1 = file1.ReadAll();
      // Closes the text file
      file1.Close();
      DeleteFile(fileName1);
      var strFirst = fileText1.toString();  //Variable to convert the file path to string type 
      var strsplit = strFirst.split("Timestamp:,");  //Array to store the splitted text
      var strSecond = strsplit[1].toString();  //Variable to convert the array element to string type
      var strSplit1 = strSecond.split("Report Name:");  //Variable to store the splitted text from array
      TxtData1 = Trim(strsplit[0]+strSplit1[1]);  //Concatenate the splitted text
      Log.Message(TxtData1);    
    }
    else{
      Log.Error("File not found");
      TxtData1="";
    }
    return TxtData1;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  } 
}

//*************************************************************************************************
// Function Name        : ListTable
// Function Description : To click on Edit/View/Make Primary Link for a particular Device Program/System/Patient Setting
// Inputs               : Device Program/System/Patient Setting Name
// Returns              : None
//************************************************************************************************
function ListTable(Obj, inputvalue, link, data, stepnumber) { 
  try{  
    var intStepCounter = 1;
    Object = BuildWebObj(g_dicColNames_obj.Item(Obj));
    var TableObject = WaitForObject(Object, true); 
    if(TableObject.Exists){  //Check if the table exists
      ChildObj = TableObject.FindAllChildren("objectType", "Cell", 50);//find all child objects
      ChildObj = (new VBArray(ChildObj)).toArray();
      for (intCounter = 0; intCounter <= ChildObj.length; intCounter++){
        //Check if the input value matches with the data in the web page
        if (aqString.Compare((ChildObj[intCounter].contentText),trim(inputvalue), false) == 0){
          var intRow = ChildObj[intCounter].RowIndex;         
          if( Obj=="table_usrlist"){ 
            var WebPageEdit = TableObject.Cell(intRow, 4).Panel(0).Panel(0).link(link);  // edit link in user table
          }  
          else{    
            var WebPageEdit = TableObject.Cell(intRow, 4).link(link);  // edit link in device setting table
          }         
          switch(data){
            case "Edit":
              if (aqString.Compare((WebPageEdit.ContentText),"Edit", false) == 0){
                WebPageEdit.Click();   //Click edit link
                strLogMessage = stepnumber +" - "+data+" link is Clicked for Device Program "+inputvalue+"";
                Log.Message (strLogMessage);
                intStepCounter=0;
              }
              else{
                strLogMessage = stepnumber+ "- "+data+" link is not Clicked for Device Program "+inputvalue+"";
                Log.Message (strLogMessage);
                
              }
              break;
            case "View":
              if (aqString.Compare((WebPageEdit.ContentText),"View", false) == 0){
                WebPageEdit.Click();  //Click View link
                strLogMessage = stepnumber+ "- "+data+" link is Clicked for Device Program "+inputvalue+"";
                Log.Message (strLogMessage);
                intStepCounter=0;
              }
              else{
                strLogMessage = stepnumber+ "- "+data+" link is not Clicked for Device Program "+inputvalue+"";
                Log.Message (strLogMessage);
                
              }
              break; 
            case "Make Primary":
              if (aqString.Compare((WebPageEdit.ContentText),"Make Primary", false) == 0){
                WebPageEdit.Click();  //Click MakePrimary link
                strLogMessage = stepnumber +" - "+data+" link is Clicked for Device Program "+inputvalue+""
                Log.Message (strLogMessage);   
                intStepCounter=0;        
              }  
              else{
                strLogMessage = stepnumber+ "- "+data+" link is not Clicked for Device Program "+inputvalue+""
                Log.Message (strLogMessage);                
              }
              break; 
          }
          
          break;     
        }
      }
    }
    else{
      Log.Message("Object does not exist")
      ActualMessage=data+"-Object does not exist";
    }
    if(intStepCounter==1){
      ActualMessage=data+" link is not clicked";
    }
    else{ 
      ActualMessage=data+" link is clicked";
    }
    ExpectedMessage=data+" link shall be clicked";
    VerificationPoint(g_strFuncCall,stepnumber,intStepCounter,g_intStrtRow);
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  } 
}
//*************************************************************************************************
// Function Name        : ClickEditLink
// Function Description : Click on Editlink in DeviceSettings page                  
// Inputs               : Device Program name  
// Returns              : None        
//************************************************************************************************
function ClickEditLink(){ 
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    //Read object name from excel 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    if (strObjectName != null){
      strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strInputValue1 = ReadExcel(intIniRwNo,c_intInputVal1Col);
      //Read strStep from excel
      strStep = ReadExcel(intIniRwNo,c_intStepCol);      
      //function call
      ListTable(strObjectName,strInputValue,1,strInputValue1,strStep);
    }
    //Function Call
    VerifyLinkClick();
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 } 
}


//*************************************************************************************************
// Function Name        : LaunchWebApp
// Function Description : To Launch the Web Application Url in Desired Browser
// Inputs               : None
// Returns              : None
////************************************************************************************************
function LaunchWebApp(){  

  var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
  intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
  intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
  intIterCnt = intIniRwNo;
  try{
     
    //Close all existing browsers
    CloseAllBrowsers();
    
    //Reads the step from c_intStepCol
    var strStep = ReadExcel(g_intStrtRow,c_intStepCol);
    
    //Get the Browser Object and configure the paramters
    var BrowserObject = Browsers.Item(g_strBrowser);
    if (CompareText(g_strBrowser,"chrome")){
        BrowserObject.RunOptions = "--ignore-certificate-errors --disable-infobars";   
    } 
    
    //Launch and Maxmize the Browser    
    BrowserObject.Run("about:blank"); //Launch
    Sys.Refresh();
    var BrowserProcess = Sys.Browser(g_strBrowser);
    BrowserProcess.BrowserWindow(0).Maximize();
    
    //Navigate to the URL and assign the page to the Global Variable
    BrowserObject.Navigate(g_strWebAppURL);
    oPageObject = BrowserProcess.WaitPage(g_strFormattedUrl, 10000);
    
    //If the page is opened in the browser, then make the Step as Pass 
    if (oPageObject.Exists){
       oPageObject.Keys("^0"); //Set the Zoom level as 100% by typing Ctrl + 0
       intStepCounter = 0;//Pass
       strLogMessage = g_strBrowser + " browser is launched.";
       Log.Message(strStep + " - " + strLogMessage);
    }else{
       strLogMessage = g_strBrowser + " browser is not launched.";
       Log.Error(strStep + " - " + strLogMessage); 
    }    
    
    //Click on the Close button in Restore Session Notification
    if (CompareText(g_strBrowser,"iexplore") && oPageObject.Exists){
        //Close the Notification bar
        var NotificationBar = BrowserProcess.BrowserWindow(0).FindChild("Caption", "Notification", 1000);
        if (NotificationBar.Exists && NotificationBar.Visible){
            NotificationBar.FindChild("Caption","Close",5000).Click()
        }   
    }
    
    g_PageObject = "Sys.Browser("+chr(34)+g_strBrowser+chr(34)+").Page("+chr(34)+g_strFormattedUrl+chr(34)+")";
    oPageObject = WaitForObject(g_PageObject, true);
        
     //Click on Accept invalid SSL link if it appears on the screen
     AcceptInvalidSSL();     
    
    //Click on the Close button in Restore pages Dialog
    if (CompareText(g_strBrowser,"chrome") && oPageObject.Exists){
      var RestoreDialog = BrowserProcess.FindChild("Caption","Restore pages?",5000);
      if (RestoreDialog.Exists){
          RestoreDialog.FindChild("Caption","Close",5000).Click();//Close the Notification bar
      }  
    }    
    
  }  
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   intStepCounter = 1;
 }finally{
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,g_intEndRow);
      //Stop the test if the browser is not launched.
      if (intStepCounter>0){
        StopCurrentTest();
      }
 }
}


//*************************************************************************************************
// Function Name        : ForgotPwdValidation
// Function Description : To validate forgot password scenario in login page with multiple mail id(disabled,locked)                   
// Inputs               : User credentials 
// Returns              : None            
//************************************************************************************************
function ForgotPwdValidation() {
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    //Click on Accept invalid SSL link if it appears on the screen
    AcceptInvalidSSL();  
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        Object = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        ObjectArray = Object.split("*");
        objName = ObjectArray[0] + g_dicColNames_obj.item(strObjectName);      
        strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
        strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol    
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
                
        //This function perForms the strActions specIfied in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      } 
    }    
    g_stepnum = strStep;
   }
   catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   } 
}

//*************************************************************************************************
// Function Name        : ChangePassword
// Function Description : Change the password of a clinic user account                                
// Inputs               : Current Password,New Password           
// Returns              : None
//*************************************************************************************************
function ChangePassword(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
        if (strObjectName == "NewPwd") {  //If the obejct name is "NewPwd" generate new password 
          var strGeneratedPwd = strInputValue;
          WriteExcel(intIterCnt,c_intInputVal1Col,g_TestDataSheetName,strGeneratedPwd); //Write the generated password in testdata sheet
        }
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol        
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);     
      }
    }  
    g_stepnum = strStep;
    }
   catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   }
} 
//*************************************************************************************************
// Function Name        : ChangeEmail
// Function Description : Change the email of a clinic user account                                
// Inputs               : New email id , Password         
// Returns              : None
//*************************************************************************************************
function ChangeEmail(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol 
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      }
    }  
    g_stepnum = strStep;
  }
   catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
} 

//*************************************************************************************************
// Function Name        : ChangeSecurityQuestion
// Function Description : Change the SecurityQuestion of a clinic user account                                             
// Inputs               : Security Questions,Answers and Current Password           
// Returns              : None
//*************************************************************************************************
function ChangeSecurityQuestion(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol    
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      }
    }      
    g_stepnum = strStep;
  }
  catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
} 
//*************************************************************************************************
// Function Name        : NavToAccountSettings
// Function Description : To navigate to account settings page                   
// Inputs               : None     
// Returns              : None
//*************************************************************************************************
function NavToAccountSettings(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
        strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
        if (strObjectName != null){
          objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
          strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
          strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
          strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol
          sInput1Value = ReadExcel(intIterCnt, c_intInputVal1Col);
          if (CompareText(strObjectName,"SelectClinic_ClinicSelect")){ 
            objName = WaitForObject(objName, true);
            strInputValue = GetExternalData(sInput1Value, strInputValue);
	          var Property = ("objectType")
            var Value = ("Link")
            if((g_strBrowser == "edge")&&(strInputValue != "Account Settings")){
                var Property = ("className")
                var Value = ("clinic-name")           
            }
            ChildObj = objName.FindAllChildren(Property,Value, 50);
            ChildObj = new VBArray(ChildObj).toArray();
            ExpectedMessage = strInputValue+" shall be clicked in the "+strObjectName;
            for(var intCounter = 0; intCounter <= (ChildObj.length)- 1; intCounter++){
              var ActualValue = Trim(ChildObj[intCounter].innerText);
              if(CompareText(ActualValue,strInputValue)){
                ChildObj[intCounter].Click();
                Wait(3);
                WaitForPageSync();
                intStepCounter = 0;
                ActualMessage = strInputValue+" is clicked in the "+strObjectName;
                break;          
              }else {
                intStepCounter = 1;
                ActualMessage = strInputValue+" is not clicked in the "+strObjectName;
              }
            }   
          }
          else{  
          //This function performs the strActions specified in the excel 
          intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
          }
          VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
        }
     }
    if (!CompareText(sInput1Value,"TestData")){
      strUserName = BuildWebObj(g_dicColNames_obj.item("AccountSettings_UserName"));  //Build object from the given object name
      Wait(3);
      strUserName = WaitForObject(strUserName, false)  
      if(!IsNullorUndefined(strUserName)){
        WriteExcel(intEndRwNo,c_intInputVal1Col,g_TestDataSheetName,GetText(strUserName));
      }
    }        
    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
} 
//*************************************************************************************************
// Function Name        : DeleteAllDevicePgm
// Function Description : To delete all device programs except primary device program                   
// Inputs               : None      
// Returns              : None
//*************************************************************************************************
function DeleteAllDevicePgm(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    var objName;
    var TableObject;
    var LinkObject;
    var strMakePrimaryText;
    var PropertyName = ("ObjectType","ObjectIdentifier")
    var PropertyValue = ("Link","2")
    for (var intRow = 2; intRow <= 5; intRow++){
      objName = BuildWebObj(g_dicColNames_obj.item("Clinical_DeviceSettings_ListTable"));  //Build object from the given object name
      TableObject = WaitForObject(objName, true);
      LinkObject = TableObject.Cell(intRow, 4).FindChild(PropertyName,PropertyValue);
      if (IsExists(LinkObject) && LinkObject.TagName == "A"){
        strMakePrimaryText = GetText(LinkObject);
        //Check if Make Primary link exists and delete all device programs except primary device program
        if (ContainsText(strMakePrimaryText,"Make Primary") || ContainsText(strMakePrimaryText,"Make Active")){
          TableObject.Cell(intRow, 4).Link(1).Click();  //Click edit link on the non-primary device program     
          for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){      
            strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
            objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
            strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
            strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
            strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol    
            intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
            VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt); 
          }         
        }
      }   
    }   
    g_stepnum = strStep;
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }  
}


//*************************************************************************************************
// Function Name        : LaunchCMD
// Function Description : To Launch the command prompt
// Inputs               : None
// Returns              : None
////************************************************************************************************
function LaunchCMD(){  
  var intStepCounter = 1;  //Fail
  ExpectedMessage = "Command window shall be launched";
  ActualMessage = "Command window is not launched"; 
  try{
    strStep = ReadExcel(g_intStrtRow,c_intStepCol);  //Reads the step from c_intStepCol
    strStepType = ReadExcel (g_intStrtRow,c_intStepType); //Reads the step type from c_intStepType     
    //Close the Command Prompt if it exists   
    CloseProcess("cmd")
         
    //Open a new Command Prompt      
    Sys.OleObject("WScript.Shell").Run("cmd.exe");
	Wait(2);
    if(Sys.WaitProcess("cmd", 5000).Exists){
      intStepCounter = 0;
      ActualMessage = "Command window is launched";
    } 
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,g_intStrtRow);
      g_stepnum = strStep;
  } 
}

//*************************************************************************************************
// Function Name        : EnterValueinCMD
// Function Description : To enter commands in command prompt
// Inputs               : None
// Returns              : None
////************************************************************************************************
function EnterValueinCMD(){  
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = g_dicColNames_obj.item(strObjectName);  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        if(aqString.Find(strInputValue,"Stores")!= -1) { 
          InputValueArray = strInputValue.split("Stores") 
          strInputValue = InputValueArray[0]+g_sProjPath+"\Stores\\"+InputValueArray[1]  
        }
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        Wait(2);      
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);  
      }  
    }
    g_stepnum = strStep; 
   }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }   
}

//*************************************************************************************************
// Function Name        : VerifyXMLValues
// Function Description : To verify the values present in the xml file                   
// Inputs               : None     
// Returns              : None
//*************************************************************************************************
function VerifyXMLValues(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var strStepType;       //Variable for storing step type
    var DeviceXMLFile;    //Variable to store XML path
    var strTime = aqString.Replace(aqDateTime.Time(), ":", "_");   //Variable to store timestamp
    var RenamedFile;  //Variable to store the renamed file inside bitmap folder
    var FileExist = false;  //Variable to check whether the required xml file exists 
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    strInputValue = ReadExcel(intIniRwNo, c_intInputValCol);   //Reads the input value from c_intInputValCol
    strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    DeviceXMLFile = g_ToolsFolderPath+"\\"+strInputValue;
    
    if(aqFileSystem.Exists(DeviceXMLFile)){
      FileExist = true;
      RenamedFile = g_bitmap+ "\\"+ g_TestDataSheetName+"-"+strTime+"-"+strInputValue;
      aqFileSystem.CopyFile(DeviceXMLFile,RenamedFile);  //Copy prescription.xml to Stores
      var xmlObject = Sys.OleObject("Msxml2.DOMDocument.6.0");
      xmlObject.load(RenamedFile);
      xmlfilename = g_RelativePathToBitmap+ "\\"+g_TestDataSheetName+"-"+strTime+"-"+strInputValue  //relative path of the xml file
      fnInsertResult("VerifyXMLValues",strStep,"XML files shall be generated","XML files are generated" ,"PASS",strStepType);
    }else{
      FileExist = false;   
      xmlfilename = g_bitmap+ "\\"+g_TestDataSheetName+"-"+strTime+"-"+"Error.txt"
      aqFile.Create(xmlfilename);
      aqFile.WriteToTextFile(xmlfilename, "XML files are not generated.", aqFile.ctUTF8);
      xmlfilename = g_RelativePathToBitmap+ "\\"+g_TestDataSheetName+"-"+strTime+"-"+"Error.txt"
      fnInsertResult("VerifyXMLValues",strStep,"XML files shall be generated " ," XML files are not generated due to decryption failure" ,"FAIL",strStepType);
    }
     
    for (intIterCnt = intIniRwNo+1 ; intIterCnt <= intEndRwNo ; intIterCnt++){
        intStepCounter = 1; //Initialize the variable to fail for each loop  
        strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
        strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol
        ExpectedMessage = "The value for " + strObjectName + " shall be " + strInputValue+ " in the XML file"        
         if(FileExist){
          var ActualText = xmlObject.selectSingleNode("//"+strObjectName).text;
          if (CompareText(ActualText,strInputValue)){            
            ActualMessage = "The value for " + strObjectName + " is " + strInputValue + " in the XML file"
            intStepCounter = 0;       
          }else{
            ActualMessage = "Expected XML value is" + strInputValue + " but actual is :" + ActualText                  
          } 
        }else{
          ActualMessage = "XML files are not generated due to decryption failure";           
        } 
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);    
    }
    g_stepnum = strStep;    
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{
    xmlfilename = "";
  } 
}

 
//************************************************************************************************
// Function Name        : VerifySettingValues
// Function Description : To Verify Device Program/PatientSetting/SystemSetting/Template values as per test data              
// Inputs               : Device Program/PatientSetting/SystemSetting/Template values 
// Returns              : None         
//************************************************************************************************
function VerifySettingValues(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      g_strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (g_strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(g_strObjectName));  //Build object from the given object name  
        if (aqString.Find(g_strObjectName,"&&")!= -1){
          objName = BuildWebObj(g_strObjectName);  //Build object from the given object name
        }   
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol      
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,g_strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
        if (g_strObjectName == "RetryButton")//To reload the entire webpage
        {
        
        
        }
        g_strObjectName = "";     
      }
    }   
    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : AddUser
// Function Description : To add a clinic user                   
// Inputs               : None      
// Returns              : None
//*************************************************************************************************
function AddUser(){
    try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      g_strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      
      if (g_strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(g_strObjectName));  //Build object from the given object name  
        if (aqString.Find(g_strObjectName,"&&")!= -1){
          objName = BuildWebObj(g_strObjectName);  //Build object from the given object name
        }   
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
        //This function performs the strActions specified in the excel 
        
         if (g_strObjectName == "CA_AddUser_LastName") {  //If the object name is AddPatient_FirstName write the generated new patient name in excel sheet
          WriteExcel(intIterCnt,c_intInputVal1Col,g_TestDataSheetName,strInputValue)
        }     
        intStepCounter = DataEntry(strAction,objName,strInputValue,g_strObjectName,strStep);
        if (g_strObjectName == "AddClinic_SubmitBtn"){ 
          Wait(5); 
        }
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt); 
        g_strObjectName = ""; 
        
      }
    }  
    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }   
}

//*************************************************************************************************
// Function Name        : MenuNavigation
// Function Description : Navigate to Primary menu in uploading treatment file portal               
// Inputs               : Primary menu name  
// Returns              : None        
//************************************************************************************************
function MenuNavigation(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    var ObjectArry = new Array();
    var strStepType;       //Variable for storing step type
    g_TreatmentFileUpload = true ;
    var ChildObjectFound = false;
    
    //Read object name from excel 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
    strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol 
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    
    SwitchBrowser("Secondary");     
    ObjectArry = BuildWebObj(g_dicColNames_obj.item(strObjectName));
    var Object = WaitForObject(ObjectArry, true);    
    if(Object.Exists){
      ChildObj = Object.FindAllChildren("objectType","Link",50);  //Find all the child objects of the type Link
      ChildObj = new VBArray(ChildObj).toArray();
      for (intChildCnt = 0 ; intChildCnt <= (ChildObj.length-1) ; intChildCnt++){ 
        if (aqString.Compare((ChildObj[intChildCnt].contentText), strInputValue, false) == 0){
          ChildObj[intChildCnt].Click();
          if(strObjectName == "Treament_Send"){
            Wait(5);
            WaitForPageSync();
            AcceptInvalidSSL();           
          } 
          ChildObjectFound = true;
          break;
        }
      } 
    }        
         
    var Object=Sys.Browser("iexplore").Page("*").FindChild("contentText","Yes",100);
    if(Object.Exists){
      Object.Click();
    }
    
    if(ChildObjectFound){
        fnInsertResult("MenuNavigation",strStep,"MenuNavigation  to "+strInputValue+" shall be successful","MenuNavigation to "+strInputValue+" is successful","PASS",strStepType);
    }else{
        fnInsertResult("MenuNavigation",strStep,"MenuNavigation to "+strInputValue+" shall be successful","MenuNavigation to "+strInputValue+" is not successful","FAIL",strStepType);
    }
    
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  finally{
    g_stepnum = strStep;
    g_TreatmentFileUpload = false ;
    SwitchBrowser("Primary");
  } 
}
//*************************************************************************************************
// Function Name        : UploadTreamentFile
// Function Description : To upload the treatment file                   
// Inputs               : Treatment file     
// Returns              : None
//*************************************************************************************************
function UploadTreamentFile(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    g_TreatmentFileUpload = true;
    SwitchBrowser("Secondary");
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
      strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol   
      if (aqString.Find(strObjectName,"&&")!= -1){
        strInputValue=g_sProjPath+"\Stores\\Files\\"+strInputValue;
        strObjectName = ReplaceAll(strObjectName,"\"","");
        strObjectName = strObjectName + "&&ObjectType&&Edit"
        var Browser = "iexplore" 
        var strParent = "Sys.Browser("+chr(34)+Browser+chr(34)+").Dialog("+chr(34)+"Choose File to Upload"+chr(34)+")"; 
        objName = BuildFindChild(strParent,strObjectName)        
        objName = WaitForObject(objName, true);        
        Sys.Refresh();
        objName.Keys(strInputValue);
        
        ExpectedMessage="Filepath shall be entered in the Filename textbox"
        ActualMessage="Filepath is entered in the Filename textbox"
        
        //Retry by clicking on the object and then typing value. 
        if(!CompareText(GetText(objName),strInputValue)){
          objName.Click();
          objName.Keys(strInputValue);
        }
        
        if(!CompareText(GetText(objName),strInputValue)){
          ActualMessage="Filepath is not entered in the Filename textbox"
          intStepCounter = 1;
        }

        Wait(1);
        strParent = WaitForObject(strParent,true)
        strParent.FindChild("WndCaption","&Open", 1000).Click();
      }else{
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        //To get timestamp of treatment file upload and write in excel sheet
        if (CompareText(strObjectName,"Treatment_ProcessURL")) {
            WriteTreatmentuploadTimestamp(intIniRwNo);   
        } 
      } 
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt); 
      }
    }  
    g_stepnum = strStep;
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{
    g_TreatmentFileUpload = false;     
    SwitchBrowser("Primary"); 
  }
   
}

//*************************************************************************************************
// Function Name        : LaunchNotepad
// Function Description : To launch notepad++ and find 200 and replace        
// Inputs               : None     
// Returns              : None
//*************************************************************************************************
function LaunchNotepad(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strUserName ;      //Variable for storing UserName
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = g_dicColNames_obj.item(strObjectName);  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol 
        
          
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      }
    }   
  }  
  catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  } 
}
//*************************************************************************************************
// Function Name        : TextfileSearch
// Function Description : To search text in a text file                   
// Inputs               : None     
// Returns              : None
//*************************************************************************************************
function TextfileSearch(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    var ForReading = 1;  //Constant to denote open file in read mode
    var objfs;  //Variable to store file system object
    var objf;  //Variable to store text file
    var PassCounter = 0;  //Variable to store the pass/fail counter
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      ExpectedMessage = ReadExcel(intIterCnt,c_VerifnPointExpected);  //Read expected result from excel 
      ActualMessage = ReadExcel(intIterCnt,c_VerifnPointActual);  //Read actual pass result from excel
      //Creates a new file object    
      objfs = Sys.OleObject("Scripting.FileSystemObject");
      if(intIterCnt == intIniRwNo){  //Reads file name from initial row
        if(aqFileSystem.Exists(strInputValue)){      //Check if the file exists  
          objf = aqFile.OpenTextFile(strInputValue, aqFile.faRead ,aqFile.ctANSI);  //Open the text file       
          var strText =  objf.ReadAll();
          PassCounter = PassCounter + 0;
        }
        else{
          PassCounter = PassCounter + 1;
          Log.Message ("File does not exist");
        }
      }
      if(intIterCnt == intEndRwNo){  //Searches 500 in the text file
        intStepCounter = strText.search(strInputValue);
        if(intStepCounter > 0){
          PassCounter = PassCounter + 0;
        }
        else{
          PassCounter = PassCounter + 1;
        }
      } 
      var fileName1= g_ToolsFolderPath+"\\SettingsResponse.enc";
      var filename2=g_bitmap
      aqFileSystem.CopyFile(fileName1,filename2)
      Txtfilename = g_RelativePathToBitmap+ "\\SettingsResponse.enc";
      VerificationPoint(g_strFuncCall,strStep,PassCounter,intIterCnt); 
    }
    objf.Close()
    objf = null;
    objfs = null;   
  }
  catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  } 
}

//*************************************************************************************************
// Function Name        : DateTimeFormat
// Function Description : To update date to upload treatment file                 
// Inputs               : Treatment File      
// Returns              : None  
//************************************************************************************************
function DateTimeFormat(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    g_TreatmentFileUpload = true;
    SwitchBrowser("Secondary");
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        //Read input value from excel
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
        if(strObjectName == "InterviewStartdate"){
          TodaysDate = aqDateTime.Now()
          var WriteDate =aqConvert.DateTimeToFormatStr(TodaysDate,"%d %B %Y");
          WriteExcel(intIterCnt,c_intInputVal1Col,g_TestDataSheetName,WriteDate);
        }
        //Read strStep from excel
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol    
        //This function performs the strActions specified in the excel
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      }
      if(intIterCnt==intEndRwNo-1){
        Wait(3);
        var Object=Sys.Browser("iexplore").Page("*").FindChild("ObjectLabel","Continue",100);
        if(Object.Exists){          
          Object.Click();
        }
      } 
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
    }   
    
    g_stepnum = strStep;
  }
  catch(e){
  Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  finally{
    g_TreatmentFileUpload = false ;
    SwitchBrowser("Primary");
  } 
}
//*************************************************************************************************
// Function Name        : LaunchWebAppInOtherBrowser
// Function Description : To Launch the Web Application Url in Desired Browser
// Inputs               : None
// Returns              : None
////************************************************************************************************
function LaunchWebAppInOtherBrowser(){  
  try{
    var strStep = ReadExcel(g_intStrtRow,c_intStepCol);  //Reads the step from c_intStepCol
    var intStepCounter = 1;   
    g_strBrowserName1="iexplore";        
    SecondBrowser =  btIExplorer;
    if(CompareText(g_strBrowserName,"IE")){
     g_strBrowserName1="Firefox";         
     SecondBrowser =  btFirefox;
    }
   for(i=0;i<5;i++){      
      Browsers.Item(SecondBrowser).Run(g_strWebAppURL);  
      if(Sys.WaitBrowser(g_strBrowserName1).Exists){ 
        page_url = Sys.Browser(g_strBrowserName1).Page("*").URL;
        if (aqString.Find(page_url, g_strWebAppURL) != -1){
          intStepCounter = 0;
          Sys.Refresh();
          Sys.Browser(g_strBrowserName1).BrowserWindow(0).Maximize();            
          break;
        } 
      } 
   }      
  SwitchBrowser("Secondary"); 
  Expected = "User shall be navigated to "+g_strWebAppURL+" successfully";       
  if (intStepCounter == 0){    
    Actual = "User is navigated to "+g_strWebAppURL+" succeessfully"; 
    if (CompareText(g_strBrowserName1,"iexplore")){
        //Close the Notification bar
        var NotificationBar = Sys.Browser(g_strBrowserName1).BrowserWindow(0).FindChild("Caption", "Notification", 1000);
        if (NotificationBar.Exists && NotificationBar.Visible){
            NotificationBar.FindChild("Caption","Close",5000).Click()
        }  
    } 
     //Click on Accept invalid SSL link if it appears on the screen
     AcceptInvalidSSL();     
  }else{    
    Actual ="User is not navigated to "+g_strWebAppURL;      
  }      
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    Actual = "User is not navigated to "+g_strWebAppURL+" due to the exception "+e.description;
  }
  finally{
    if(intStepCounter == 1){
      StopCurrentTest();
    }
    SwitchBrowser("Primary");
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,g_intStrtRow);
    g_stepnum = strStep;
  }     
}



//*************************************************************************************************
// Function Name        : ClinicAdminSearchAndRoleDisable
// Function Description : To search a clinic admin and disable the admin role for a clinic.                 
// Inputs               : None      
// Returns              : None
//*************************************************************************************************
function ClinicAdminSearchAndRoleDisable(){
  try {
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    g_strBrowserName1="iexplore";      
    if(CompareText(g_strBrowserName,"IE")){
      g_strBrowserName1="Firefox";         
    }  
    SwitchBrowser("Secondary");
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
      //This function performs the strActions specified in the excel 
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
    }
    g_stepnum = strStep;  
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  finally{
    SwitchBrowser("Primary");
  } 
}



//*************************************************************************************************
// Function Name        : EditPreferences
// Function Description : To edit language,primary clinic and email preferences                   
// Inputs               : None     
// Returns              : None
//*************************************************************************************************
function EditPreferences(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol    
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      }
    }  
     g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : LaunchTreatmentFile
// Function Description : To launch the treatment file URL
// Inputs               : None
// Returns              : None
//************************************************************************************************
function LaunchTreatmentFile(){
  try{  
    var IEChildObj = Sys.FindChild("ProcessName","iexplore");  //Variable for storing process name
    if (IEChildObj.exists){
      IEChildObj.Terminate(); //Terminate the process
    }
    g_TreatmentFileUpload = true;
    var strStep = ReadExcel(g_intStrtRow,c_intStepCol);  //Reads the step from c_intStepCol
    var strStepType = ReadExcel (g_intStrtRow,c_intStepType); //Reads the step type from c_intStepType
    var intStepCounter = 1; //Variable to store pass/fail count
    var TreatmentFileURL = GetDicParam("TreatmentFileURL");
    for(i=0;i<5;i++){      
      Browsers.Item(btIExplorer).Run(TreatmentFileURL);   
      var BrowserProcess = Sys.WaitBrowser("iexplore")
      if(BrowserProcess.Exists){ 
        SwitchBrowser("Secondary");
        if (aqString.Find(oPageObject.URL, TreatmentFileURL) != -1){
          intStepCounter = 0;    
          BrowserProcess.BrowserWindow(0).Maximize();        
          break;
        }  
      }
    }            
    if (intStepCounter == 0){    
      Actual = "User is navigated to "+TreatmentFileURL+" succeessfully";                       
      //Close the Notification bar
      var NotificationBar = BrowserProcess.BrowserWindow(0).FindChild("Caption", "Notification", 1000);
      if (NotificationBar.Exists && NotificationBar.Visible){
          NotificationBar.FindChild("Caption","Close",5000).Click()
      }   
      //Click on Accept invalid SSL link if it appears on the screen
      AcceptInvalidSSL();  
    }else{    
      Actual ="User is not navigated to "+TreatmentFileURL;      
    }      
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    Actual = "User is not navigated to "+TreatmentFileURL+" due to the exception "+e.description;
  }finally{
    if(intStepCounter == 1){
      StopCurrentTest();
    }
    g_TreatmentFileUpload = false;
    SwitchBrowser("Primary");
    g_stepnum = strStep;
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,g_intStrtRow);
  }      
}
//*************************************************************************************************
// Function Name        : XMLResponseFindValue
// Function Description : To edit language,primary clinic and email preferences                   
// Inputs               : None     
// Returns              : None
//*************************************************************************************************

function XMLResponseFindValue(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    
    
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){ //for
        strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        arrayfirst = strObjectName.split("&&")
        var prop=arrayfirst[0];
        var prop_value=arrayfirst[1];
        var prop1="FindChild("+prop+","+prop_value+","+" 1000"+")";
        var strParent = "Sys.Browser("+chr(34)+g_strBrowser+chr(34)+").Page("+chr(34)+g_strFormattedUrl+"*"+chr(34)+")";         
        objName = strParent+"."+prop1; 
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol 
        strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
        var objDesc = WaitForObject(objName, true);
        if ((objDesc.Exists) && (aqString.Find(objDesc.contentText, "clearButton") != -1)){
          if (aqString.Find(objDesc.contentText, strInputValue) != -1){
            ActualMessage= strInputValue + " is displayed on the screen";
            ExpectedMessage= strInputValue + " shall be displayed on the screen";
            fnInsertResult(g_strFuncCall,strStep,ExpectedMessage,ActualMessage,"PASS",strStepType); 
         }else {
            ActualMessage= strInputValue + " is not displayed on the screen";
            ExpectedMessage= strInputValue + " shall be displayed on the screen";
            fnInsertResult(g_strFuncCall,strStep,ExpectedMessage,ActualMessage,"FAIL",strStepType); 
            g_intFailCnt = g_intFailCnt + 1;
          }             
        }
        else {  
          ActualMessage=  strInputValue + "  is not displayed on the screen";
          ExpectedMessage= strInputValue + "  shall be displayed on the screen";
          fnInsertResult(g_strFuncCall,strStep,ExpectedMessage,ActualMessage,"FAIL",strStepType); 
          g_intFailCnt = g_intFailCnt + 1;
        }                
      }
    }
    g_stepnum = strStep;
  }
  catch(e){
  Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest); 
  }   
}
//*************************************************************************************************
// Function Name        : SaveDecryptedFile
// Function Description : To save the decrypted file in the project location                   
// Inputs               : None     
// Returns              : None
//*************************************************************************************************

function SaveDecryptedFile(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = (g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol 
        if (aqString.Find(strObjectName,"&&")!= -1){
          strInputValue=g_sProjPath+"\Stores\\Files\\"+strInputValue;  //append the input value with project path
          arrayfirst = strObjectName.split("&&")
          var prop=arrayfirst[0];
          var prop_value=arrayfirst[1];
          var prop1="FindChild("+prop+","+prop_value+","+" 1000"+")";
          var Browser = "iexplore" 
          var strParent = "Sys.Browser("+chr(34)+Browser+chr(34)+").Dialog("+chr(34)+"*"+chr(34)+")";          
          objName = strParent+"."+prop1;  //Build object from the given object nameect from the given object name
        }     
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);  
        
      }
    } 
    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : ClickLink
// Function Description : To Click the Edit link in patient search or to click Remove/Disable link in user search               
// Inputs               : None   
// Returns              : None      
//************************************************************************************************
function ClickLink(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol   
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      //Read strStep from excel
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
      //Read strAction from excel
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol     
      if (strObjectName != null){
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));        
      }
      
      
      if (strObjectName == "Help_OrderMgmtDemoLink"){
       Sys.Keys("[PageDown]");   //To scroll down for taking the screenshot of Help Links.
      }  
        
     intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep); 
     VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
    }  
    g_stepnum = strStep;   
    }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }   
}

//*************************************************************************************************
// Function Name        : CreateOrEditPatient
// Function Description : To create a new patient                   
// Inputs               : None     
// Returns              : None
//*************************************************************************************************
function CreateOrEditPatient(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterationCnt = intIniRwNo ; intIterationCnt <= intEndRwNo ; intIterationCnt++){ 
      strObjectName = ReadExcel(intIterationCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        strInputValue = ReadExcel(intIterationCnt,c_intInputValCol);  //Reads the input value rom c_intInputValCol    
        strStep = ReadExcel(intIterationCnt,c_intStepCol);  //Reads the step from c_intStepCol    
        strAction = ReadExcel(intIterationCnt,c_intActionCol);  //Reads the action from c_intActionCol   
        strStepType = ReadExcel (intIterationCnt,c_intStepType); //Reads the step type from c_intStepType
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName)); 
        if (strObjectName == "AddPatient_FirstName") {  //If the object name is AddPatient_FirstName write the generated new patient name in excel sheet
          WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheetName,strInputValue)
        }            
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        
        
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterationCnt);
      }
    }  
    
    g_stepnum = strStep;
    } 
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : SearchPatient
// Function Description : To search Patient from patient search criteria               
// Inputs               : Patient Name      
// Returns              : None
//**************************************************************************************************
function SearchPatient(){
  try{
    var intIniRwNo;       //Variable for storing initial row number
    var intEndRwNo;       //Variable for storing end row number
    var strObjectName;    //Variable for storing object name
    var objName;          //Variable for storing object 
    var strInputValue;    //Variable for storing input value 
    var strAction;        //Variable for storing action 
    var strUserName;      //Variable for storing UserName
    var intStepCounter = 1  //Variable for storing flag  
    //Reads initial row and end row for a keyword
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){ 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value rom c_intInputValCol    
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol    
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol     
      if (strObjectName != null){
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));        
      }   
      //This function performs the strActions specified in the excel 
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      
    } 
    WriteBaxterpatientId(intEndRwNo);
    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : NewAccountActivation
// Function Description : To activate user account by creating new password, terms and conditions acceptance and creating new security questions.              
// Inputs               : Activation Link      
// Returns              : None
//**************************************************************************************************
function NewAccountActivation(){
try{
  var intIniRwNo;       //Variable for storing initial row number
  var intEndRwNo;       //Variable for storing end row number
  var strObjectName;    //Variable for storing object name
  var objName;          //Variable for storing object 
  var strInputValue;    //Variable for storing input value 
  var strAction;        //Variable for storing action 
  var strUserName;      //Variable for storing UserName
  var intStepCounter = 1  //Variable for storing flag  
  //Reads initial row and end row for a keyword
  intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
  intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
  for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){ 
    strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value rom c_intInputValCol    
    strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol    
    strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol     
    if (strObjectName != null){
      //Build object
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));        
    }   
    //This function performs the strActions specified in the excel 
    intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
    
    
    } 
    g_stepnum = strStep;
  }
  catch(e){
  Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : SecondaryMenuVerify
// Function Description : To Verify existence of Secondary menu                  
// Inputs               : Secondaru menu name     
// Returns              : None
//*************************************************************************************************
  function SecondaryMenuVerify(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing Action
    var intStepCounter = 1;    //Variable for storing flag
    var objnotexist = 0;   //Variable to verify if the object exist
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    //Read object name from excel 
    for (intCounter = intIniRwNo; intCounter <= intEndRwNo; intCounter++){
      strObjectName = ReadExcel(intCounter,c_intObjNameCol);
      //Read browser input from excel
      strInputValue = ReadExcel(intCounter,c_intInputValCol);
      strAction = ReadExcel(intCounter,c_intActionCol);
      //Read strStep from excel
      strStep = ReadExcel(intCounter,c_intStepCol);     
      //Build object
      ArryObject = BuildWebObj(g_dicColNames_obj.item(strObjectName));
      var Object = WaitForObject(ArryObject, true);
      if(Object.Exists){
        ChildObj = Object.FindAllChildren("objectType","Link",50);  //Find all the child objects of the type Link
        ChildObj = new VBArray(ChildObj).toArray();
        for(loopcounter = 0; loopcounter < ChildObj.length;loopcounter++){ 
        //Check the secondary menu name which is given as inputvalue            
        if (aqString.Compare((ChildObj[loopcounter].contentText), strInputValue, false) == 0){
          ExpectedMessage=strInputValue+" shall exist on the Screen";
          ActualMessage=strInputValue+" exist on the Screen";
          intStepCounter = 0;
          break;    
          }  
        else{
          intStepCounter = 1;
        }          
      }
     
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intCounter);
    }         
       else{
          Log.Message("Object does not exist");
          intStepCounter = 1;         
       }
    }
  g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//'*************************************************************************************************
//' Function Name        : VerifyContentClncSettings
//' Function Description : To verify content available in the Clinic Settings Landing page                 
//' Inputs               : None
//' Returns              : None            
//'*************************************************************************************************
  function VerifyContentClncSettings(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intCounter = intIniRwNo; intCounter <= intEndRwNo; intCounter++){
      //Read object name from excel 
      strObjectName = ReadExcel(intCounter,c_intObjNameCol);  //Reads the object name from c_intObjNameCol   
      strInputValue = ReadExcel(intCounter,c_intInputValCol);  //Reads the input value from c_intInputValCol
      //Read strStep from excel
      strStep = ReadExcel(intCounter,c_intStepCol);  //Reads the step from c_intStepCol
      //Read strAction from excel
      strAction = ReadExcel(intCounter,c_intActionCol);  //Reads the action from c_intActionCol  
      if (strObjectName != null){
        //Build object
          Objname = BuildWebObj(g_dicColNames_obj.item(strObjectName));    
          var Object = WaitForObject(Objname, true);
          if (Object.Exists){
            Object = Object.ContentText; 
          } else {
            Object = "Error";
          }
          if(aqString.Find(Object,strInputValue)!= -1){ // verify the input value against application
          intStepCounter = 0;
          }
          else {
          intStepCounter = 1;
          }
          VerificationPoint(g_strFuncCall,strStep,intStepCounter,intCounter);
        }   
      g_stepnum = strStep;
    }
  }
catch(e){
  Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }     
}


function NameListSort(){
var strStep = ""; 
 var strStepType;       //Variable for storing step type
 var intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
 var intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
 var strInputValue;
 var customerlink  = BuildWebObj(g_dicColNames_obj.item("CustomerService_Link"));
 customerlink = WaitForObject(customerlink, true);
  for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
  strStep = ReadExcel(intIterCnt,c_intStepCol);
  strInputValue = ReadExcel(intIterCnt,c_intInputValCol); 
  strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
var Actualarray =  new Array();
var returnarray =  new Array();

try{
if(aqString.Compare(strInputValue,"Ascending", false) == 0){ 
var ExpArray = new Array();

customerlink.Click();
Delay(10000);
var RecordCount = BuildWebObj(g_dicColNames_obj.item("PatientList_RecordCount"));  //Build object from the given object name
//var Ct = Sys.Browser("iexplore").Page("*").Panel("wrapper").Panel("content").Panel("main_content").Panel(0).Panel("column_1").Panel("layout_column_column_1").Panel("*").Panel(0).Panel(0).Panel(0).Panel(0).Panel(1).Panel("form_layout").Form("patientSearchForm").Panel(1).Panel(0).Panel(1).Panel(0);
var linkcount = WaitForObject(RecordCount, true);
var pagecount = linkcount.ChildCount;

for (var i=0;i<=pagecount-2;i++){
 returnarray = internalsort();
            
            Log.Message(returnarray);
            ExpArray = ExpArray.concat(returnarray);
             returnarray = [];              
}
      
       ExpArray=ExpArray.sort();    
       Log.Message(ExpArray);
   
}         

customerlink.Click();
Delay(8000);
var Sortlink  = BuildWebObj(g_dicColNames_obj.item("PatientName_SortingLink"));
Sortlink = WaitForObject(Sortlink, true);
Sortlink.Click();
Delay(8000);
if(aqString.Compare(strInputValue,"Descending", false) == 0){
Sortlink.Click();
ExpArray=ExpArray.reverse();
Delay(8000);
}

//#####################
for (var i=0;i<=pagecount-2;i++){
 returnarray = internalsort();
            
            Log.Message(returnarray);
            Actualarray = Actualarray.concat(returnarray);
             returnarray = [];               
}
var  passcounter = 0;
//Comparison
if(Actualarray.length = ExpArray.length){
for(i=0;i<ExpArray.length;i++){

if(aqString.Compare(Actualarray[i],ExpArray[i], false) == 0){
passcounter = passcounter + 0
}
else{
Log.Message("Fail")
passcounter = passcounter + 1
}
}
}else{
Log.Message("Fail");
passcounter = passcounter + 1
}
    if (passcounter == 0){
 
      //insert result
      fnInsertResult("NameListSorting",strStep,strInputValue+" Sorted list shall be " +ExpArray,strInputValue+" Sorted list is" +Actualarray,"PASS",strStepType);
    }     
    else{

      fnInsertResult("NameListSorting",strStep,strInputValue+" Sorted list shall be " +ExpArray ,strInputValue+" Actual list is "+Actualarray,"FAIL",strStepType);
      g_intFailCnt = g_intFailCnt + 1;
    }

g_stepnum = strStep;

}
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }

function internalsort(){
var valuearray = new Array();
var splitarray = new Array();
var Sortedtable = BuildWebObj(g_dicColNames_obj.item("PatientList_Table"));  //Build object from the given object name 
//var Sortedtable = Sys.Browser("iexplore").Page("*").Panel("wrapper").Panel("content").Panel("main_content").Panel(0).Panel("column_1").Panel("layout_column_column_1").Panel("*").Panel(0).Panel(0).Panel(0).Panel(0).Panel(1).Panel("form_layout").Form("patientSearchForm").Panel(1).Table("patient");
var Sortedtableoject = WaitForObject(Sortedtable, true);
var  rows = Sortedtableoject.rows.length;
for (var r = 1;r < rows; r++) {  
      
      valuearray[r-1] = Sortedtableoject.Cell(r, 0).innerText;
           splitarray = valuearray[r-1].split(",");
           splitarray = splitarray[0].split(" ");
           valuearray[r-1] = splitarray[splitarray.length-1].toLowerCase()
            }
            Delay(3000);
            if(Sys.Browser(g_strBrowserName).Page("*").FindChild("className","next",100000).Exists){
            Sys.Browser(g_strBrowserName).Page("*").FindChild("className","next",100000).Click();
            Delay(8000);
            }
            return valuearray;              

}
}
}
function Patientfilter(){
try{
var intIniRwNo;        //Variable for storing initial row number
  var intEndRwNo;        //Variable for storing end row number
  var strObjectName;     //Variable for storing object name
  var objName;           //Variable for storing object 
  var strInputValue;     //Variable for storing input value 
  var strAction;         //Variable for storing action 
  var strUserName ;      //Variable for storing UserName
  var strStep = "";           //Variable for storing step number
  var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
  var strLoginUser;
  var strStepType;       //Variable for storing step type
  intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
  intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

  for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
    strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    if (strObjectName != null){
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      if(intIterCnt == intIniRwNo){
        strInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);  //Reads the input value from c_intInputValCol
        strLoginUser = strInputValue1 + "(" +strInputValue + ")";
      }
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
      //This function performs the strActions specified in the excel 
      strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      var Table = BuildWebObj(g_dicColNames_obj.item("PatientList_Table"));
      var tableoject = WaitForObject(Table, true);
      var  rows = tableoject.rows.length;
      var Records = BuildWebObj(g_dicColNames_obj.item("Patientlist_NoRecordstext"));
      Records = WaitForObject(Records, true);
      if((rows!=0)||(aqString.Find(Records.ContentText,strInputValue)!= -1)){
      fnInsertResult("Patientfilter",strStep,"Patient shall be filtered based on "+strObjectName,"Patient list is filtered based on" +strObjectName,"PASS",strStepType);
      }else{
      fnInsertResult("Patientfilter",strStep,"Patient shall be filtered based on "+strObjectName,"Patient list is not filtered based on" +strObjectName,"FAIL",strStepType);
      }
      }
  }  
  
  g_stepnum = strStep;
  }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 } 

}

function PatientListSearch_LinkNavigations(){
try{
  var intIniRwNo;        //Variable for storing initial row number
  var intEndRwNo;        //Variable for storing end row number
  var strObjectName;     //Variable for storing object name
  var objName;           //Variable for storing object 
  var strInputValue;     //Variable for storing input value 
  var strAction;         //Variable for storing action 
  var strStep = "";           //Variable for storing step number
  var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
  intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
  intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
  for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo; intIterCnt++){
    strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    //Build object
   
    strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    //Build object
    VerificationObj = BuildWebObj(g_dicColNames_obj.item(strObjectName));
    strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol      
    strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
    strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol    
    //This function performs the strActions specified in the excel
    intStepCounter = DataEntry(strAction,VerificationObj,strInputValue,strObjectName,strStep);
    if(aqString.Compare("Click",strAction, false) == 0){
    Delay(6000);
    }
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt); 
  }  
 g_stepnum = strStep; 
}

 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//************************************************************************************************
// Function Name        : ClickPatientLink
// Function Description : To click on Patient link and navigate to Patient page               
// Inputs               : None     
// Returns              : None
//**************************************************************************************************
function ClickPatientLink(){
try{
  var intIniRwNo;       //Variable for storing initial row number
  var intEndRwNo;       //Variable for storing end row number
  var strObjectName;    //Variable for storing object name
  var VerificationObj;          //Variable for storing object 
  var strInputValue;    //Variable for storing input value 
  var strAction;        //Variable for storing action 
  var strStep ;         //Variable for storing Step number
  var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)     
  intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
  intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword  
  for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
    strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);//Reads input value from c_intInputValCol 
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol  
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol    
      if (strObjectName != null){
        //Build object
        VerificationObj = BuildWebObj(g_dicColNames_obj.item(strObjectName));        
      }         
      //This function performs the strActions specified in the excel 
      intStepCounter = DataEntry(strAction,VerificationObj,strInputValue,strObjectName,strStep); 
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
    } 
    
    WritepatientId(intEndRwNo);

  g_stepnum = strStep;
   }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
} 


//*************************************************************************************************
// Function Name        : ExecuteQuery
// Function Description : To execute a query in database                 
// Inputs               : Query  
// Returns              : None  
//**************************************************************************************************
function ExecuteQuery(){

  try{
  
    var DBConnection;      //variable for storing connection
    var RecordSet;           //variable for storing recordset
    var NumOfRecord = -1;      //Variable to store number of records after query is executed. Initializing to a value less than Zero
    var ExpectedResult;
    var ActualResult;
    var StepStatus = "FAIL";
    var RecordValue = null;          //Variable to store the value of the record
    
    var intIniRwNo;       //Variable for storing initial row number
    var intEndRwNo;       //Variable for storing end row number
    var strObjectName;    //Variable for storing object name
    var VerificationObj;          //Variable for storing object 
    var strInputValue;    //Variable for storing input value 
    var strAction;        //Variable for storing action 
    var strStep ;         //Variable for storing Step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)     
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
    OutputFileName = ReplaceAll(strStep + "-" + GetTimeStamp() + "-DatabaseOutput.txt", " " , "");
    var FilePath = g_bitmap+ "\\" + OutputFileName;
    GeneratedPDFReportTextFile = g_RelativePathToBitmap+ "\\" + OutputFileName;
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
        strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);//Reads input value from c_intInputValCol
        strInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);//Reads input value from c_intInputValCol 
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol  
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
        strStepType = ReadExcel(intIterCnt,c_intStepType);  //Reads the step from c_intStepCol
        ActualText = null;    
        
    try{
           //Execute the Query on the first row alone.
          if (intIterCnt == intIniRwNo){
                try{
                  ExpectedResult = "SQL Query shall be executed. <BR>"+chr(34)+strInputValue+chr(34);
                  ActualResult = "SQLQuery was not executed. <BR>"+chr(34)+strInputValue+chr(34);
                  WriteExcel(intIterCnt,c_intInputVal1Col,g_TestDataSheetName,"NULL");//Write NULL before running the Query.
                  //Configure the Connection object and establish the connection
                  DBConnection = ADO.CreateADOConnection();
                  DBConnection.ConnectionString ="Provider=OraOLEDB.Oracle;"+"Data Source=(DESCRIPTION =(ADDRESS_LIST =(ADDRESS =(PROTOCOL = TCP)(HOST = "+g_DBHost+")(PORT = "+g_DBPort+")))(CONNECT_DATA =(SERVICE_NAME = "+g_DBServiceName+")));" +"User ID="+g_DBUserName+";"+"Password="+g_DBPassword+";"; 
                  DBConnection.LoginPrompt = false;
                  DBConnection.Open();
                  // Execute a query    
      if(aqString.Find(strInputValue,"DBTablename")!= -1){
        strInputValue = aqString.Replace(strInputValue,"DBTablename",g_DBTablename);
      }
                  RecordSet = DBConnection.Execute_(strInputValue);
                  RecordCount = RecordSet.RecordCount; 
                  if(RecordCount > 0){
                      ActualText = RecordSet.Fields(strObjectName).Value;
          StepStatus = "PASS";
                      WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheetName,ActualText);//Write the return value from the query in excel sheet
                      ActualResult = "SQL Query is executed sucessfully and fetched the value '"+ActualText+"' for " + strObjectName;
          Log.Message(strStep + " - " + ActualResult);
                      aqFile.WriteToTextFile(FilePath, RecordSet.GetString("2", 1000 , "|", "\n" , "NULL") , aqFile.ctUTF8, true);
      }else{
        ActualResult = "SQL Query is executed sucessfully but it fetched no value for " + strObjectName;    
        Log.Error(strStep + " - " + ActualResult);    
      }  
    }catch(oError){      
      ActualResult = "Unable to execute SQL Query due to the Error. <BR>"+chr(34) + oError.description + chr(34);
      Log.Error(strStep + " - " + ActualResult);
      aqFile.WriteToTextFile(FilePath, "Unable to execute SQL Query due to the Error: " + oError.description  , aqFile.ctUTF8, true);
    }
      //Verify the Database values for all other rows
      }else{
          ExpectedResult = "DB value for the column '"+strObjectName+ "', Row number '"+strInputValue1+ "' shall be '"+strInputValue+"'";
          ActualResult = "DB value for the column '"+strObjectName+  "', Row number '"+strInputValue1+ "' is not '"+strInputValue+"'";
          RecordSet.Move(strInputValue1-1, 1); //Move to the desired row from the beginning of the recordset
          try {
              ActualText = RecordSet.Fields(strObjectName).Value;   //Reads the db record value for the given column name
          }catch(Ignore){}              
          if (CompareText(strInputValue,ActualText)){
            StepStatus = "PASS";
            ActualResult = "DB value for the column '"+strObjectName+ "', Row number '"+strInputValue1+  "' is '"+strInputValue+"'";
          }else{        
            StepStatus = "FAIL";                
          }   
          Log.Message(strStep + " - " + ActualResult);       
      } 
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
        ActualResult = "Unable to execute SQL Query due to the Error. <BR>"+chr(34) + oError.description + chr(34);
        Log.Error(strStep + " - " + ActualResult);
        aqFile.WriteToTextFile(FilePath, "Unable to execute SQL Query due to the Error: "+ oError.description  , aqFile.ctUTF8, true);
  }finally{
        fnInsertResult("ExecuteQuery",strStep,ExpectedResult,ActualResult,StepStatus,strStepType);
    if(StepStatus == "FAIL"){      
      StopCurrentTest();
    }
    }    
   } //For Loop Ends   
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
      ActualResult = "Step failed due to the exception "+e.description;
      aqFile.WriteToTextFile(FilePath, ActualResult , aqFile.ctUTF8, true);
    fnInsertResult("ExecuteQuery",strStep,ExpectedResult,ActualResult,StepStatus,strStepType);
  }finally{
      g_stepnum = strStep;
      GeneratedPDFReportTextFile = "";
    try{    
        DBConnection.Close();
    }catch(Ignore){}
  }
}


//*************************************************************************************************
// Function Name        : UpdateQuery
// Function Description : To execute a query in database                 
// Inputs               : Query  
// Returns              : None  
//**************************************************************************************************
function UpdateQuery(){

    try{

        var intIniRwNo;       //Variable for storing initial row number
        var strInputValue;    //Variable for storing input value
        var AConnection, RecSet; //variable for storing connection and recordset
        intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
        
        strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);
        var strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol
        var strStepType = ReadExcel(intIniRwNo, c_intStepType);  //Reads the step from c_intStepType
        var ExpectedResult = "SQL Query shall be executed. <BR> " + strInputValue;
        var ActualResult = "SQL Query is not executed. <BR> " + strInputValue;
        var StepStatus = "FAIL";
        
        try{
            // Create a Connection object
            AConnection = ADO.CreateADOConnection();
    
            // Specify the connection string  
            AConnection.ConnectionString ="Provider=OraOLEDB.Oracle;"+"Data Source=(DESCRIPTION =(ADDRESS_LIST =(ADDRESS =(PROTOCOL = TCP)(HOST = "+g_DBHost+")(PORT = "+g_DBPort+")))(CONNECT_DATA =(SERVICE_NAME = "+g_DBServiceName+")));" +"User ID="+g_DBUserName+";"+"Password="+g_DBPassword+";"; 
    
           //Suppress the login dialog box
            AConnection.LoginPrompt = false;
            AConnection.Open();
    
	          if(aqString.Find(strInputValue,"DBTablename")!= -1){
      		    strInputValue = aqString.Replace(strInputValue,"DBTablename",g_DBTablename);
      	    }	    	    
	
            // Execute a simple query
            AConnection.Execute_(strInputValue);
            
            ActualResult = "SQL Query is executed. <BR> " + strInputValue;
            StepStatus = "PASS";
            Log.Message(strStep+" - "+ActualResult);
        }catch(oError){  
            StepStatus = "FAIL";    
            ActualResult = "Unable to execute SQL Query due to the Error. <BR>"+chr(34) + oError.description + chr(34);
            Log.Error(strStep + " - " + ActualResult);
        }

    }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }finally{
        if(StepStatus == "FAIL"){ 
          g_intFailCnt = g_intFailCnt + 1; 
          intRowIteration_TC = intMaxRowCnt_TC+ 1000;
          intIniRwNo = 1000;
          StepStatus = "FAIL";
        }
        fnInsertResult("UpdateQuery",strStep,ExpectedResult,ActualResult,StepStatus,strStepType);
        g_stepnum = strStep;
        try{    
          AConnection.Close();
        }catch(Ignore){}        
    }
 
 } 
 

 //*************************************************************************************************
// Function Name        : SelectDeliveryDate
// Function Description : To navigate to expected month and year of a datePick control                  
// Inputs               : Month,Year  
// Returns              : None  
//**************************************************************************************************
function SelectDeliveryDate(){
try{
 var intIniRwNo;        //Variable for storing initial row number
  var intEndRwNo;        //Variable for storing end row number
  var strObjectName;     //Variable for storing object name
  var objName;           //Variable for storing object 
  var strInputValue;     //Variable for storing input value 
  var strAction;         //Variable for storing action 
  var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
  var strStepType;       //Variable for storing step type
  intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
  strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
    var day = new Date(strInputValue);
    var newdate = day.toString(); 
    //to get the date in expected format
    Arrayinput = newdate.split(" ");
    var inputdate = Arrayinput[2];
    //Read object name from excel 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
    //Read strAction from excel
    strAction = ReadExcel(intIniRwNo,c_intActionCol);  //Reads the action from c_intActionCol     
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    objArry = BuildWebObj(g_dicColNames_obj.item(strObjectName)) //Build object from the given object name  
    objName = WaitForObject(objArry, true);
    //find ChildObject using finall method
    ChildObj = objName.FindAllChildren("objectType","Cell", 30);
    ChildObj = (new VBArray(ChildObj)).toArray();
    //Click on a particular date
    for(var intCounter = 0; intCounter <= (ChildObj.length)- 1; intCounter++){
      if((Trim(ChildObj[intCounter].contentText) == Trim(inputdate))){
        ChildObj[intCounter].Click();
        intStepCounter = 0;
        break;          
        }
      }
    if(intStepCounter == 0){    
      fnInsertResult("SelectDeliveryDate",strStep,"User shall be able to enter a date","User is entered a date ","PASS",strStepType);
    }
    else{    
      fnInsertResult("SelectDeliveryDate ",strStep,"User shall be able to enter a date","User is not entered a date","FAIL",strStepType);
      g_intFailCnt = g_intFailCnt + 1; 
    }
    g_stepnum = strStep; 
    }

catch(e){
Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
}
}

//*************************************************************************************************
// Function Name        : VerifyShipment
// Function Description : To verify the shipment in delivery calender               
// Inputs               : Input Value
// Returns              : None  
//**************************************************************************************************
function VerifyShipment(){
try{
 var intIniRwNo;        //Variable for storing initial row number
  var intEndRwNo;        //Variable for storing end row number
  var strObjectName;     //Variable for storing object name
  var objName;           //Variable for storing object 
  var strInputValue;     //Variable for storing input value 
  var strAction;         //Variable for storing action 
  var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
  var strStepType;       //Variable for storing step type
  intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
  strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
  strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
  strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
  strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    //find ChildObject using finall method
    objArry = BuildWebObj(g_dicColNames_obj.item(strObjectName))
    objName = WaitForObject(objArry, true);
    ChildObj = objName.FindAllChildren("objectType","Cell", 30);
    ChildObj = (new VBArray(ChildObj)).toArray();
      for(var intCounter = 0; intCounter <= (ChildObj.length)- 1; intCounter++){
      var Object = Trim(ChildObj[intCounter].innerHTML);
      if(aqString.Find(Object,strInputValue)!= -1){
        intStepCounter = 0;
        break;          
        }
      }
      
      if(intStepCounter == 0){    
        fnInsertResult("VerifyShipment",strStep,"Shipment shall be available in the delivery calendar","Shipment is available in the delivery calendar","PASS",strStepType);
      }
      else{    
        fnInsertResult("VerifyShipment ",strStep,"Shipment shall be available in the delivery calendar","Shipment is not available in the delivery calenda","FAIL",strStepType);
        g_intFailCnt = g_intFailCnt + 1; 
      }
  g_stepnum = strStep; 
    }

catch(e){
Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
}
}

//************************************************************************************************
// Function Name        : CreateCompany
// Function Description : To Create Company in liferay portal              
// Inputs               : None     
// Returns              : None
//**************************************************************************************************
function CreateCompany(){
try{
  var intIniRwNo;       //Variable for storing initial row number
  var intEndRwNo;       //Variable for storing end row number
  var objName;          //Variable for storing object 
  var strInputValue;    //Variable for storing input value 
  var strAction;        //Variable for storing action 
  var strObjectName;    // Variable for storing object name
  var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)     
  
  intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
  intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword  
  for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);//Reads input value from c_intInputValCol 
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol  
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol 
     
    if (strObjectName != null){
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
      }     
      //This function performs the strActions specified in the excel 
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep); 
      
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      if (g_strObjectName == "liferay_companyname") {  //If the object name is AddPatient_FirstName write the generated new patient name in excel sheet
            var strGeneratedName = strInputValue
            WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheetName,strGeneratedName)
          } 
          //}
    } 
    
    Writesiteid(intEndRwNo);

  g_stepnum = strStep;
   }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
} 

//*************************************************************************************************
// Function Name        : Writesiteid
// Function Description : To write the site id of created company in testdata sheet            
// Inputs               : None
// Returns              : None         
//************************************************************************************************ 
function Writesiteid(intRow){
 try{
  
  //Build object
  objName = BuildWebObj(g_dicColNames_obj.item("liferay_companysiteid"));  //Build object from the given object name
  var tableObj = WaitForObject(objName, false);   
  if (!IsExists(tableObj)) {
    Log.Error("Unable to fetch Site ID");
  }    
  var strSiteID = tableObj.contentText;  //Get the Baxter patient ID
  WriteExcel(intRow,5,g_TestDataSheetName,strSiteID);  //Write Baxter patient id in testdata sheet
  }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : WritepatientId
// Function Description : To write the created patient name and Baxter patient id in testdata sheet            
// Inputs               : None
// Returns              : None         
//************************************************************************************************ 
function WritepatientId(intRow){
 try{
  
  //Build object
  objName = BuildWebObj(g_dicColNames_obj.item("BaxterPatientID"));  //Build object from the given object name
  var tableObj = WaitForObject(objName, false);  
  if (!IsExists(tableObj)) {
    Log.Error("Unable to fetch Patient ID");
  }  
  var strBaxterPID =  tableObj.contentText;  //Get the Baxter patient ID
  WriteExcel(intRow,5,g_TestDataSheetName,strBaxterPID);  //Write Baxter patient id in testdata sheet
  }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}
//*************************************************************************************************
// Function Name        : LoadSQL
// Function Description : To execute a file using database                
// Inputs               : Query  
// Returns              : None  
//**************************************************************************************************
function LoadSQL(){  
try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var strParent;
    var ForReading;
    var FS;
    var FName;
    var SQLQuery;
    var index;
    var CompanyID;
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
     Delay(50000);
     Sys.Refresh();
     Sys.Process("sqldeveloper64W").SwingObject("main-window").Activate();
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      strObjectName1 = g_dicColNames_obj.item(strObjectName);
      strParent = "Sys.Process("+chr(34)+"sqldeveloper64W"+chr(34)+").SwingObject("+chr(34)+"*"+chr(34)+")";
      objName = BuildFindChild(strParent,strObjectName1);
        
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        if(aqString.Find(strInputValue,"Stores")!= -1) { 
          strInputValue = g_sProjPath+strInputValue;  
          ForReading = 1;
          FS = Sys.OleObject("Scripting.FileSystemObject");
          FName = FS.OpenTextFile(strInputValue, ForReading);
          SQLQuery = aqFile.ReadWholeTextFile(aqConvert.VarToStr(strInputValue), aqFile.ctANSI);
          index = SQLQuery.indexOf("values", 0);
          CompanyID = SQLQuery.substr(index+8,6); 
          WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheetName,CompanyID)    
        }
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol 
        
        //This function performs the strActions specified in the excel
        Sys.Refresh();
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        if(aqString.Find(strObjectName,"SqlSelect_connectionDialogOKbtn")!= -1) { 
          Delay(28000);  
        }
        Wait(3);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);    
    }
    g_stepnum = strStep; 
   }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }   
}

//*************************************************************************************************
// Function Name        : EsignPrescription
// Function Description : To Esign a prescription assigned to a patient                               
// Inputs               : Name/surname/email of the user     
// Returns              : None
//*************************************************************************************************
function EsignPrescription(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name 
        strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol 
        strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
        if (strObjectName == "Esign_PatientList"){
	      objName = WaitForObject(objName, true);
        intStepCounter = 1;
        ChildObj = objName.FindAllChildren("ObjectType","Cell", 100);
        ChildObj = (new VBArray(ChildObj)).toArray();
       //Click on a particular patient
        for(intCounter = 0; intCounter <= (ChildObj.length)- 1; intCounter++){
        var Object = Trim(ChildObj[intCounter].contentText);
        if(aqString.Find(Object,strInputValue)!= -1){
         var intRow = ChildObj[intCounter].RowIndex;    
         objName.Cell(intRow, 5).Checkbox("therapyId").Click();
         intStepCounter = 0;
         fnInsertResult("EsignPrescription",strStep,"Patient "+strInputValue+" shall be selected for review","Patient "+strInputValue+" is selected for review","PASS",strStepType);
         break;
         }
        }
        if (intStepCounter !=0 ){  
          StopCurrentTest();   
          fnInsertResult("EsignPrescription",strStep,"Patient "+strInputValue+" shall be selected for review","Patient "+strInputValue+" is not selected for review","FAIL",strStepType);
//          VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo); 
          }
        }  
        else
        {
        
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
        }     
      }
    }  
    
    g_stepnum = strStep;
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : DeactivateandDeleteUser
// Function Description : To delete a user from customer service portal using liferay portal             
// Inputs               : None     
// Returns              : None
//**************************************************************************************************
function DeactivateandDeleteUser(){
  try{
    var intIniRwNo;       //Variable for storing initial row number
    var intEndRwNo;       //Variable for storing end row number
    var strObjectName;    //Variable for storing object name
    var objName;          //Variable for storing object 
    var strInputValue;    //Variable for storing input value 
    var strAction;        //Variable for storing action 
    var intStepCounter = 1  //Variable for storing flag  
    var strStepType;       //Variable for storing step type
    //Reads initial row and end row for a keyword
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
      strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value rom c_intInputValCol    
      strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol    
      strAction = ReadExcel(intIniRwNo,c_intActionCol);
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));
      objName = WaitForObject(objName, true);
      if(aqString.Compare(objName.Text,trim(strInputValue),false)==0)
      {
        fnInsertResult("DeactivateandDeleteUser",strStep,"The User Email Address("+objName.Text+") shall be matched with "+strInputValue+"","The User Email Address("+objName.Text+") is matched with "+strInputValue+"","PASS",strStepType);
        for (intIterCnt = intIniRwNo+1 ; intIterCnt <= intEndRwNo ; intIterCnt++){ 
        strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value rom c_intInputValCol    
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol    
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol     
        strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));
        if((strObjectName == "Liferay_DeleteOKButton")&&(aqString.Find(g_browserinfo,"Chrome")!= -1)){        
          objName = WaitForObject(objName, false);
          if(!objName.Exists){           
            objName = Sys.Browser("chrome").Dialog("*");
            strInputValue = "[Enter]";
            strAction = "EnterValue" ;
          }        
        }        
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
        
      } 
    }
    else
    {
    fnInsertResult("DeactivateandDeleteUser",strStep,"The User Email Address shall be matched with "+strInputValue+"","The User Email Address is not matched with "+strInputValue+"","Fail",strStepType);
    }
    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}
//*************************************************************************************************
// Function Name        : SiteAccessLaunch
// Function Description : To Launch the Web Application Url in Desired Browser
// Inputs               : None
// Returns              : None
////************************************************************************************************
function SiteAccessLaunch(){  
  try{   
    
    var intIniRwNo;       //Variable for storing initial row number
    var intEndRwNo;      //Variable for storing end row number      
    var strInputValue;    //Variable for storing input value 
    var strObjectName;   //Variable for storing Objectname
    var strAction;       //Variable for storing Action
    var strStep = "";          //Variable for storing Step Number
    var pageurl;
    var intStepCounter = 1;
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword   
    strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);//Reads input value from c_intInputValCol
    strAppurl = strInputValue;
    if(aqString.Find(strInputValue,"http")== -1){
     strAppurl = g_strWebAppURL+strInputValue;  //Read Web Application URL from Config file
     }
    g_strBrowserName = GetDicParam("BrowserName");   //Browser Name from Config file
    strStep = ReadExcel(g_intStrtRow,c_intStepCol);  //Reads the step from c_intStepCol
    strStepType = ReadExcel (g_intStrtRow,c_intStepType); //Reads the step type from c_intStepType
    switch (trim(g_strBrowserName)){
      case "IE":
       Browsers.Item(btIExplorer).Run(strAppurl);
       break;  
      case "Chrome":
        Browsers.Item(btChrome).Run(strAppurl);
        break;
      case "Firefox":
        Browsers.Item(btFirefox).Run(strAppurl);
        break;
      case "Safari":
        Browsers.Item(btSafari).Run(strAppurl);
        break;
      case "Opera":
        Browsers.Item(btOpera).Run(strAppurl);
        break;  
      case "Edge":
        Browsers.Item(btEdge).Run(strAppurl);
        break;     
    
    } 
      Sys.Refresh();
  
  var page_url = Sys.Browser(g_strBrowser).Page("*").URL;
  if (aqString.Compare(page_url, strAppurl, false) == 0){ 
     intStepCounter = 0;
     Expected = "User shall be navigated to "+strAppurl+" successfully";  //Read expected result from excel
     Actual = "User is navigated to "+strAppurl+" succeessfully";  //Read actual pass result from excel
     fnInsertResult("SiteAccessLaunch",strStep,Expected,Actual,"PASS",strStepType);
     
     }
     else{
     Expected = "User shall be navigated to "+strAppurl+" successfully";  //Read expected result from excel
     Actual ="User is not navigated to "+strAppurl+" successfully";  //Read actual fail result from excel
     fnInsertResult("SiteAccessLaunch",strStep,Expected,Actual,"FAIL",strStepType);
     }
  
  for (intIterCnt = intIniRwNo+1 ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);//Reads input value from c_intInputValCol 
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol  
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol 
      //strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
    if (strObjectName != null){
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
      }     
      //This function performs the strActions specified in the excel 
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep); 
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt); 
  
  }  
  }
catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
}
}


//************************************************************************************************
// Function Name        : CreateTemplate
// Function Description : To click on Patient link and navigate to Patient page               
// Inputs               : None     
// Returns              : None
//**************************************************************************************************
function CreateTemplate(){
try{
  var intIniRwNo;       //Variable for storing initial row number
  var intEndRwNo;       //Variable for storing end row number
  var strObjectName;    //Variable for storing object name
  var objName;          //Variable for storing object 
  var strInputValue;    //Variable for storing input value 
  var strAction;        //Variable for storing action 
  var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)     
  intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
  intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword  
  for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
    strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);//Reads input value from c_intInputValCol 
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol  
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol    
      if (strObjectName != null){
        //Build object
        VerificationObj = BuildWebObj(g_dicColNames_obj.item(strObjectName));        
      }         
      //This function performs the strActions specified in the excel 
      intStepCounter = DataEntry(strAction,VerificationObj,strInputValue,strObjectName,strStep); 
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
    } 
    
    g_stepnum = strStep;
   }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : GetTokenId
// Function Description : To Copy the Token ID of the page
// Inputs               : None
// Returns              : None
////************************************************************************************************
function GetTokenId(){  
  try{
      var intEndRwNo;       //Variable for storing end row number
      var pageurl;
      var index;
      var tokenid;
      var characters;
      intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword  
      g_strBrowserName = GetDicParam("BrowserName");   //Browser Name from Config file
      var strStep = ReadExcel(intEndRwNo,c_intStepCol);  //Reads the step from c_intStepCol
      pageurl = Sys.Browser(g_strBrowserName).Page("*").URL;
      index = pageurl.indexOf("p_auth=", 0);
      tokenid = pageurl.substr(index+7,8); 
      WriteExcel(intEndRwNo,c_intInputVal1Col,g_TestDataSheetName,tokenid)     
      VerificationPoint(g_strFuncCall,strStep,0,intEndRwNo);
  }  
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}
//*************************************************************************************************
// Function Name        : AddClinic
// Function Description : To create a new clinic                   
// Inputs               : None     
// Returns              : None
//*************************************************************************************************
function AddClinic(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterationCnt = intIniRwNo ; intIterationCnt <= intEndRwNo ; intIterationCnt++){ 
      strObjectName = ReadExcel(intIterationCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        strInputValue = ReadExcel(intIterationCnt,c_intInputValCol);  //Reads the input value rom c_intInputValCol    
        strStep = ReadExcel(intIterationCnt,c_intStepCol);  //Reads the step from c_intStepCol    
        strAction = ReadExcel(intIterationCnt,c_intActionCol);  //Reads the action from c_intActionCol   
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName)); 
        if (strObjectName == "ClinicName") {  //If the object name is AddPatient_FirstName write the generated new patient name in excel sheet
          WriteExcel(intIterationCnt,c_intInputVal1Col,g_TestDataSheetName,strInputValue)
        }      
        else if (strObjectName == "ClinicERPNumber") {  //If the object name is AddPatient_FirstName write the generated new patient name in excel sheet
          WriteExcel(intIterationCnt,c_intInputVal1Col,g_TestDataSheetName,strInputValue)
        }      
         else if (strObjectName == "CA_AddUser_LastName") {  //If the object name is AddPatient_FirstName write the generated new patient name in excel sheet
          WriteExcel(intIterationCnt,c_intInputVal1Col,g_TestDataSheetName,strInputValue)
        }                
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterationCnt);
      }
    }  
    
    g_stepnum = strStep;
    } 
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}
//*************************************************************************************************
// Function Name        : SelectCompany
// Function Description : To select a company while creating a company admin             
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function SelectCompany(){
  try{ 
      var intIniRwNo;
      var strInputValue;
      var strStep = "";
      var objTable;
      var TotalRowCount;
      var PageNumber;
      var Rownumber;
      var InputValue;
      intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
      strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value rom c_intInputValCol
      strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
      objTable = BuildWebObj(g_dicColNames_obj.item("GA_AU_CompanyTable"));
      objTable=WaitForObject(objTable, true);
      TotalRowCount = objTable.rows.length;
      var NextLinks = BuildWebObj(g_dicColNames_obj.item("GA_AU_LastLink"));
      NextLinks = WaitForObject(NextLinks, true);
      var TotalLinks = NextLinks.ChildCount;
      if (NextLinks.VisibleOnScreen){
      for(PageNumber=0; PageNumber<=TotalLinks-2;PageNumber++)
      {
        for (Rownumber = 1;Rownumber < TotalRowCount; Rownumber++) 
        {  
          InputValue = objTable.Cell(Rownumber, 2).ContentText;
          if(aqString.Compare(InputValue,strInputValue,false)==0 )
          {
           objTable.Cell(Rownumber, 0).Checkbox("*").Click();             
            break;
          }
        }
        var LastLink = Sys.Browser(g_strBrowser).Page("*").FindChild("className","next",100000);
        if(objTable.Cell(Rownumber, 0).Checkbox("*").Checked == true)
        {
          fnInsertResult("SelectCompany",strStep,""+strInputValue+" shall be selected successfully",""+strInputValue+" is selected successfully","PASS",strStepType);
          break;
        }
        else if((LastLink.exists) && (objTable.Cell(Rownumber, 0).Checkbox("*").Checked !=-1))
        {
          LastLink.Click();
          Delay(5000);
        }
        else
        {
          intStepCount= intStepCount+1;
          fnInsertResult("SelectCompany",strStep,""+strInputValue+" shall be selected successfully",""+strInputValue+" is not selected successfully","FAIL",strStepType);  
          break;                
        }
             
      }
      }
      else 
      {
      for (Rownumber = 1;Rownumber < TotalRowCount; Rownumber++) 
        {  
          InputValue = objTable.Cell(Rownumber, 2).ContentText;
          if(aqString.Compare(InputValue,strInputValue,false)==0 )
          {
           objTable.Cell(Rownumber, 0).Checkbox("*").Click();                      
            break;
          }
          }
        if(objTable.Cell(Rownumber, 0).Checkbox("*").Checked == true)
        {
          fnInsertResult("SelectCompany",strStep,""+strInputValue+" shall be selected successfully",""+strInputValue+" is selected successfully","PASS",strStepType);
        }
        else
        {
          intStepCount= intStepCount+1;
          fnInsertResult("SelectCompany",strStep,""+strInputValue+" shall be selected successfully",""+strInputValue+" is not selected successfully","FAIL",strStepType);  
                   
        }
        }
      
    g_stepnum = strStep;            
  }
  catch(e)
  {
  Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : FindAndReplace
// Function Description : To find and replace in Text file             
// Inputs               : None     
// Returns              : None
//**************************************************************************************************
function FindAndReplace(){
  try{
    var intIniRwNo;        //Variable for storing initial row number   
    var strInputValue;     //Variable for storing input value    
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword    
    strInputValue = ReadExcel(intIniRwNo, c_intInputValCol);  //Reads the input value from c_intInputValCol
    
    var FileName = g_sProjPath+"\Stores\\Files\\"+strInputValue;
    strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    fnInsertResult("FindAndReplace",strStep,"Text file " +strInputValue + " shall be exists","Text file " +strInputValue + " exists","PASS",strStepType);
    
    for (intIterCnt = intIniRwNo + 1 ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      var FindString = strInputValue.split("&&")
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol   
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol   
      ForReading = 1;
      FS = Sys.OleObject("Scripting.FileSystemObject");
      FName = aqFile.OpenTextFile(FileName,aqFile.faRead, aqFile.ctANSI); //Opens the text file in read mode
      var SQLQuery = aqFile.ReadWholeTextFile(FileName, aqFile.ctANSI);  //Reads the text file
      var ReplacedText = aqString.Replace(SQLQuery,FindString[0],FindString[1])  //Find and replace given string in input value    
      FName = aqFile.OpenTextFile(FileName,aqFile.faWrite, aqFile.ctANSI,true);  //Open the text file in write mode
      FName.Write(ReplacedText)  //Write replaced string to text file
      FName.Close()  //Close the text file
      ExpectedMessage = strObjectName+ " shall be replaced successfully";
      if(aqString.Find(ReplacedText,FindString[0])== -1){     
        ActualMessage = strObjectName+ " is replaced successfully";
        intStepCounter = 0;
      }
      else{
        ActualMessage = strObjectName+ " is not replaced successfully";
        intStepCounter = 1;
      }
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);               
    }
    g_stepnum = strStep; 
   }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }               
}


//*************************************************************************************************
// Function Name        : VerifyTableData
// Function Description : To Verify the data present in the table               
// Inputs               : Table Name, Table Cell Values
// Returns              : None        
//************************************************************************************************
function VerifyTableData(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name 
    var strInputValue;     //Variable for storing input value  
    var InputArry = new Array();     //Variable for storing input values
    var TableArry = new Array();      //Variable for storing table values
    var strStep = "";           //Variable for storing step number    
    var Expected;  //Variable for storing Expected result
    var Actual;  //Variable for storing actual result
    var InputCounter = 0;  //Variable for storing counter which stores input values
    var Rowcnt;  //Variable for storing row count of the table
    var ColCnt;  //Variable for storing column count of the table
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
      strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
      //Store the input data in an array named InputArry
      InputArry[InputCounter] = strInputValue;
      //Log.Message  ("Input Value is "+InputArry[InputCounter]+"");
      InputCounter = InputCounter + 1;
    } 
    //Build object          
    ArryObject = BuildWebObj(g_dicColNames_obj.item(strObjectName))
    Object = WaitForObject(ArryObject, true) 
    ScrollIntoView(Object) 
    if(Object.Exists){
      Rowcnt = Object.RowCount;  //Reads the rowcount
      //ColCnt = Object.ColumnCount(1);  //Reads the columncount of second row
      var TableCounter = 0;
      if((strObjectName == "FlagDescriptionTable")|| (strObjectName == "Clncl_ADS_ConfirmationTable1") || (strObjectName == "Clncl_ADS_ConfirmationTable2") || (strObjectName == "Clncl_ADS_ConfirmationTable3") || (strObjectName == "Clncl_ADP_ConfirmationTable")||(strObjectName == "Clncl_APS_ConfirmationTable")||(strObjectName == "Clncl_ASS_ConfirmationTable")||(strObjectName == "Clncl_APS_ConfirmationTableData") || (aqString.Find(g_strAppname,"Claria") != -1) && (strObjectName == "TherapyDetailsTable")){
        intcol=1;  //If the table name is FlagDescriptionTable or TherapyDetailsTable start column count from second column
      }
      else{
        intcol=0;
      }
            
      if(aqString.Find(g_strAppname,"Amia") != -1){
        intRow = 0;
      }
      else{
        intRow = 1;
      }
      for( RowCounter=intRow;RowCounter<=Rowcnt-1;RowCounter++){
        ColCnt = Object.ColumnCount(RowCounter); 
        for( ColCounter=intcol;ColCounter<=ColCnt-1;ColCounter++){   
          if((Object.Cell(RowCounter,ColCounter).contentText).length != 0){
         
            TableArry[TableCounter] = Object.Cell(RowCounter,ColCounter).contentText; //Store the cell value in TableArry   
            TableCounter = TableCounter + 1 ;     //Increment the TableCounter
            
          }
        }      
      }
      var TableLen = TableArry.length;  //Stores the length of the TableArry
      var IntCounter = 0; //Initialize IntCounter
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
       strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
       strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
       //Compare the values of Table array and input array      
        for (IntCounter = IntCounter ; IntCounter < TableLen; IntCounter++){  
          g_IsLastObjectWeb = true;       
          Expected = strObjectName+" shall have the value "+InputArry[IntCounter]                 
            if (Trim(TableArry[IntCounter]) == Trim(InputArry[IntCounter])){ //Compare the values of input value with cell value 
              Actual = strObjectName+" has the value "+InputArry[IntCounter]
              fnInsertResult(g_strFuncCall,strStep,Expected,Actual,"PASS",strStepType);        
            }
            else{
              Actual = "Expected value in "+strObjectName+" shall be "+InputArry[IntCounter] + " but actual value is "+TableArry[IntCounter]
              fnInsertResult(g_strFuncCall,strStep,Expected,Actual,"FAIL",strStepType);
              g_intFailCnt = g_intFailCnt + 1; 
            }   
         
          break;  
        }  
        IntCounter = IntCounter+1;  
      }
    }
    else if (intIterCnt > 1000){
      Expected = strObjectName+" shall have the value "+InputArry[IntCounter]
      Actual = "Table Object does not exist";
      fnInsertResult(g_strFuncCall,strStep,Expected,Actual,"FAIL",strStepType);
      g_intFailCnt = g_intFailCnt + 1;
    }        
    g_stepnum = strStep; 
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}



//*************************************************************************************************
// Function Name        : SearchClinic
// Function Description : To select a company while creating a company admin             
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function SearchClinic(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol   
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      //Read strStep from excel
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
      //Read strAction from excel
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol     
      if (strObjectName != null){
        //Build object
        VerificationObj = BuildWebObj(g_dicColNames_obj.item(strObjectName));        
      }
      
      
     intStepCounter = DataEntry(strAction,VerificationObj,strInputValue,strObjectName,strStep); 
     VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
    }  
    g_stepnum = strStep;   
  }
catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
}   
}

//*************************************************************************************************
// Function Name        : LaunchSpecificURL
// Function Description : To Launch the specific Application Url in Desired Browser
// Inputs               : None
// Returns              : None
////************************************************************************************************
function LaunchSpecificURL(){  
  try{
    Delay(10000);
    var URLlink = ReadExcel(g_intStrtRow,c_intInputValCol);  //Reads the URL from c_intInputValCol
    var strStep = ReadExcel(g_intStrtRow,c_intStepCol);  //Reads the step from c_intStepCol
    var BrowserProcess = null;
    BrowserProcess = Browsers.Item(g_strBrowser);
    
    //Launch or navigate to the URL
    if (Sys.WaitBrowser(g_strBrowser).Exists){
      BrowserProcess.Navigate(URLlink);
    }else{
      BrowserProcess.Run(URLlink);  
    } 
    
    Sys.Refresh();
    Sys.Browser().BrowserWindow(0).Maximize();
    
    VerificationPoint(g_strFuncCall,strStep,0,g_intEndRow);
  }  
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
}
}

//*************************************************************************************************
// Function Name        : DeleteTemplate
// Function Description : To delete a device program template             
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function DeleteTemplate(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol   
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      //Read strStep from excel
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
      //Read strAction from excel
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol     
      if (strObjectName != null){
        //Build object
        VerificationObj = BuildWebObj(g_dicColNames_obj.item(strObjectName));        
      }
     intStepCounter = DataEntry(strAction,VerificationObj,strInputValue,strObjectName,strStep); 
     VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
    }  
    g_stepnum = strStep;   
  }
catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
}   
}

//*************************************************************************************************
// Function Name        : VerifyPDFReport
// Function Description : Verify the PDF Report generated
// Inputs               : None
// Returns              : None
////************************************************************************************************
function VerifyPDFReport(){
 try{
    var strInputValue ;    //Variable for storing input value 
    var strStep = "";   //Variable for storing step number
    var InputValArry = new Array();  //Declare array for storing input values
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    var intInputCounter = 0;  //Initialize counter for storing input value
    var intStepNoCounter = 0;  //Initialize counter for storing step number
    var intStepCounter=1;
    var strObjectName;
    var fileText1;
    fileText1 = SplitPDFFiles(ReadExcel(intIniRwNo,c_intInputValCol));
    var strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
    if (fileText1!=null && g_report==0){
      intStepCounter = 0;
    }
    //GeneratedPDFReportTextFile = g_RelativePathToBitmap+ "\\"+g_TestDataSheetName+"_reports.pdf"
     VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo);
     g_stepnum = strStep;
     GeneratedPDFReportTextFile = "";
  } 
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}
//*************************************************************************************************
// Function Name        : VerifyCSVReport
// Function Description : Verify the CSV Report generated
// Inputs               : None
// Returns              : None
////************************************************************************************************
function VerifyCSVReport(){
try{
    var strInputValue ;    //Variable for storing input value 
    var strStep = "";   //Variable for storing step number
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    var intInputCounter = 0;  //Initialize counter for storing input value
    var intStepNoCounter = 0;  //Initialize counter for storing step number
    var intStepCounter = 1; 
    var TxtData1=SplitTextFile(ReadExcel(intIniRwNo,c_intInputValCol)); // Variable for storing input file data
    ActualMessage=ReadExcel(intIniRwNo,c_intInputValCol)+" is exist in the project folder";
    ExpectedMessage=ReadExcel(intIniRwNo,c_intInputValCol)+" shall be exist in the project folder";
    var strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
    if (TxtData1!=null){
        intStepCounter = 0;
    }    
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo); 
      csvfilename="";
      csvfilename1="";
      g_stepnum = strStep;
      }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}



//*************************************************************************************************
// Function Name        : SaveReportFromTreatmentSummary
// Function Description : To save report from treatment summary page
// Inputs               : None      
// Returns              : None
//*************************************************************************************************
function SaveReportFromTreatmentSummary(){
  try{
    var intIniRwNo = g_intStrtRow;
    g_report=1;   //Is equal to 1 when report content is not copied 
    Sys.Clipboard="";
    var strInputValue = g_sProjPath+"\Stores\\Files\\"+ReadExcel(intIniRwNo,c_intInputValCol);
    var strAction = ReadExcel(intIniRwNo, c_intActionCol)+".txt";  //Reads the action from c_intActionCol
    var strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
    var TestDataPath =  g_sProjPath+"\Stores\\Files\\"+strInputValue;   
     
    ExpectedMessage="PDF shall be generated";
    ActualMessage="PDF is not generated";
    if(g_strBrowserName == "iexplore"){    
      var Object = Sys.Browser(g_strBrowser).Dialog("*");
      Object = WaitForObject(Object, false);
      if(Object.Exists){
        Object = Object.FindChild("Caption","Open", 1000);
        Object.Click();
      }
    }         
    Sys.Browser(g_strBrowserName).BrowserWindow(0).FindChild("WndCaption", "AVScrollView", 1000).Activate(); 
    if(Sys.WaitProcess("AcroRd32", 200).Exists){
      if(Sys.Process("AcroRd32").ToolWindow("Reading Untagged Document").WaitWindow("GroupBox", "", 1, 100).Exists){
        Sys.Process("AcroRd32").ToolWindow("Reading Untagged Document").Window("GroupBox", "", 1).Button("Cancel").Click() ;   
      }
      else if(Sys.Process("AcroRd32", 2).ToolWindow("Reading Untagged Document").WaitWindow("GroupBox", "", 1, 100).Exists){
        Sys.Process("AcroRd32", 2).ToolWindow("Reading Untagged Document").Window("GroupBox", "", 1).Button("Cancel").Click();   
      }
        
    
      var PDFName =  g_sProjPath+"\Stores\\Files\\reports.pdf";
      if(g_strBrowserName == "Chrome"){
       Sys.Browser(g_strBrowserName).BrowserWindow(0).Keys("^s"); 
     
       Sys.Browser(g_strBrowserName).Dialog("Save As").FindChild("Caption", "File name:", 1000).Keys(PDFName);
       Sys.Browser(g_strBrowserName).Dialog("Save As").FindChild("Caption", "Save", 1000).Click();    
      }
      else{   
        Sys.Browser(g_strBrowserName).BrowserWindow(0).FindChild("WndCaption", "AVScrollView", 1000).Keys("^!s"); 
      
        if(!(Sys.Process("AcroRd32").Dialog("Save As").Exists)){
          Sys.Keys("^s");
          Log.Message("Ctrl+S clicked")
          Sys.Browser(g_strBrowserName).Dialog("Save As").FindChild("Caption", "File name:", 1000).Keys(PDFName);
          Sys.Browser(g_strBrowserName).Dialog("Save As").FindChild("Caption", "Save", 1000).Click();    
        } 
        else{
        
          Sys.Process("AcroRd32").Dialog("Save As").FindChild("Caption", "File name:", 1000).Keys(PDFName);   
          Sys.Process("AcroRd32").Dialog("Save As").FindChild("Caption", "Save", 1000).Click(); 
       }
      }
    
      CloseCommandPrompt();
      Sys.OleObject("WScript.Shell").Run("cmd.exe");
    
      CommandWindow = WaitForObject(g_dicColNames_obj.item("CMDWindow"), true)
      var PDFPath = g_sProjPath+"\Stores\\Files"
      CommandWindow.Keys("cd "+PDFPath)
      CommandWindow.Keys("[Enter]");
    
      CommandWindow.Keys("start reports.pdf")
      CommandWindow.Keys("[Enter]");   
     
      CommandWindow.Keys("exit");  
      CommandWindow.Keys("[Enter]");     
      if(g_strBrowserName == "Chrome"){
        intProcessNum = 2;  //Variable to store process number
      }
      else{
        intProcessNum = 4;
      }
      if(Sys.Process("AcroRd32", intProcessNum).WaitWindow("AcrobatSDIWindow", "reports.pdf*", 1, 200).Exists){
        Sys.Process("AcroRd32", intProcessNum).Window("AcrobatSDIWindow", "reports.pdf*", 1).Activate();
        Sys.Process("AcroRd32", intProcessNum).Window("AcrobatSDIWindow", "reports.pdf*", 1).Keys("[Enter]");   
        if(Sys.Process("AcroRd32", intProcessNum).ToolWindow("Reading Untagged Document").WaitWindow("GroupBox", "", 1, 100).Exists){ 
          Sys.Process("AcroRd32", intProcessNum).ToolWindow("Reading Untagged Document").Window("GroupBox", "", 1).Button("Cancel").Click()   
        }   
        ActualMessage="PDF is generated";
        var ClipBoardText = "";
        var DoCount = 0;
        //Copy the contents of PDF
        do {  
          Sys.Process("AcroRd32", intProcessNum).Window("AcrobatSDIWindow", "reports.pdf*", 1).Click();
          Log.Message(Sys.Clipboard);        
        
          Sys.Process("AcroRd32", intProcessNum).Window("AcrobatSDIWindow", "reports.pdf*", 1).Keys("^[Home]");
        
          Sys.Process("AcroRd32", intProcessNum).Window("AcrobatSDIWindow", "reports.pdf*", 1).Keys("^![End]");
        
        
          Sys.Process("AcroRd32", intProcessNum).Window("AcrobatSDIWindow", "reports.pdf*", 1).Keys("^c");
        
          Log.Message(Sys.Clipboard);
          ClipBoardText = Sys.Clipboard;
          DoCount++;
        }while(ClipBoardText.length==0 && DoCount<5)    
        Sys.Process("AcroRd32", intProcessNum).Window("AcrobatSDIWindow", "reports.pdf*", 1).Keys("^q");
        //Creates the FileSystemObject object
        var fso = Sys.OleObject("Scripting.FileSystemObject");        
        if(aqFileSystem.Exists(strInputValue)){  
          file1 = fso.OpenTextFile(strInputValue, ForWriting = 2);  //open file in write mode
          file1.Write("");  //make file empty       
          if(Sys.Clipboard!=null){
            file1.Write(Sys.Clipboard);         
          }          
          g_report=0;   //Is equal to 0 when report content is copied 
          intStepCounter=0;
          file1.Close();                    
          var file1 = fso.OpenTextFile(strInputValue, ForReading = 1);
          // Reads the contents of the text file
          var fileText1 = file1.ReadAll();
          var text = fileText1.replace(/(\r\n|\n|\r)/gm," ");  //Replaces all the line breaks
          file1 = fso.OpenTextFile(strInputValue, ForWriting = 2);  //open file in write mode
          file1.Write("");  //make file empty
          file1.Write(text);  //write the replaced string in text file
          file1.Close();   //Save and close the file
              
        }
        else{
          Log.Error("File Not Found");
        }  
      }
      else{
        intStepCounter=1;
      }    
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo);
      g_stepnum = strStep; 
      aqFile.Delete(PDFName);  //Delete the saved report
    }
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  } 
}


//*************************************************************************************************
// Function Name        : UpdateOrDeleteTemplate
// Function Description : To update or delete template in other browser                 
// Inputs               : None      
// Returns              : None
//*************************************************************************************************
function UpdateOrDeleteTemplate(){
  try {
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword    
    g_strBrowserName1="iexplore";      
    if(CompareText(g_strBrowserName,"IE")){
      g_strBrowserName1="Firefox";         
    } 
    SwitchBrowser("Secondary");
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol 
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  

      if (strObjectName == "SelectClinic_ClinicSelect"){ 
          objName = WaitForObject(objName, true);
          ChildObj = objName.FindAllChildren("objectType","Link", 30);
          ChildObj = new VBArray(ChildObj).toArray();
            for(var intCounter = 0; intCounter <= (ChildObj.length)- 1; intCounter++){
            var Object = Trim(ChildObj[intCounter].innerText);
            if(aqString.Find(Object,Trim(strInputValue))!= -1){
            ChildObj[intCounter].Click();
            Wait(4);
            WaitForPageSync();            
              intStepCounter = 0;
              ExpectedMessage = "The clinic "+strInputValue+" shall be clicked.";
              ActualMessage = "The clinic "+strInputValue+" is clicked."; 
              break;          
              }
            }   
            }
            else {
      //This function performs the strActions specified in the excel 
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      }
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
    }
    g_stepnum = strStep;  
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  finally{
    SwitchBrowser("Primary");
  } 
}

//*************************************************************************************************
// Function Name        : RefreshScreen
// Function Description : To refresh the screen in particular browser               
// Inputs               : None      
// Returns              : None
//*************************************************************************************************
function RefreshScreen(){
  try{
   var LeavePage;
   intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
   intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
   var intStepCounter = 0;
   var strStep = "";
   strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol 
   Sys.Browser(g_strBrowser).BrowserWindow(0).Keys("[F5]");
   LeavePage = Sys.Browser(g_strBrowser).Page("*").FindChild("ObjectIdentifier","OK", 1000);
   if (LeavePage.Exists){
     LeavePage.Click();
   }
   VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo);  
   
   g_stepnum = strStep; 
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : VerifyModifiedDate
// Function Description : To Verify the modified date of a Device Program/System Setting/Patient Setting               
// Inputs               : Device Setting name  
// Returns              : None        
//************************************************************************************************
function VerifyModifiedDate(){
  try{
    
    //Change date to specific Format
    var d = new Date();
    UTCMon = d.getUTCMonth()+1
    var CurrentDate = UTCMon+"/"+d.getUTCDate()+"/"+d.getUTCFullYear()
    var FormattedDate = aqConvert.DateTimeToFormatStr(CurrentDate, "%d %B %Y");  //Variable for storing today//s date in specific format
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword  
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol 
    if (strObjectName != null){
      strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    }
    //Build object
    Object = BuildWebObj(g_dicColNames_obj.item(strObjectName));
    TableObject = WaitForObject(Object, true); 
    ScrollIntoView(TableObject);      
    ChildObj = TableObject.FindAllChildren("objectType","Cell",50);
    ChildObj = (new VBArray(ChildObj)).toArray();
    for (intChildCnt = 0; intChildCnt <= ChildObj.length; intChildCnt++){  
      if (aqString.Compare((ChildObj[intChildCnt].contentText), strInputValue, false) == 0){
        var intRowNumber = ChildObj[intChildCnt].RowIndex;  //Variable for storing row number of given strInputValue
        var strWebPageDate = TableObject.Cell(intRowNumber,3).ContentText;  //Variable for storing date modified displayed on the webpage
        break;
      }
    }  
    //Check if VerifyModifiedDate is successful
    if (aqString.Compare(FormattedDate, strWebPageDate, false) == 0){
      fnInsertResult("VerifyModifiedDate",strStep,"Modified date shall appear as "+FormattedDate,"Modified date appears as "+FormattedDate,"PASS",strStepType);     
      strLogMessage = strStep +" - Modified date for Device Program "+strInputValue+" is verified as "+FormattedDate+"";
      Log.Message(strLogMessage);
    }
    else{
      fnInsertResult("VerifyModifiedDate",strStep,"Modified date shall appear as "+FormattedDate,"Modified date does not appear as "+FormattedDate,"FAIL",strStepType);
      strLogMessage = strStep +" - Modified date for Device Program "+strInputValue+" is expected as "+FormattedDate+" but actual is "+strWebPageDate+"";
      Log.Message(strLogMessage);
      g_intFailCnt = g_intFailCnt + 1;
    }
    g_stepnum = strStep;
  }
  catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  } 
}

//************************************************************************************************
// Function Name        : VerifyModifiedPerson
// Function Description : To Verify the modified person of a Device Program/System Setting/Patient Setting               
// Inputs               : Device Program name 
// Returns              : None         
//************************************************************************************************
function VerifyModifiedPerson(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name 
    var strInputValue;     //Variable for storing input value 
    var strInputValue1;     //Variable for storing another input value
    var strStepType;       //Variable for storing step type
   intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    if (strObjectName != null){
      strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strInputValue1 = ReadExcel(intIniRwNo,c_intInputVal1Col);  //Reads the input value from c_intInputVal1Col
      strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    }
    TableObj = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object
    
  	Object = WaitForObject(TableObj, true);
    if(Object.Exists){
    ScrollIntoView(Object);  
    ChildObj = Object.FindAllChildren("objectType","Cell",50);
    ChildObj = (new VBArray(ChildObj)).toArray();  
    for (intChildCnt = 0; intChildCnt <= ChildObj.length; intChildCnt++){  
      if (aqString.Compare((ChildObj[intChildCnt].contentText), strInputValue, false) == 0){
        var intRow = ChildObj[intChildCnt].RowIndex;  //Variable to store the rowindex of the given strInputValue   
        var strModifiedUser = Object.Cell(intRow,2).ContentText;  //Variable to store the modified person
        break;
      }
     } 
    }
    else{
      Log.Message("Object does not exist")
    } 
    //Verify if modified user is as per test data 
    if (aqString.Compare(trim(strModifiedUser), trim(strInputValue1), false) == 0){                 
      strLogMessage = strStep +" - Modified person for "+strInputValue+" is verified as "+strInputValue1+""
      Log.Message (strLogMessage);
      //insert result
      fnInsertResult("VerifyModifiedPerson",strStep,"Modified user shall appear as "+strInputValue1,"Modified user appears as "+strInputValue1,"PASS",strStepType);     
    }
    else{
      strLogMessage = strStep +" - Modified person for "+strInputValue+" is expected to be "+strInputValue1+" but actual is "+strModifiedUser+""
      Log.Message (strLogMessage);
      fnInsertResult("VerifyModifiedPerson",strStep,"Modified user shall appear as "+strInputValue1,"Modified user does not appear as "+strInputValue1,"FAIL",strStepType);
      g_intFailCnt = g_intFailCnt + 1;
    }
    g_stepnum = strStep;
  }
  catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  } 
}


//************************************************************************************************
// Function Name        : SelectTemplate
// Function Description : To select Device Program/System Setting/Patient Setting template                   
// Inputs               : Template name      
// Returns              : None
//*************************************************************************************************
function SelectTemplate(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var strLoginUser;
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
        //strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
        
        
       
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);  
      }
    }  
    g_stepnum = strStep;
  }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }   
}

//************************************************************************************************
// Function Name        : SelectFilter
// Function Description : To select Filters in clinical dashboard                   
// Inputs               :       
// Returns              : None
//*************************************************************************************************
function SelectFilter(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var strLoginUser;
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
        //strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
        
        
        
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);  
      }
    }  
    g_stepnum = strStep;
  }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }   
}

//*************************************************************************************************
// Function Name        : ApplyTemplate
// Function Description : To delete a device program template             
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function ApplyTemplate(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol   
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      //Read strStep from excel
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
      //Read strAction from excel
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol     
      //strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
      if (strObjectName != null){
        //Build object
        VerificationObj = BuildWebObj(g_dicColNames_obj.item(strObjectName));        
      }
      
     intStepCounter = DataEntry(strAction,VerificationObj,strInputValue,strObjectName,strStep); 
     
     VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
    }  
    g_stepnum = strStep;   
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }   
}
//*************************************************************************************************
// Function Name        : UpdateTemplateandprograminOtherBrowser
// Function Description : To update a clinic patient settings template or program.                 
// Inputs               : None      
// Returns              : None
//*************************************************************************************************
function UpdateTemplateandprograminOtherBrowser(){
  try {
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    
    g_strBrowserName1="iexplore";      
    if(CompareText(g_strBrowserName,"IE")){
      g_strBrowserName1="Firefox";         
    } 
     
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      SwitchBrowser("Secondary");
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name      
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol  
      
      if (strObjectName == "SelectClinic_ClinicSelect"){ 
          objName = WaitForObject(objName, true);
          ChildObj = objName.FindAllChildren("objectType","Link", 30);
          ChildObj = new VBArray(ChildObj).toArray();
            for(var intCounter = 0; intCounter <= (ChildObj.length)- 1; intCounter++){
              var Object = Trim(ChildObj[intCounter].innerText);
              ExpectedMessage=strInputValue+" shall be clicked in the "+strObjectName; 
              if(aqString.Find(Object,Trim(strInputValue))!= -1){
                ChildObj[intCounter].Click();
		            WaitForPageSync();
                intStepCounter = 0;
                ActualMessage=strInputValue+" is clicked in the "+strObjectName;
                break;          
               }
               else {
                intStepCounter = 1;
                ActualMessage=strInputValue+" is not clicked in the "+strObjectName;
               }
            }   
       }
      else{  
        //This function performs the strActions specified in the excel 
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        if (strObjectName == "PasswordPopup_Submitbutton"){
          Wait(10);
        }      
      }
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);      
      g_strObjectName = "";
    }
    g_stepnum = strStep;  
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  finally{
    SwitchBrowser("Primary");
  }
   
}
//************************************************************************************************
// Function Name        : VerifyNotVisibleOnScreen
// Function Description : To verify that the object does not exist on the screen           
// Inputs               : None  
// Returns              : None      
//************************************************************************************************ 
function VerifyNotVisibleOnScreen(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1; //Variable to store pass/fail count
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){ 
    strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    if (strObjectName != null){
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol     
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
      //strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
      var ObjVis = WaitForObject(objName, true);  
      if(ObjVis.VisibleOnScreen==false){
        intStepCounter = 0;         
      }
                   
    }    
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
  }
  g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : VerifyFlagSymbolTable
// Function Description : To verify the values and flag in Flag Symbol table                    
// Inputs               : None     
// Returns              : None
//*************************************************************************************************

function VerifyFlagSymbolTable(){
  try{
   var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value    
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var intRowCounter ;  //Variable to store the rowcount of the table
    var strFlagColour;      //Variable for storing Flag colour
    var RowCnt ;  //Variable to store total row count of the table
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      intRowCounter = 1;
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol     
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol        
      strInputValue1 = ReadExcel(intIterCnt, c_intInputVal1Col);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol  
      //strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
      Object = BuildWebObj(g_dicColNames_obj.Item(strObjectName));
      var TableObject = WaitForObject(Object, true);
      ScrollIntoView(TableObject)
      RowCnt = TableObject.RowCount;
      if(aqString.Find(strInputValue1,"Warning") != -1){
        strFlagColour = "Yellow";
      }
      else if(aqString.Find(strInputValue1,"Error") != -1){
        strFlagColour = "Red";     
      }
      ExpectedMessage = strFlagColour+ " colour flag icon and "+strInputValue+" value shall be displayed in the Flag Symbol Table";
      for (intRowCounter = intRowCounter ; intRowCounter < RowCnt; intRowCounter++){
        FlagIcon = TableObject.Cell(intRowCounter, 0).FindChild("className",trim(strInputValue1),1000);
        //Check if flag exist and input value matches with the table value
        if((FlagIcon.Exists) && ((trim(TableObject.Cell(intRowCounter, 1).contentText)) == trim(strInputValue))){
          intStepCounter = 0;  
          ActualMessage = strFlagColour+ " colour flag icon and "+strInputValue+" value is displayed in the Flag Symbol Table";   
          break;   
        } 
               
      }
      if((intRowCounter == RowCnt) && (intStepCounter == 1)){
        ActualMessage = ActualMessage = strFlagColour+ " colour flag icon and "+strInputValue+" value is not displayed in the Flag Symbol Table";      
      }
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
    }                                                          
    g_stepnum = strStep;     
 }
  catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : ClickTreatmentFlag
// Function Description : To Click Treatment Flag in Patient Snapshot screen.                  
// Inputs               : Patient Name
// Returns              : None        
//************************************************************************************************
function ClickTreatmentFlag(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var strObjectName;     //Variable for storing object name 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1; //Variable for storing pass/fail for each step 
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword   
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol       
    strInputValue = ReadExcel(intIniRwNo, c_intInputValCol);  //Reads the input value from c_intInputValCol
    strInputValue1 = ReadExcel(intIniRwNo, c_intInputVal1Col);  //Reads the input value from c_intInputValCol
    strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol  
    //strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    Object = BuildWebObj(g_dicColNames_obj.Item(strObjectName));
    var TableObject = WaitForObject(Object, true);
    intChildCnt = TableObject.ChildCount
    ExpectedMessage = "Treatment Flag icon shall be clicked for the date "+strInputValue;
    for (intCounter = 0 ; intCounter < intChildCnt ; intCounter++){ 
      flagdate = TableObject.Child(intCounter).contentText.replace(/(\r\n|\n|\r)/gm," ");  //Replaces all the line breaks
      if(aqString.Find(flagdate,trim(strInputValue1)) != -1){    
        if (TableObject.Child(intCounter).FindChild("ObjectIdentifier","flag_1*", 1000).Exists) {
          TableObject.Child(intCounter).FindChild("ObjectIdentifier","flag_1*", 1000).Click();
          ActualMessage = "Treatment Flag icon is clicked for the date "+strInputValue;
          intStepCounter = 0;       
        }
        else {
          ActualMessage = "Treatment Flag icon is not clicked for the date "+strInputValue;
          
        }
       break;
     }
   }
    g_stepnum = strStep;
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo); 
  }
  catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : UpdateFlagRules
// Function Description : To update flag rules for a clinic                   
// Inputs               : None      
// Returns              : None
//*************************************************************************************************
function UpdateFlagRules(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
        //This function performs the strActions specified in the excel      
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);   
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);  
      }
    }  
    g_stepnum = strStep;
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }   
}


//*************************************************************************************************
// Function Name        : TreatmentFlagVerification
// Function Description : To verify existence of Flag treatment file in the treatment dashboard                  
// Inputs               : None
// Returns              : None        
//************************************************************************************************
function TreatmentFlagVerification(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));
    strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
    strInputValue1 = ReadExcel(intIniRwNo,c_intInputVal1Col);  //Reads the input value from c_intInputVal1Col       
    strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol      
    strAction = ReadExcel(intIniRwNo,c_intActionCol);  //Reads the action from c_intActionCol   
    //strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    var splitarray = new Array();  // Variable to store the no of records split string
    var pagearray = new Array(); // variable to store the split value of pagecount if it is decimal
    var Flagrow = 0; // variable to store the flag row number
    var Flagcol = 0; //varible  for flag column number
    var iIndex = 0; // varible to store the page nagtion index
    var pageno = 0; //varible to store the total current page number
    var table = BuildWebObj(g_dicColNames_obj.item("TreatmentDashBoard_Table"));  //Build object from the given object name 
    var tableobject = WaitForObject(table, true);
    var rows = tableobject.rows.length;
    var tablecolumns = tableobject.ColumnCount(0);
    var FlagType = new Array();  //Variable to store flag type getting from strInputValue1
    FlagType = strInputValue1.split(",");
    //To get the flag column
    for (var col = 1;col < tablecolumns; col++) {       
      Columnvalue = tableobject.Cell(0, col).ContentText;
      Columnvalue = Columnvalue.replace(/(\r\n|\n|\r)/gm," ");  //Replaces all the line breaks
      if(aqString.Find(Columnvalue,(FlagType[0]))!= -1){
        Flagcol = col;
        break;
      } 
    }
    // to get the no of pages based on the no of patients in the records string
    var Pagenation = BuildWebObj(g_dicColNames_obj.item("TreatmentDashBoard_NextPageNavigation")); 
    var TotalPatientCount = WaitForObject(objName, true);
    splitarray = (TotalPatientCount.ContentText).split("of");
    if(splitarray[1]>10){
      pagecount = splitarray[1]/10;
      if(aqString.Find(pagecount,("."))!= -1){
        pagecount = pagecount.toString();
        pagearray = pagecount.split(".")
        if(pagearray[1]>0){
        pagecount = Number(pagearray[0])+1
        }
      }
    }
    else{
      pagecount=1;
    }
    // to set the index if page count exceeds 5
    if(pagecount<5){
      iIndex = pagecount-1;
    }
    else{
      iIndex = 4;
    }
    for (pageno=1; pageno<=pagecount; pageno++){ //click the page no sequentially
      if(pagecount !=1){
        if (pageno > 4){
          iIndex = 1;
        }
        if(pageno == pagecount){
          iIndex = 0;
        }
        pagenation = Pagenation+"."+"Child("+iIndex+")";
        pagenation = WaitForObject(pagenation, true); 
        pagenation.Click();
        Delay(3000);
        iIndex--;             
      }
      
      for (var r = 1;r < rows; r++) {       // search for the patient row in each page
        PatientName = tableobject.Cell(r, 0).ContentText;
        if(aqString.Find(PatientName,trim(strInputValue))!= -1){
          //flagrow = row no contains the flag.
          Flagrow = r;
          break;
        } 
      }
      if(Flagrow!=0){
        break;
      } 
    }
    FlagObject = tableobject.Cell(Flagrow, Flagcol).FindChild("className",FlagType[1],1000);
    //This function performs the strActions specified in the excel 
    intStepCounter = DataEntry(strAction,FlagObject,strInputValue,strObjectName,strStep);        
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo);
    g_stepnum = strStep;         
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}     
         
//*************************************************************************************************
// Function Name        : ClinicalDashboardFilter
// Function Description : To Verify No Communication flag is filtered in treatment dashboard                  
// Inputs               : None
// Returns              : None        
//************************************************************************************************
function ClinicalDashboardFilter(){
try{
var intIniRwNo;        //Variable for storing initial row number
var strObjectName;     //Variable for storing object name
var objName;           //Variable for storing object 
var strInputValue;     //Variable for storing input value 
var strAction;         //Variable for storing action 
var strStep = "";            //Variable for storing step number
var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)

intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));
strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol      

var splitarray = new Array();
var pagearray = new Array();
var table = BuildWebObj(g_dicColNames_obj.item("TreatmentDashBoard_Table"));  //Build object from the given object name 
var tableobject = WaitForObject(table, true);
var tablecolumns = tableobject.ColumnCount(0);
var Pagenation = BuildWebObj(g_dicColNames_obj.item("TreatmentDashBoard_NextPageNavigation"));
var TotalPatientCount = WaitForObject(objName, true);


splitarray = (TotalPatientCount.ContentText).split("of");
if(splitarray[1]>10){
pagecount = splitarray[1]/10;
if(aqString.Find(pagecount,("."))!= -1){
pagecount = pagecount.toString();
pagearray = pagecount.split(".")
if(pagearray[1]>0){
pagecount = Number(pagearray[0])+1
     }
   }
  }else{
  pagecount=1;
}
for (var i=1;i<=pagecount;i++){
var rows = tableobject.rows.length;
  for(var r=1;r<rows;r++){
  
  var check=false;
     for (var col=1;col<tablecolumns;col++) {       
      FlagObject = tableobject.Cell(r, col).FindChild("className","normalNoWiFi*",1000);
      if(FlagObject.Exists){
      check=true;
      break;
          } 
      }
      if(!check){
       break;
      }
    }  
      
      if((!check)||(pagecount==1)){
            break;
            }           
      var pageno = i-1;
      pagenation = Pagenation+"."+"Child("+pageno+")";
      pagenation = WaitForObject(pagenation, true);
      pagenation.Click();
      Delay(3000);    
                                     
   }
    if(check){
     intStepCounter = 0;
    }else{
     intStepCounter = 1;
    }
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo); 
   
  g_stepnum = strStep;    
      
}
catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : VerifyTreatmentFlag
// Function Description : To Verify Treatment Flag in Patient Snapshot screen.                  
// Inputs               : Patient Name
// Returns              : None        
//************************************************************************************************
function VerifyTreatmentFlag(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var strObjectName;     //Variable for storing object name 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";            //Variable for storing step number
    var intStepCounter =1;
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword   
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol       
    strInputValue = ReadExcel(intIniRwNo, c_intInputValCol);  //Reads the input value from c_intInputValCol
    strInputValue1 = ReadExcel(intIniRwNo, c_intInputVal1Col);  //Reads the input value from c_intInputValCol
    strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol  
    Object = BuildWebObj(g_dicColNames_obj.Item(strObjectName));
    var TableObject = WaitForObject(Object, true);
    ChildObj = TableObject.FindAllChildren("innerHtml","*flag_1*",30);  //Find all the child objects of the type Link
    ChildObj = new VBArray(ChildObj).toArray();
    for (intChildCnt = 0 ; intChildCnt < ChildObj.length ; intChildCnt++){ 
      var flagdate =ChildObj[intChildCnt].ContentText;
      flagdate = flagdate.replace(/(\r\n|\n|\r)/gm," ");  //Replaces all the line breaks
      if (aqString.Find((flagdate),trim(strInputValue1))!= -1){
        if (ChildObj[intChildCnt].FindChild("ObjectIdentifier","flag_1*", 1000).Exists) {
          intStepCounter=0;        
          break;
        }              
      }
    }      
 }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }finally{
    g_stepnum = strStep;
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo);
  } 
}
	

//'*************************************************************************************************
//' Function Name        : TimeframeDiff
//' Function Description : To validate,timeframe difference                 
//' Inputs               : Days  
//' Returns              : None        
//'************************************************************************************************
function TimeframeDiff(){
try {
 var intIniRwNo;        //'Variable for storing initial row number
  var intEndRwNo;        //'Variable for storing end row number
  var strObjectName;     //'Variable for storing object name
  var obj;               //'Variable for storing object 
  var strInputValue;    // 'Variable for storing input value 
  var strAction;        // 'Variable for storing action 
  var strStepType;       //Variable for storing step type
  blnFlag = 0;
  intIniRwNo = g_intStrtRow;
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol       
    strInputValue = ReadExcel(intIniRwNo, c_intInputValCol);  //Reads the input value from c_intInputValCol
    strInputValue1 = ReadExcel(intIniRwNo, c_intInputVal1Col);  //Reads the input value from c_intInputValCol
    strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol  
    
   //'Build object
   
  Strobject = BuildWebObj(g_dicColNames_obj.item(strObjectName));
  //WaitForObject(Strobject);
  var Objdaterange1 = WaitForObject(Strobject, true);
  StrDate = Objdaterange1.contentText ;
  data = StrDate.split("-"); 
  datedi =   myDatediff( data[0],data[1],strInputValue1);
   
   if (datedi == strInputValue){
    strLogMessage = strStep +" - Timeframe difference between two dates is "+strInputValue+"";
    Log.Message  (strLogMessage);
    intStepCounter =0;
   // fnInsertResult ("TimeframeDiff",strStep,"The time difference between treatment dates shall be "+strInputValue+"","The time difference between treatment dates is "+strInputValue+"","PASS");
    }
    else{
    strLogMessage = strStep +"-Timeframe difference between two dates is expected to be "+strInputValue+" but actual is "+datedi+"";
    Log.Message  (strLogMessage);
    intStepCounter =1
   // fnInsertResult ("TimeframeDiff",strStep,"The time difference between treatment dates shall be "+strInputValue+"","The time difference between treatment dates is not "+strInputValue+"","FAIL");
   // g_intFailCnt = g_intFailCnt + 1;
    }
    g_stepnum = strStep;     
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo); 
} 
catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//'************************************************************************************************
//' Function Name        : myDatediff
//' Function Description : To get the date difference in years, months,weeks,days,minutes,seconds                  
//' Inputs               : Date and interval       
//'*************************************************************************************************

function myDatediff(date1,date2,interval) {
  try {
    var second=1000, minute=second*60, hour=minute*60, day=hour*24, week=day*7;
    date1 = new Date(date1);    
    date2 = new Date(date2);
    date1 = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate());
    date2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());
    var timediff = date2 - date1;
    
    if (isNaN(timediff)) return NaN;
    switch (interval) {
        case "years": return date2.getFullYear() - date1.getFullYear();
        case "months": return (
            ( date2.getFullYear() * 12 + date2.getMonth() )
            -
            ( date1.getFullYear() * 12 + date1.getMonth() )
        );
        case "weeks"  : return Math.floor(timediff / week);
        case "days"   : return Math.floor(timediff / day); 
        case "hours"  : return Math.floor(timediff / hour); 
        case "minutes": return Math.floor(timediff / minute);
        case "seconds": return Math.floor(timediff / second);
        default: return undefined;
    }
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}
//************************************************************************************************
// Function Name        : GetTodayDate
// Function Description : To get and write today date in excel sheet           
// Inputs               : None  
// Returns              : None      
//************************************************************************************************ 
function GetTodayDate(){
try{
    var intIniRwNo;        //Variable for storing initial row number
    var strInputValue;     //Variable for storing input value 
    var strStep = "";            //Variable for storing step number
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    strInputValue = ReadExcel(intIniRwNo, c_intInputValCol);  //Reads the input value from c_intInputValCol
    strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol      
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheetName,strInputValue)  
    Log.Message("Today Date is entered in excel sheet");
    fnInsertResult ("Today date",strStep,strInputValue+" shall be entered",strInputValue+" is entered","PASS",strStepType);
    g_stepnum = strStep;      
    }
catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}




//*************************************************************************************************
// Function Name        : VerifyTherapyUFTable
// Function Description : To verify the values of Therapy UF Table                    
// Inputs               : None      
// Returns              : None
//*************************************************************************************************
function VerifyTherapyUFTable(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        //strInputValue = strInputValue.replace(/(\r\n|\n|\r)/gm," ");  //Replaces all the line breaks
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
        //This function performs the strActions specified in the excel 
        //strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);  
      }
    }  
    g_stepnum = strStep;
  }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }   
}

//*************************************************************************************************
// Function Name        : EnterReportDate
// Function Description : To enter date (number) from date pick control                   
// Inputs               : Date specified in testdata     
// Returns              : None    
//*************************************************************************************************
function EnterReportDate(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strResult;         //Variable for storing result
    var Arrayinput = new Array();  //Arry to store date
    var objnotexist = 0;   //Variable to verify if the object exist
    var intStepCounter = 1;  //Variable for storing pass/fail count
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    //Read object name from excel 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    if (strObjectName != null){    
      //Read input value from excel
      strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
      var d = new Date(strInputValue);
      var n = d.toString(); 
      //to get the date in expected format
      Arrayinput = n.split(" ");
      var inputnum = Arrayinput[2]; 
      var month=Arrayinput[1];
      var year= Arrayinput[5];
      //To select month from calender 
      var monthobj = BuildWebObj(g_dicColNames_obj.item("ReportsDateSelect_Month"));
      monthobj = WaitForObject(monthobj, true);
      monthobj.ClickItem(trim(month));
      //To select year from calender 
      var yearobj = BuildWebObj(g_dicColNames_obj.item("ReportsDateSelect_Year"));
      yearobj = WaitForObject(yearobj, true);
      yearobj.ClickItem(trim(year));
      //Read strStep from excel
      strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
      //Read strAction from excel
      strAction = ReadExcel(intIniRwNo,c_intActionCol);  //Reads the action from c_intActionCol     
      //find ChildObject using finall method
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
      strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);
      objArry = BuildWebObj(g_dicColNames_obj.item(strObjectName))
      objName = WaitForObject(objArry, true);
      ChildObj = objName.FindAllChildren("objectType","Cell", 30);
      ChildObj = (new VBArray(ChildObj)).toArray();
      //Click on a particular date
      for(var intCounter = 0; intCounter <= (ChildObj.length)- 1; intCounter++){
        if((Trim(ChildObj[intCounter].contentText) == Trim(inputnum))){
          ChildObj[intCounter].Click();
          intStepCounter = 0;
          break;          
          }
        }
      }   
      var inputdate=inputnum+"-"+month+"-"+year 
      if ((intStepCounter == 0)&&(objnotexist==0)) {    
      
      fnInsertResult("EnterReportDate",strStep,"User shall be able to enter a date to generate report","User is entered a date of "+inputdate+" to generate report","PASS",strStepType);
      }
      else{    
        fnInsertResult("EnterReportDate ",strStep,"User shall be able to enter a date to generate report","User is not entered a date of "+inputdate+" to generate report","FAIL",strStepType);
        g_intFailCnt = g_intFailCnt + 1; 
      }
      g_stepnum = strStep; 
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : UpdateFlagRulesinOtherBrowser
// Function Description : To update a Clinic Flag Rules in other browser               
// Inputs               : None      
// Returns              : None
//*************************************************************************************************
function UpdateFlagRulesinOtherBrowser(){
  try {
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)

    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    g_strBrowserName1="iexplore";      
    if(CompareText(g_strBrowserName,"IE")){
      g_strBrowserName1="Firefox";         
    } 
    SwitchBrowser("Secondary"); 
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name 
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol  
      //This function performs the strActions specified in the excel 
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);   
    }
    g_stepnum = strStep;  
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  finally{
    SwitchBrowser("Primary");
  } 
}

//*************************************************************************************************
// Function Name        : SiteAccessReportLaunch
// Function Description : To Launch the Web Application Url for Report in Desired Browser
// Inputs               : None
// Returns              : None
////************************************************************************************************
function SiteAccessReportLaunch(){  
  try{
    
    
    var intIniRwNo;       //Variable for storing initial row number
    var intEndRwNo;      //Variable for storing end row number      
    var strInputValue;    //Variable for storing input value 
    var strObjectName;   //Variable for storing Objectname
    var strAction;       //Variable for storing Action
    var strStep = "";          //Variable for storing Step Number
    var pageurl;
    var arrayobj ;         //variable for storing array of object
    var objName;          //Variable for storing builded web object 
    var intStepCounter = 1;    //Variable for storing flag
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword   
    strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);//Reads input value from c_intInputValCol
    strAppurl = strInputValue;
    if(aqString.Find(strInputValue,"http")== -1){
     strAppurl = g_strWebAppURL+strInputValue;  //Read Web Application URL from Config file
     }
    g_strBrowserName = GetDicParam("BrowserName");   //Browser Name from Config file
    strStep = ReadExcel(g_intStrtRow,c_intStepCol);  //Reads the step from c_intStepCol
    strStepType = ReadExcel (g_intStrtRow,c_intStepType); //Reads the step type from c_intStepType
    switch (trim(g_strBrowserName)){
      case "IE":
       Browsers.Item(btIExplorer).Run(strAppurl);
       break;  
      case "Chrome":
        Browsers.Item(btChrome).Run(strAppurl);
        break;
      case "Firefox":
        Browsers.Item(btFirefox).Run(strAppurl);
        break;
      case "Safari":
        Browsers.Item(btSafari).Run(strAppurl);
        break;
      case "Opera":
        Browsers.Item(btOpera).Run(strAppurl);
        break; 
      case "Edge":
        Browsers.Item(btEdge).Run(strAppurl);
        break;     
     
    } 
      Sys.Refresh();
  
    if (aqString.Find(strAppurl,"=csv")!= -1){
    switch (trim(g_strBrowserName)){
      case "IE":
       var CSVSaveWindowIE = Sys.Browser(g_strBrowser).Dialog("*").FindChild("Caption","Save as", 1000);
       if (CSVSaveWindowIE.Exists){
       intStepCounter = 0;
       }
       break;  
      case "Chrome":
       var CSVSaveWindowChrome = Sys.Browser(g_strBrowser).Dialog("*").FindChild("Caption","Save as type:",1000);
       if (CSVSaveWindowChrome.Exists){
       intStepCounter = 0;
       }
       break;
      case "Firefox":
       var CSVSaveWindowFF = Sys.Browser(g_strBrowser).UIPage("*").FindChild("idStr","save", 1000);
       if (CSVSaveWindowFF.Exists){
       intStepCounter = 0;
       }
       break;
      case "Edge":
       var CSVSaveWindowEdge = Sys.Browser(g_strBrowser).BrowserWindow(0).FindChild("ObjectIdentifier","Save_as",500);
       if (CSVSaveWindowEdge.Exists){
       intStepCounter = 0;
       }
       break;
    } 
    }
    if (aqString.Find(strAppurl,"=pdf")!= -1){
    if(Sys.Browser(g_strBrowser).BrowserWindow(0).Exists){
    intStepCounter = 0;
    }
    }
    if (intStepCounter==0){
    
     Expected = "User shall be navigated to "+strAppurl+" successfully";  //Read expected result from excel
     Actual = "User is navigated to "+strAppurl+" succeessfully";  //Read actual pass result from excel
     fnInsertResult("SiteAccessReportLaunch",strStep,Expected,Actual,"PASS",strStepType);
     
     }
     else{
     Expected = "User shall be navigated to "+strAppurl+" successfully";  //Read expected result from excel
     Actual = "User is navigated to "+strAppurl+" succeessfully";  //Read actual pass result from excel
     Actual="User is not navigated to "+strAppurl+" successfully";  //Read actual fail result from excel
     fnInsertResult("SiteAccessReportLaunch",strStep,Expected,Actual,"FAIL",strStepType);
     }
  
  }
catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
}
}
//************************************************************************************************
// Function Name        : VerifyPDFFiles
// Function Description : To Verify two PDF files                  
// Inputs               : Downloaded File Path and Input file path  
// Returns              : None        
//************************************************************************************************
function VerifyPDFFiles(){
  try{
    var strInputValue ;    //Variable for storing input value 
    var strStep = "";    //Variable for storing step number
    var InputValArry = new Array();  //Declare array for storing input values
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword  
    var intInputCounter = 0;  //Initialize counter for storing input value
    var intStepNoCounter = 0;  //Initialize counter for storing step number
    var intStepCounter=0;
    var strObjectName
    var fileText1 = SplitPDFFiles(ReadExcel(intIniRwNo,c_intInputValCol));
    var strStep1 = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
    var strStepType;       //Variable for storing step type
    ActualMessage=ReadExcel(intIniRwNo,c_intInputValCol)+" is exist in the project folder";
    ExpectedMessage=ReadExcel(intIniRwNo,c_intInputValCol)+" shall be exist in the project folder";
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    VerificationPoint(g_strFuncCall,strStep1,intStepCounter,1,strStepType);
    if (fileText1!=null && g_report==0){
      for (var intIterCnt = intIniRwNo+1 ; intIterCnt <= intEndRwNo ; intIterCnt++){   
        //Read input value from excel
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
        strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
        if(IsEmpty(strAction)){ //to compare with the PDF
          ExpectedMessage=strInputValue+" shall be matched with the PDF";
          if(aqString.Find(fileText1,strInputValue)==-1){
            intStepCounter = 0; 
            ActualMessage=strInputValue+" is not present in the Report Generated";
          }
          else{
            ActualMessage=strInputValue+" is present in the Report Generated";
            intStepCounter = 1; 
          }
        } 
        else{ //to close the pdf 
          strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameColstrObjectName = readexcel
          objName = g_dicColNames_obj.item(strObjectName);  //Build object from the given object name    
          intStepCounter =  DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        }       
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      }
      
      g_stepnum = strStep; 
    }
    else{    
      FirstStep = ReadExcel(intIniRwNo+1,c_intStepCol);  //Reads the step from c_intStepCol
      LastStep = ReadExcel(intEndRwNo,c_intStepCol);  //Reads the step from c_intStepCol
      Step = FirstStep+ " - "+LastStep
      fnInsertResult(g_strFuncCall,Step,"PDF data shall be copied","PDF data is not copied","FAIL",strStepType);
    }
  } 
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : MultiTreatmentFlagVerification
// Function Description : To verify existence of multiple flags in the treatment dashboard                  
// Inputs               : Date specified in testdata     
// Returns              : None    
//*************************************************************************************************
function MultiTreatmentFlagVerification(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));
    strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
    strInputValue1 = ReadExcel(intIniRwNo,c_intInputVal1Col);  //Reads the input value from c_intInputVal1Col  
    strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol      
    strAction = ReadExcel(intIniRwNo,c_intActionCol);  //Reads the action from c_intActionCol   
    //strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    var splitarray = new Array();  // Variable to store the no of records split string
    var pagearray = new Array(); // variable to store the split value of pagecount if it is decimal
    var Flagrow = 0; // variable to store the flag row number
    var Flagcol = 0; //varible  for flag column number
    var iIndex = 0; // varible to store the page nagtion index
    var pageno = 0; //varible to store the total current page number
    var table = BuildWebObj(g_dicColNames_obj.item("TreatmentDashBoard_Table"));  //Build object from the given object name 
    var tableobject = WaitForObject(table, true);
    var rows = tableobject.rows.length;
    var tablecolumns = tableobject.ColumnCount(0);
    //To get the flag column
    for (var col = 1;col < tablecolumns; col++) {       
      Columnvalue = tableobject.Cell(0, col).ContentText;
      Columnvalue = Columnvalue.replace(/(\r\n|\n|\r)/gm," ");  //Replaces all the line breaks
      if(aqString.Find(Columnvalue,(strInputValue1))!= -1){
        Flagcol = col;
        break;
      } 
    }
    // to get the no of pages based on the no of patients in the records string
    var Pagenation = BuildWebObj(g_dicColNames_obj.item("TreatmentDashBoard_NextPageNavigation")); 
    var TotalPatientCount = WaitForObject(objName, true);
    splitarray = (TotalPatientCount.ContentText).split("of");
    if(splitarray[1]>10){
      pagecount = splitarray[1]/10;
      if(aqString.Find(pagecount,("."))!= -1){
        pagecount = pagecount.toString();
        pagearray = pagecount.split(".")
        if(pagearray[1]>0){
          pagecount = Number(pagearray[0])+1
        }
      }
    }
    else{
      pagecount=1;
    }
    // to set the index if page count exceeds 5
    if(pagecount<5){
      iIndex = pagecount-1;
    }
    else{
      iIndex = 4;
    }

    for (pageno=1; pageno<=pagecount; pageno++){ //click the page no sequentially
      if(pagecount !=1){
        if (pageno > 4) {
          iIndex = 1;
        }
        if(pageno == pagecount){
          iIndex = 0;
        }
        pagenation = Pagenation+"."+"Child("+iIndex+")";
        pagenation = WaitForObject(pagenation, true); 
        pagenation.Click();
        Delay(3000);
        iIndex--;             
      }            
      for (var r = 1;r < rows; r++) {       // search for the patient row in each page
        PatientName = tableobject.Cell(r, 0).ContentText;
        if(aqString.Find(PatientName,trim(strInputValue))!= -1){
          //flagrow = row no contains the flag.
          Flagrow = r;
          break;
        } 
      }
      if(Flagrow!=0){
        break;
      } 
    }  
    //build the flag object based on row and column obtained.
    FlagObject = tableobject.Cell(Flagrow, Flagcol).FindChild("className","normalErrorMulti*",1000);
    //This function performs the strActions specified in the excel 
    intStepCounter = DataEntry(strAction,FlagObject,strInputValue,strObjectName,strStep);        
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo);
    g_stepnum = strStep;         
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}
//*************************************************************************************************
// Function Name        : RenameTreatmentFile
// Function Description : To rename the treatment file which is present in the Project folder                  
// Inputs               : File name present in Stores folder     
// Returns              : None    
//*************************************************************************************************

function RenameTreatmentFile(){
  try{
    var intIniRwNo;        //Variable for storing initial row number   
    var strInputValue;     //Variable for storing input value    
    var strStep = "";            //Variable for storing step number
    var ExistingFileName        //variable to store the old file name      
    var NewFileName;        //variable to store the rename file
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
  
    strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol   
    
    strTime = (aqConvert.DateTimeToFormatStr(aqDateTime.Time(), "%H%M%S"));
    strDate = (aqConvert.DateTimeToFormatStr(aqDateTime.Today(), "%y%m%d"));
    
    strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
    
    ExistingFileName = strInputValue;
    FileNamelength = aqString.GetLength(strInputValue);

    NewFileName = strInputValue.replace(strInputValue.substring(FileNamelength-4,strInputValue.lastIndexOf("_")+1),strTime);
    NewFileName = NewFileName.replace(NewFileName.substring(NewFileName.lastIndexOf("_"),NewFileName.lastIndexOf("_")-6),strDate);
    
    strInputValue=g_sProjPath+"\Stores\\Files\\"+strInputValue;

    aqFileSystem.CopyFile(strInputValue, g_sProjPath+"\Stores\\Files\\"+NewFileName);
          
    ExpectedMessage = ExistingFileName+ " shall be renamed successfully";
    if(aqFileSystem.Exists(g_sProjPath+"\Stores\\Files\\"+NewFileName)){  
      WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheetName,NewFileName);   
      ActualMessage = ExistingFileName+ " is renamed successfully";
      intStepCounter = 0;
    }
    else{
      ActualMessage = ExistingFileName+ " is not renamed successfully";
      intStepCounter = 1;
      
    } 
    g_stepnum = strStep; 
   }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  finally{
    if(intStepCounter == 1){
      StopCurrentTest();
    }
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo); 
  }               
} 

//*************************************************************************************************
// Function Name        : FileDelete
// Function Description : To delete the treatment file which is present in the Project folder                  
// Inputs               : File name to be deleted which is present in Stores folder     
// Returns              : None    
//*************************************************************************************************
function FileDelete(){
  try{
    var intIniRwNo;        //Variable for storing initial row number   
    var strInputValue;     //Variable for storing input value    
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword    
    strInputValue = ReadExcel(intIniRwNo, c_intInputValCol);  //Reads the input value from c_intInputValCol
    var FileName = g_sProjPath+"\Stores\\Files\\"+strInputValue;
    strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    ExpectedResult = "File "+ strInputValue + " shall be Deleted";
    if(aqFileSystem.Exists(FileName)){     
        aqFileSystem.DeleteFile(FileName);
        if(!aqFileSystem.Exists(FileName)){          
          intStepCounter = 0;
          ActualResult = "File "+ strInputValue + " is Deleted";
          Result = "PASS";
        }else{          
          ActualResult = "File "+ strInputValue + " is not Deleted";          
        }
      }    
   }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    ActualResult = "File "+ strInputValue + " is not Deleted due to the exception "+e.description;
  }     
  finally{
    if(intStepCounter == 1){      
      StopCurrentTest();
      Result = "FAIL";
    }
    fnInsertResult("FileDelete",strStep,ExpectedResult,ActualResult,Result,strStepType);
  } 
  g_stepnum = strStep;           
}



//*************************************************************************************************
// Function Name        : PreconditionDeleteUser
// Function Description : To delete a user using liferay portal             
// Inputs               : None     
// Returns              : None
//**************************************************************************************************
function PreconditionDeleteUser(){
  try{
    
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol  
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
        var NoUserFound = BuildWebObj(g_dicColNames_obj.item("liferay_nousersfoundmsg"));
        Object = WaitForObject(NoUserFound, true);
        var DeactivateUserCheckbox = BuildWebObj(g_dicColNames_obj.item("Liferay_UserCheckBox"));
        DeactivateUserCheckbox = WaitForObject(DeactivateUserCheckbox, false);
        var DeleteUserCheckbox = BuildWebObj(g_dicColNames_obj.item("Liferay_Del_UserCheckbox"));
        DeleteUserCheckbox = WaitForObject(DeleteUserCheckbox, false);        
        if (Object.exists){      
        fnInsertResult("PreconditionDeleteUser",strStep,"The User Email Address shall not be present in environment.","The User Email Address is not present in the environment","PASS",strStepType);
        }          
        else if (DeactivateUserCheckbox.Exists){
          DeactivateUserCheckbox.Click();
          var DeactivateButton = BuildWebObj(g_dicColNames_obj.item("Liferay_DeactivateButton"));
          DeactivateButton = WaitForObject(DeactivateButton, false);
          DeactivateButton.Click();
          
          var DeactivateOKButton = BuildWebObj(g_dicColNames_obj.item("Liferay_DeactivateOKButton"));
          DeactivateOKButton = WaitForObject(DeactivateOKButton, false);
          DeactivateOKButton.Click(); 
          fnInsertResult("PreconditionDeleteUser",strStep,"The User Email Address shall be deactivated from the environment.","The User Email Address is deactivated from the environment","PASS",strStepType);             
        }
        else if (DeleteUserCheckbox.Exists){
          DeleteUserCheckbox.Click();
          var DeleteButton = BuildWebObj(g_dicColNames_obj.item("Liferay_Del_DeleteButton"));
          DeleteButton = WaitForObject(DeleteButton, false);
          DeleteButton.Click();
          
          var DeleteOKButton = BuildWebObj(g_dicColNames_obj.item("Liferay_DeleteOKButton"));
          DeleteOKButton = WaitForObject(DeleteOKButton, false);
          DeleteOKButton.Click(); 
          fnInsertResult("PreconditionDeleteUser",strStep,"The User Email Address shall be deleted from the environment.","The User Email Address is deleted from the environment","PASS",strStepType);             
        }
            
      
    
    g_stepnum = strStep;
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }

}

//*************************************************************************************************
// Function Name        : PreconditionDeleteCompany
// Function Description : To delete a Company using liferay portal             
// Inputs               : None     
// Returns              : None
//**************************************************************************************************
function PreconditionDeleteCompany(){
  try{
    
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol  
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
        var NoSitesFound = BuildWebObj(g_dicColNames_obj.item("liferay_nositesfoundmsg"));
        Object = WaitForObject(NoSitesFound, false);               
        if (Object.exists){      
        fnInsertResult("PreconditionDeleteCompany",strStep,"The Company shall not be present in environment.","The Company is not present in the environment","PASS",strStepType);
        }          
        else {
          var ActionsButton = BuildWebObj(g_dicColNames_obj.item("Liferay_ActionButton"));
          ActionsButton = WaitForObject(ActionsButton, false);
          ActionsButton.Click();
          var DeactivateButton = BuildWebObj(g_dicColNames_obj.item("Liferay_SitesDeactivateButton"));
          DeactivateButton = WaitForObject(DeactivateButton, false);
          var DeleteButton = BuildWebObj(g_dicColNames_obj.item("Liferay_SitesDeleteButton"));
          DeleteButton = WaitForObject(DeleteButton, false);
          if (ActionsButton.Exists)
          {
          DeactivateButton.Click();
          var DeactivateOKButton = BuildWebObj(g_dicColNames_obj.item("Liferay_SitesDeactivateOKButton"));
          DeactivateOKButton = WaitForObject(DeactivateOKButton, false); 
          DeactivateOKButton.Click();
          
                        
          var ActionsButton = BuildWebObj(g_dicColNames_obj.item("Liferay_ActionButton"));
          ActionsButton = WaitForObject(ActionsButton, false);
          ActionsButton.Click();
          DeleteButton.Click();
          var DeleteOKButton = BuildWebObj(g_dicColNames_obj.item("Liferay_SitesDeactivateOKButton"));
          DeleteOKButton = WaitForObject(DeleteOKButton, false); 
          DeleteOKButton.Click();
          fnInsertResult("PreconditionDeleteCompany",strStep,"The Company shall be deactivated and deleted from the environment.","The Company is deactivated and deleted from the environment","PASS",strStepType);
          }     
        }    
    
    g_stepnum = strStep;
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
 }
 
//*************************************************************************************************
// Function Name        : DeletePatient
// Function Description : To delete patient from database.      
// Inputs               : None     
// Returns              : None
//**************************************************************************************************

function DeletePatient(){
  try{
    var intIniRwNo;       //Variable for storing initial row number
    var strInputValue;    //Variable for storing input value
    var AConnection;      //variable for storing connection
    var RecSet;           //variable for storing recordset
    var NumOfRecord;      //Variable to store number of records after query is executed
    var RecordValue;          //Variable to store the value of the record
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);
    strInputValue1 = ReadExcel(intIniRwNo,c_intInputVal1Col);
    var Query = new Array();
    Query = strInputValue1.split("&&");
    
    var strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol
    var strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    // Create a Connection object
    AConnection = ADO.CreateADOConnection();
    // Specify the connection string  
    AConnection.ConnectionString ="Provider=OraOLEDB.Oracle;"+"Data Source=(DESCRIPTION =(ADDRESS_LIST =(ADDRESS =(PROTOCOL = TCP)(HOST = "+g_DBHost+")(PORT = "+g_DBPort+")))(CONNECT_DATA =(SERVICE_NAME = "+g_DBServiceName+")));" +"User ID="+g_DBUserName+";"+"Password="+g_DBPassword+";"; 
   //Suppress the login dialog box
    AConnection.LoginPrompt = false;
    AConnection.Open();
    // Execute a query
    if(aqString.Find(strInputValue,"DBTablename")!= -1){
      strInputValue = aqString.Replace(strInputValue,"DBTablename",g_DBTablename);
    }
    RecSet = AConnection.Execute_(strInputValue)
    NumOfRecord = RecSet.RecordCount; 
     
    if(NumOfRecord > 0){
    RecordValue = RecSet.Fields(strObjectName).Value;
    Query[0] = Query[0]+"'"+RecordValue+"'"
    RecSet = AConnection.Execute_(Query[0])
    Query[1] = Query[1]+"'"+RecordValue+"'"
    RecSet = AConnection.Execute_(Query[1])
    //WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheetName,RecordValue);//Write the return value from the query in excel sheet 
      fnInsertResult("DeletePatient",strStep,"Query ("+strInputValue+") and ("+Query[0]+") and ("+Query[1]+") shall be executed","Query ("+strInputValue+") and ("+Query[0]+") and ("+Query[1]+") is executed","PASS",strStepType);
    }
    else{
      fnInsertResult("DeletePatient",strStep,"The values shall not be present in the Database.","The values are not present in the Database.","PASS",strStepType);  
    }
    AConnection.Close();  
    g_stepnum = strStep; 
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : PlaceOrder
// Function Description : To place order in patient portal                   
// Inputs               : None     
// Returns              : None
//*************************************************************************************************
function PlaceOrder(){
  try{
      var intIniRwNo;        //Variable for storing initial row number
      var intEndRwNo;        //Variable for storing end row number
      var strObjectName;     //Variable for storing object name
      var objName;           //Variable for storing object 
      var strInputValue;     //Variable for storing input value 
      var strAction;         //Variable for storing action 
      var strStep;           //Variable for storing step number
      var intStepCounter = 0;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
      var TotalStepCounter = 0;
      intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
      intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
      strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
      SolutionTable = BuildWebObj(g_dicColNames_obj.item(strObjectName));
      SolutionTable = WaitForObject(SolutionTable, true);
      if ((SolutionTable.Exists)!=-1){   
          fnInsertResult("PlaceOrder",strStep,"The "+strObjectName+" shall be applicable for the patient.","The "+strObjectName+" are applicable for the patient.","PASS",strStepType); 
          for (intIterCnt = intIniRwNo+1 ; intIterCnt <= intEndRwNo ; intIterCnt++){      
              strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol      
              objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
              strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
              strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
              strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol 
              Wait(3);
              intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
              Wait(6); //This page take time to load
              TotalStepCounter = TotalStepCounter + intStepCounter; //Add the intStepCounter for all steps  
              VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt); 
          } 
      }else{
          fnInsertResult("PlaceOrder",strStep,"The "+strObjectName+" shall not be applicable for the patient.","The "+strObjectName+" are not applicable for the patient.","PASS",strStepType);
      }
 }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }finally{
      UpdateSummaryStatus(TotalStepCounter) ;
      g_stepnum = strStep;
 }
}



//*************************************************************************************************
// Function Name        : WriteConfirmationNumber
// Function Description : To write the  patient unique id in testdata sheet            
// Inputs               : None
// Returns              : None         
//************************************************************************************************ 
function WriteConfirmationNumber(intRow){
 try{
    var ConfirmNo;
    var objName;  
    
    //Build object
    objName = BuildWebObj(g_dicColNames_obj.item("PlaceOrder_Confirmationnumber"));  //Build object from the given object name
    ConfirmNo = WaitForObject(objName, false);
    if (!IsExists(ConfirmNo)) {
      Log.Error("Unable to fetch Confirmation Number");
    } 
    var Confirmationnumber = ConfirmNo.ContentText;//Get Patient Activation Code
    WriteExcel(intRow,5,g_TestDataSheetName,Confirmationnumber);  //Write Baxter patient id in testdata sheet
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : ViewOrder
// Function Description : To view placed order in patient portal                   
// Inputs               : None     
// Returns              : None
//*************************************************************************************************
function ViewOrder(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol  
        //This function performs the strActions specified in the excel 
//        
        //strStepType = ReadExcel (intIterCnt,c_intStepType); //Reads the step type from c_intStepType
        
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
//        
//        
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);  
      }
    }  
 }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  g_stepnum = strStep;
}

//*************************************************************************************************
// Function Name        : DateSelection
// Function Description : To enter date from date pick control                   
// Inputs               : Date specified in testdata     
// Returns              : None    
//*************************************************************************************************
function DateSelection(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strResult;         //Variable for storing result
    var Arrayinput = new Array();  //Arry to store date
    var objnotexist = 0;   //Variable to verify if the object exist
    var intStepCounter = 1;  //Variable for storing pass/fail count
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    //Read object name from excel 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    if (strObjectName != null){    
      //Read input value from excel
      strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
      var d = new Date(strInputValue);
      var n = d.toString(); 
      //to get the date in expected format
      Arrayinput = n.split(" ");
      var inputnum = Arrayinput[2]; 
      var month=Arrayinput[1];
      var year= Arrayinput[5];
      //To select year from calender 
      var yearobj = BuildWebObj(g_dicColNames_obj.item("DateSelect_Year"));
      yearobj = eval(yearobj);
      yearobj.ClickItem(trim(year));
      //To select month from calender 
      var monthobj = BuildWebObj(g_dicColNames_obj.item("DateSelect_Month"));
      monthobj = WaitForObject(monthobj, true);
      monthobj.ClickItem(trim(month));
      //Read strStep from excel
      strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
      //Read strAction from excel
      strAction = ReadExcel(intIniRwNo,c_intActionCol);  //Reads the action from c_intActionCol     
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
      //find ChildObject using finall method
      strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);
      objArry = BuildWebObj(g_dicColNames_obj.item(strObjectName))
      objName = WaitForObject(objArry, true);
      ChildObj = objName.FindAllChildren("objectType","Cell", 30);
      ChildObj = (new VBArray(ChildObj)).toArray();
      //Click on a particular date
      for(var intCounter = 0; intCounter <= (ChildObj.length)- 1; intCounter++){
        if((Trim(ChildObj[intCounter].contentText) == Trim(inputnum))){
          ChildObj[intCounter].Click();
          intStepCounter = 0;
          break;          
          }
        }
      }   
      var inputdate=inputnum+"-"+month+"-"+year 
      if ((intStepCounter == 0)&&(objnotexist==0)) {    
      
      fnInsertResult("DateSelection",strStep,"User shall be able to enter a date","User is entered a date of "+inputdate+" ","PASS",strStepType);
      }
      else{    
        fnInsertResult("DateSelection",strStep,"User shall be able to enter a date","User is not entered a date of "+inputdate+" ","FAIL",strStepType);
        g_intFailCnt = g_intFailCnt + 1; 
      }
      g_stepnum = strStep; 
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : RenameFile
// Function Description : To rename the file which is present in the Project folder                  
// Inputs               : File name present in Stores folder     
// Returns              : None    
//*************************************************************************************************

function RenameFile(){
  try{
    var intIniRwNo;        //Variable for storing initial row number   
    var strInputValue;     //Variable for storing input value    
    var strStep = "";            //Variable for storing step number
    var ExistingFileName        //variable to store the old file name      
    var NewFileName;        //variable to store the rename file
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol   
    strTime = (aqConvert.DateTimeToFormatStr(aqDateTime.Time(), "%H%M%S"));
    strDate = (aqConvert.DateTimeToFormatStr(aqDateTime.Today(), "%y%m%d"));
    //strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
    
    ExistingFileName = strInputValue;
    InputValArray = strInputValue.split(".");
    
    NewFileName = InputValArray[0]+"_"+strDate+"_"+strTime+"."+InputValArray[1];
               
    strInputValue=g_sProjPath+"\Stores\\Files\\"+strInputValue;
     ExpectedMessage = ExistingFileName+ " shall be renamed successfully to " +NewFileName;

    aqFileSystem.CopyFile(strInputValue, g_sProjPath+"\Stores\\Files\\"+NewFileName);
          
   
    if(aqFileSystem.Exists(g_sProjPath+"\Stores\\Files\\"+NewFileName)){  
      WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheetName,NewFileName);   
      ActualMessage = ExistingFileName+ " is renamed successfully to "+NewFileName;
      intStepCounter = 0;
    }
    else{
      ActualMessage = ExistingFileName+ " is not renamed successfully to "+NewFileName;
      intStepCounter = 1;
      
    } 
    g_stepnum = strStep; 
   }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  finally{
    if(intStepCounter == 1){
    ActualMessage = ExistingFileName+ " is not renamed successfully to "+NewFileName;
      StopCurrentTest();
    }
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo); 
  }               
} 
//*************************************************************************************************
// Function Name        : TakeScreenshotTab
// Function Description : Take a Screenshot for the active tab of the browser when multiple tabs are open
// Inputs               : None
// Returns              : None
//************************************************************************************************
function TakeScreenshotTab() {
try {
		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing end row number
		var strInputValue; //Variable for storing input value
		var strStep = "";  //Variable for storing step number
    var strStepType; // Variable for storing StepType
		var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    //var intCount = 0; // Variable for compare with intStepCounter
		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
		for (intIterCnt = intIniRwNo; intIterCnt <= intIniRwNo; intIterCnt++) {
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
			strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
			Delay(strInputValue);
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt); 
			

		}
		// Click Leave this page option on windows explorer
		if (Sys.Browser("iexplore").BrowserWindow(0).Window("WorkerW", "Navigation Bar", 1).Window("ReBarWindow32", "", 1).Window("TabBandClass", "", 1).Window("DirectUIHWND", "", 1).MSAAObject("client").MSAAObject("page_tab_list_Tab_Row").MSAAObject("pt_benonhi911globalbaxtercom_Tab_Group_8").Exists) {
			Sys.Browser("iexplore").BrowserWindow(0).Window("WorkerW", "Navigation Bar", 1).Window("ReBarWindow32", "", 1).Window("TabBandClass", "", 1).Window("DirectUIHWND", "", 1).MSAAObject("client").MSAAObject("page_tab_list_Tab_Row").MSAAObject("pt_benonhi911globalbaxtercom_Tab_Group_8").Click();
		  Delay(3000);
    }
		
    
			if ((intStepCounter == 0) && (intIterCnt < 1000)) {
				//insert stepwise result
				
        fnInsertResult("TakeScreenshotTab",strStep,"The link shall be opened in a new window","The link is opened in a new window ","PASS",strStepType);
			} else {
				
        fnInsertResult("TakeScreenshotTab",strStep,"The link shall be opened in a new window","The link is not opened in a new window ","FAIL",strStepType);
        g_intFailCnt = g_intFailCnt + 1; 
			}
		
		 g_stepnum = strStep; 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}
//*************************************************************************************************
//Function Name        : VerifyBtnLabel
//Function Description : To Verify Label on the Patient List Dashboard Page
//Inputs               : None
//Returns              : None
//************************************************************************************************

function VerifyBtnLabel() {
	try {
		var intIniRwNo;
		var intEndRwNo;
		var strObjectName;
		var objName;
		var strInputValue;
		var strAction;
		var strStepNumber;
		var intStepCounter = 1;
    intIniRwNo = g_intStrtRow;
		intEndRwNo = g_intEndRow;

		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strObjectName = ReadExcel(intIterCnt, c_intObjNameCol);
			if (strObjectName != null) {
				objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
				strInputValue = ReadExcel(intIterCnt, c_intInputValCol);
				strAction = ReadExcel(intIterCnt, c_intActionCol);
				strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
				intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt); 
				
			}
		}
		
    g_stepnum = strStep;
	} catch (e) {
		
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}


////*************************************************************************************************
//// Function Name        : ReportGeneration
//// Function Description : Clinic User can Generate CS Report in CSP
//// Inputs               : None
//// Returns              : None
////************************************************************************************************

function ReportGeneration() {
	try {
		var intIniRwNo;
		var intEndRwNo;
		var strObjectName;
		var objName;
		var strInputValue;
		var strAction;
		var strStep = ""; 
    var strStepType; // Variable for storing StepType
		var intStepCounter = 1;
    //var intCount = 0; // Variable for compare with intStepCounter
		var strInputValue;

		intIniRwNo = g_intStrtRow;
		intEndRwNo = g_intEndRow;

		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strObjectName = ReadExcel(intIterCnt, c_intObjNameCol);
			if (strObjectName != null) {
				objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
				strInputValue = ReadExcel(intIterCnt, c_intInputValCol);
				strAction = ReadExcel(intIterCnt, c_intActionCol);
				strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
				}
		}
    
    g_stepnum = strStep;
	} catch (e) {
		
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : UpdateTodoRules
// Function Description : Update values in Todo Rules screen
// Inputs               : Todo Rules values
// Returns              : None
//*************************************************************************************************


function UpdateTodoRules() {
	try {
		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing end row number
		var strObjectName; //Variable for storing object name
		var objName; //Variable for storing object
		var strInputValue; //Variable for storing input value
		var strAction; //Variable for storing action
		var strStep = "";  //Variable for storing step number
		var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    //var intCount = 0; // Variable for compare with intStepCounter
		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			//Read object name from excel
			strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
			if (strObjectName != null) {
				//Build object
				objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
				//Read input value from excel
				strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
				//Read strStepNumber from excel
				strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        
				//Read strAction from excel
				strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
        strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
				//This function performs the strActions specified in the excel
				intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
				VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
        
			}
		}
		
    
		g_stepnum = strStep;
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : CreateServiceRequest
// Function Description : Create Service Request
// Inputs               : None
// Returns              : None
//*************************************************************************************************

function CreateServiceRequest() {
	try {
		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing end row number
		var strObjectName; //Variable for storing object name
		var objName; //Variable for storing object
		var strInputValue; //Variable for storing input value
		var strAction; //Variable for storing action
		var strUserName; //Variable for storing UserName
		var strStep = "";  //Variable for storing step number
		var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
			if (strObjectName != null) {
				objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
				strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
				strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
        strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        //This function performs the strActions specified in the excel
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
				

			}
		}
		
		    g_stepnum = strStep;
    	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}
//*************************************************************************************************
// Function Name        : ViewServiceRequest
// Function Description : View Service Request
// Inputs               : None
// Returns              : None
//*************************************************************************************************

function ViewServiceRequest() {
	try {
		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing end row number
		var strObjectName; //Variable for storing object name
		var objName; //Variable for storing object
		var strInputValue; //Variable for storing input value
		var strAction; //Variable for storing action
		var strUserName; //Variable for storing UserName
    var strStepType; // Variable for storing StepType
		var strStep = "";  //Variable for storing step number
		var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
			if (strObjectName != null) {
				objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
				strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
				strStepNumber = ReadExcel(intIterCnt, c_intStepNumberCol); //Reads the step from c_intStepTypeCol
				strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
        strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
        strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
        //This function performs the strActions specified in the excel
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
				}
		}
		
		
				
		g_stepnum = strStep;
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : ViewPatientCalendar
// Function Description : View Patient Calendar
// Inputs               : None
// Returns              : None
//*************************************************************************************************

function ViewPatientCalendar() {
	try {
		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing end row number
		var strObjectName; //Variable for storing object name
		var objName; //Variable for storing object
		var strInputValue; //Variable for storing input value
		var strAction; //Variable for storing action
		var strUserName; //Variable for storing UserName
		var strStep = "";  //Variable for storing step number
		var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
			if (strObjectName != null) {
				objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
				strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
				strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
				strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
				//This function performs the strActions specified in the excel
				intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			}
		}
		
		
		g_stepnum = strStep;
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : RenewTherapy
// Function Description : Renew Therapy
// Inputs               : None
// Returns              : None
//*************************************************************************************************

function RenewTherapy() {
	try {
		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing end row number
		var strObjectName; //Variable for storing object name
		var objName; //Variable for storing object
		var strInputValue; //Variable for storing input value
		var strAction; //Variable for storing action
		var strUserName; //Variable for storing UserName
		var strStep = "";  //Variable for storing step number
		var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
   	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
			if (strObjectName != null) {
				objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
				strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
				strStep = ReadExcel(intIterCnt, c_intStepCol); //Reads the step from c_intStepTypeCol
				strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
				//This function performs the strActions specified in the excel
				intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			}
		}
		
		g_stepnum = strStep;
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : CreateOrEditTherapy
// Function Description : Create Therapy
// Inputs               : None
// Returns              : None
//*************************************************************************************************
function CreateOrEditTherapy() {
	try {
		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing end row number
		var strObjectName; //Variable for storing object name
		var objName; //Variable for storing object
		var strInputValue; //Variable for storing input value
		var strAction; //Variable for storing action
		var strStep = "";  //Variable for storing step number
		var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
  

		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
			if (strObjectName != null) {
				objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
				strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
				strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
				strStep = ReadExcel(intIterCnt, c_intStepCol); //Reads the step from c_intStepTypeCol
				//This function perForms the strActions specIfied in the excel
				intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			}
		}
		
		
    g_stepnum = strStep;
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : ViewTherapy
// Function Description : View and Print Therapy
// Inputs               : None
// Returns              : None
//*************************************************************************************************

function ViewTherapy() {
	try {
		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing end row number
		var strObjectName; //Variable for storing object name
		var objName; //Variable for storing object
		var strInputValue; //Variable for storing input value
		var strAction; //Variable for storing action
		var strStep = "";  //Variable for storing step number
		var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    

		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
			if (strObjectName != null) {
				objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
				strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
				strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
				strStep = ReadExcel(intIterCnt, c_intStepCol); //Reads the step from c_intStepTypeCol
				//This function perForms the strActions specIfied in the excel
				intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			}
		}
		
		g_stepnum = strStep;
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : EditTherapy
// Function Description : To edit a therapy
// Inputs               : None
// Returns              : None
//*************************************************************************************************

function EditTherapy() {
try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)

	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
			objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
			strStep = ReadExcel(intIterCnt, c_intStepCol); //Reads the step from c_intStepTypeCol
			strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
			//This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
   }
	}
	
  g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : RegisterPTUser
// Function Description : To register a patient user in clinic portal
// Inputs               : Patient email address
// Returns              : Patient User
//*************************************************************************************************


function RegisterPTUser() {
try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
			objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : ApproveTherapy
// Function Description : Approve patient's therapy based on test case CS.ES.TC.001
// Inputs               : None
// Returns              : None
//**************************************************************************************************
function ApproveTherapy() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
			objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : SelectEsignLstPatient
// Function Description : Select patient's eSign record based on test case CS.ES.TC.001
// Inputs               : None
// Returns              : None
//**************************************************************************************************
function SelectEsignLstPatient() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
		objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : eSign
// Function Description : Approve or Reject Therapy
// Inputs               : None
// Returns              : None
//*************************************************************************************************
function eSign() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
			objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}
//*************************************************************************************************
// Function Name        : SearchProductExist
// Function Description : Search Prodfuct On the Pop-up
// Inputs               : None
// Returns              : None
//*************************************************************************************************
function SearchProductExist() {
      try {
            var intIniRwNo; //Variable for storing initial row number
            var intEndRwNo; //Variable for storing end row number
            var strObjectName; //Variable for storing object name
            var strInputValue; //Variable for storing input value
            var strAction; //Variable for storing Action
            var intStepCounter = 1; //Variable for storing flag
    
            intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
            //Read object name from excel
            intIterCnt = intIniRwNo;
            strObjectName = ReadExcel(intIniRwNo, c_intObjNameCol);
            //Read browser input from excel
            strBrowserinput = ReadExcel(intIniRwNo, c_intStepCol);
            strInputValue = ReadExcel(intIniRwNo, c_intInputValCol);
            strAction = ReadExcel(intIniRwNo, c_intActionCol);
            //Read strStepNumber from excel
            strStep = ReadExcel(intIniRwNo, c_intStepCol);
            //Build object
            var ArryObject =  new Array();
            ArryObject = BuildWebObjOld(g_dicColNames_obj.item("CS_TM_DIS_PopularPo-up"));
            var Object = WaitForObject(ArryObject, true);
            var flag = false;
            if (Object.Exists) {
              ChildObj = Object.FindAllChildren("className", "firsttd textEllipsis width15 longItemNumber", 500); //Find all the child objects of the type Link
              ChildObj = new VBArray(ChildObj).toArray();
              for (intChildCnt = 0; intChildCnt <= ChildObj.length - 1; intChildCnt++) {
                if (aqString.Compare((ChildObj[intChildCnt].contentText), strInputValue, true) == 0) {
                  flag = true;
                  break;
                }
              }        
              if (flag){
                Log.Message(strStep + " - " + "Product is displayed on the page");
              } else {
                intStepCounter = intStepCounter + 1;
                Log.Message(strStep + " - " + "Product is not displayed on the page");
              }
            } else {
              Log.Message(strStep + " - " + "Object does not exist");
              intStepCounter = intStepCounter + 1;
            }
            
            VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
            g_stepnum = strStep;
      } catch (e) {
            Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
      }
}

//*************************************************************************************************
// Function Name        : SearchProductNotExist
// Function Description : Search Product Not On the Pop-up
// Inputs               : None
// Returns              : None
//*************************************************************************************************
function SearchProductNotExist() {
      try {
            var intIniRwNo; //Variable for storing initial row number
            var intEndRwNo; //Variable for storing end row number
            var strObjectName; //Variable for storing object name
            var strInputValue; //Variable for storing input value
            var strAction; //Variable for storing Action
            var intStepCounter = 1; //Variable for storing flag
               intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
            //Read object name from excel
              intIterCnt = intIniRwNo;
            strObjectName = ReadExcel(intIniRwNo, c_intObjNameCol);
            //Read browser input from excel
            strBrowserinput = ReadExcel(intIniRwNo, c_intStepCol);
            strInputValue = ReadExcel(intIniRwNo, c_intInputValCol);
            strAction = ReadExcel(intIniRwNo, c_intActionCol);
            //Read strStepNumber from excel
            strStepNumber = ReadExcel(intIniRwNo, c_intStepCol);
            //Build object
            var ArryObject =  new Array();
            ArryObject = BuildWebObjOld(g_dicColNames_obj.item("CS_TM_DIS_PopularPo-up"));
            var Object = WaitForObject(ArryObject, true);
            var flag = false;
            if (Object.Exists) {
              ChildObj = Object.FindAllChildren("className", "firsttd textEllipsis width15 longItemNumber", 500); //Find all the child objects of the type Link
              ChildObj = new VBArray(ChildObj).toArray();
              for (intChildCnt = 0; intChildCnt <= ChildObj.length - 1; intChildCnt++) {
                if (aqString.Compare((ChildObj[intChildCnt].contentText), strInputValue, true) == 0) {
                  flag = true;
                  break;
                }
              }        
              if (flag){
                intStepCounter = intStepCounter + 1;
                Log.Message(strStep + " - " + "Product is displayed on the page");
              } else {
                Log.Message(strStep + " - " + "Product is not displayed on the page");
              }
            } else {
              Log.Message(strStep + " - " + "Object does not exist");
              intStepCounter = intStepCounter + 1;
            }
            
            VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
            g_stepnum = strStep;
      } catch (e) {
            Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
      }
}

//************************************************************************************************
// Function Name        : VerifySpecificString
// Function Description : Verify Special String in dropdown
// Inputs               : None
// Returns              : None
//*************************************************************************************************
function VerifySpecificString() {
	try {
		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing End row number
		var strObjectName; //Variable for storing object name
		var strInputValue; //Variable for storing input value
		var intStepCounter = 1; //Variable for storing flag
		var intAction; //Variable for storing Action
		var objName; //Variable for storing step number
		var intIterCnt;
		var objDesc;
		var DropdownOptions;
		var Counter = 0;
		var strLogMessage;

		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow;
		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol);
			strTestData = ReadExcel(intIterCnt, c_TestData);
			strStep = ReadExcel(intIterCnt, c_intStepCol);
			strObjectName = ReadExcel(intIterCnt, c_intObjNameCol);
			strAction = ReadExcel(intIterCnt, c_intActionCol);

			if (strObjectName != null) {
				objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
				objDesc = WaitForObject(objName, true);
				DropdownOptions = Trim(objDesc.wItemList); //Get all dropdown items
				aqString.ListSeparator = ";";
				var DropdownLength = aqString.GetListLength(DropdownOptions);
				for (var i = 0; i < DropdownLength; i++) {
					var DropdownItem = aqString.GetListItem(DropdownOptions, i); //Seperate the list item to Array
					if (DropdownItem != "Select" && aqString.Find(DropdownItem, strInputValue) == -1) {
						strLogMessage = strInputValue + " is NOT the only option in " + strObjectName;
						Log.Message(strLogMessage);
						intStepCounter = intStepCounter + 1;
						break;
					}
				}
			}
			VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
		}
		
		//Generate Result
		g_stepnum = strStep;
	} catch (e) {
		log.Message(g_strFunCall + e.description);
	}
}

//*************************************************************************************************
// Function Name        : UpdateUserLanguage
// Function Description : To Update User Language
// Inputs               : To-be updated Language to be selected
// Returns              : None
//************************************************************************************************
function UpdateUserLanguage() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
			objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : VerifyIndividualFields
// Function Description : To Verify the Lables, Default Value, Max Length and Valid Values for a field
// Inputs               : None
// Returns              : None
//*************************************************************************************************
function VerifyIndividualFields() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
			objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : ClickObject
// Function Description : To click on link,button and image
// Inputs               : None
// Returns              : None
//************************************************************************************************
function ClickObject() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
			objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
		}
	}
   g_stepnum = strStep;
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}
//*************************************************************************************************
// Function Name        : VerifyFieldStatus
// Function Description : Verify Field Staus
// Inputs               :
// Returns              : None
//*************************************************************************************************
function VerifyFieldStatus() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
    Delay(3000);
			objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : VerifyMessage
// Function Description : Verify Message
// Inputs               :
// Returns              : None
//*************************************************************************************************
function VerifyMessage() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
      Wait(3);
			objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : VerifyNavigatedScreen
// Function Description : Verify Navigated Screen
// Inputs               :
// Returns              : None
//*************************************************************************************************
function VerifyNavigatedScreen() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
    Delay(5000);
			objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : VerifyFormat
// Function Description : Verify Format
// Inputs               :
// Returns              : None
//*************************************************************************************************
function VerifyFormat() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
    Delay(3000);
			objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : VerifyRequiredField 
// Function Description : Verify Required Field
// Inputs               :
// Returns              : None
//*************************************************************************************************
function VerifyRequiredField() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
    Delay(3000);
			objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : CalendarWidget
// Function Description : Calendar Widget
// Inputs               :
// Returns              : None
//*************************************************************************************************
function CalendarWidget() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
    Delay(3000);
			objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : VerifyPopup
// Function Description : Verify Popup
// Inputs               :
// Returns              : None
//*************************************************************************************************
function VerifyPopup() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
			objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : RegisterPatientUser
// Function Description : Register Patient User
// Inputs               :
// Returns              : None
//*************************************************************************************************
function RegisterPatientUser() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
    Delay(3000);
			objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}
//*************************************************************************************************
// Function Name        : VerifyMessagelocation
// Function Description : Verify Message location
// Inputs               :
// Returns              : None
//*************************************************************************************************
function VerifyMessagelocation() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}
//************************************************************************************************
// Function Name        : EditClinic
// Function Description : To click on ViewDeviceSettings link and navigate to device settings page
// Inputs               : None
// Returns              : None
//**************************************************************************************************
function EditClinic() {
	try {
		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing end row number
		var strObjectName; //Variable for storing object name
		var objName; //Variable for storing object
		var strInputValue; //Variable for storing input value
		var strAction; //Variable for storing action
		var strUserName; //Variable for storing UserName
		var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strObjectName = ReadExcel(intIterCnt, c_intObjNameCol);
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads input value from c_intInputValCol
			strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
			strTestData = ReadExcel(intIterCnt, c_TestData);
			strStep = ReadExcel(intIterCnt, c_intStepCol); //Reads the step from c_intStepCol
			if (strObjectName != null) {
				//Build object
				VerificationObj = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			}
			//This function performs the strActions specified in the excel
			intStepCounter = DataEntry(strAction, VerificationObj, strInputValue, strObjectName, strStepNumber);
			if (aqString.StrMatches(strObjectName, "PLTF_CM_EditClinic_UpdateSubmitBtn")) {
				Delay(9000);
			}
			if (aqString.StrMatches(strObjectName, "PLTF_CAM_AddUser_AddClinicPop_SubmitBtn")) {
				Delay(9000);
			}
			if (aqString.StrMatches(strObjectName, "PLTF_OCM_AddEditEnableClinic_Review_SubmitBtn")) {
				Delay(9000);
			} else if (aqString.StrMatches(strObjectName, "PLTF_CAM_AddUser_AddClinicPop_SearchBtn")) {
				Delay(9000);
			}

			VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
		}
		
		g_stepnum = strStep;
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}
//*************************************************************************************************
// Function Name        : VerifyOrder
// Function Description : To Verify Order in the List
//**************************************************************************************************
function VerifyOrder() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : Search
// Function Description : To search Clinic, Patient, User and so on
// Inputs               : None
// Returns              : None
//*************************************************************************************************
function Search() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
    Delay(2000);
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : AddOrEdit
// Function Description : To Add Or Edit Clinic, Patient, User and so on
// Inputs               : None
// Returns              : None
//*************************************************************************************************
function AddOrEdit() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}
//*************************************************************************************************
// Function Name        : DateFormat
//*************************************************************************************************
function DateFormat() {
	try{

	var intIniRwNo; //Variable for storing initial row number
	var intEndRwNo; //Variable for storing end row number
	var strObjectName; //Variable for storing object name
	var objName; //Variable for storing object
	var strInputValue; //Variable for storing input value
	var strAction; //Variable for storing action
	var strStep = "";  //Variable for storing step number
	var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
 	intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
	intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
  var todayStr = getDateFormat();
	for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
		strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
		if (strObjectName != null) {
    Delay(2000);
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name  
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
		  strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
      strStepType = ReadExcel(intIterCnt, c_intStepType); //Reads the input value from c_intStepNumberCol
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			
		}
	}
	
   g_stepnum = strStep;
	 
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : getDateFormat
// Function Description : To Transfer Date in DD MM YYYY Format
//*************************************************************************************************
function getDateFormat() {
	//Declare variables
	var g_strExecutionDate;
	var strMonth;
	var strYear;
	var strfixDate;
	var today = new Date(); //creating date object
	//Creating array of months
	var monthnames = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	g_strExecutionDate = today.getDate().toString(); //To get the date in 'dd'format
	strMonth = today.getMonth(); //To get the month
	strYear = today.getFullYear().toString(); //To get the year name
	if (g_strExecutionDate.length < 2) {
		g_strExecutionDate = "0" + g_strExecutionDate;
	}
	strThisMonth = monthnames[strMonth];
	strfixDate = g_strExecutionDate + " " + strThisMonth + " " + strYear;
	return strfixDate; //Returns date in "13 July 2016"
}

//*************************************************************************************************
// Function Name        : VerifyElementWhetherExist
// Function Description : Verify one element whether match the action
// Inputs               : None
// Returns              : None
//*************************************************************************************************
function VerifyElementWhetherExist() {
	try {
		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing end row number
		var strObjectName; //Variable for storing object name
		var objName; //Variable for storing object
		//var strInputValue; //Variable for storing input value
		var strAction; //Variable for storing action
		var strStep = "";  //Variable for storing step number
		var intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
		//var workFlag = false;
		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
			if (strObjectName != null) {
				//workFlag =true;
				Delay(3000);
				objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name 
				//strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
				strTestData = ReadExcel(intIterCnt, c_TestData);
				strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
				strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
				//strAction should be VerifyNotexist or Verifyexist
				if (objName.Exists) {
					if ("VerifyNotexist" == strAction) {
						intStepCounter = intStepCounter + 1;
					}
				} else {
					if ("Verifyexist" == strAction) {
						intStepCounter = intStepCounter + 1;
					}
				}
				VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			}
		}
		g_stepnum = strStep;
	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : DBCheck_DataCompare
// Function Description : To check certain DB data is correct
// Inputs               : DB sql result
// Returns              : None
//************************************************************************************************
function DBCheck_DataCompare() {
	try {

		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing end row number
		var strInputValue; //Variable for storing input value
		var intStepCounter; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
		var AConnection;
		var RecSet; //variable for storing DB Data
		var compareArray = new Array(); //Array for storing the values need to compare
		
		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol);
			strTestData = ReadExcel(intIterCnt, c_TestData);
			strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
			strCompareCol = ReadExcel(intIterCnt, c_CompareValue);

			// Create a Connection object
			AConnection = ADO.CreateADOConnection();
			// Specify the connection string
			// AConnection.ConnectionString = "Provider=MSDASQL.1;" + "Data Source=IT1;" + "User ID=readonly_IT1;" + "Password=it1_read0nly;";
			AConnection.ConnectionString = "Provider=MSDASQL.1;" + "Data Source=" + g_DBSchema + ";" + "User ID=" + g_DBUserName + ";" + "Password=" + g_DBPassword + ";";
			//Suppress the login dialog box
			AConnection.LoginPrompt = false;
			AConnection.Open();
			// Execute a simple query
			RecSet = AConnection.Execute_(strInputValue);
			//		WriteSQLResultToExcel(RecSet);
			var recordArray = ParseRecSetToArray(RecSet);
			WriteArrayResultToExcel(recordArray);
			
			compareArray = strCompareCol.split(";");
			intStepCounter = GetCompareResult(recordArray, compareArray);
			
			VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			g_objXLWB.Close();
			AConnection.Close();
		}
		g_stepnum = strStep;

	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : DBCheck_DataContains
// Function Description : To check certain data is containing in DB
// Inputs               : TempSQLResult.xlsx
// Returns              : None
//************************************************************************************************
function DBCheck_DataContains() {
	try {

		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing end row number
		var strInputValue; //Variable for storing input value
		var intStepCounter; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
		var AConnection;
		var RecSet; //variable for storing DB Data
		var compareArray = new Array(); //Array for storing the values need to compare
		
		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
		// Create a Connection object
		AConnection = ADO.CreateADOConnection();
		// Specify the connection string
		// AConnection.ConnectionString = "Provider=MSDASQL.1;" + "Data Source=IT1;" + "User ID=readonly_IT1;" + "Password=it1_read0nly;";
		AConnection.ConnectionString = "Provider=MSDASQL.1;" + "Data Source=" + g_DBSchema + ";" + "User ID=" + g_DBUserName + ";" + "Password=" + g_DBPassword + ";";
		//Suppress the login dialog box
		AConnection.LoginPrompt = false;
		AConnection.Open();
		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol);
			strTestData = ReadExcel(intIterCnt, c_TestData);
			strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
			strCompareCol = ReadExcel(intIterCnt, c_CompareValue);
			// Execute a simple query
			RecSet = AConnection.Execute_(strInputValue);
			
			var recordArray = ParseRecSetToArray(RecSet);
			WriteArrayResultToExcel(recordArray);
			
			compareArray = strCompareCol.split(";");
			intStepCounter = GetContainsResult(recordArray, compareArray);
			
			VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			g_objXLWB.Close();
			AConnection.Close();
		}
		g_stepnum = strStep;

	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : DBCheck_VerifyTestData
// Function Description : To check DB Data are matching Test Data
// Inputs               : DB sql result, Test Data
// Returns              : None
//************************************************************************************************
function DBCheck_VerifyTestData() {
	try {

		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing end row number
		var strInputValue; //Variable for storing input value
		var strComparesheet; //Variable for storing test data sheet name
		var intStepCounter; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
		var AConnection;
		var RecSet; //variable for storing DB Data
		var compareData = new Array(); //vaiable for storing the excel values for compare

		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strInputValue = ReadExcel(intIterCnt, c_intInputValCol);
			strTestData = ReadExcel(intIterCnt, c_TestData);
			strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
			strComparesheet = ReadExcel(intIterCnt, c_TestData);

			// Create a Connection object
			AConnection = ADO.CreateADOConnection();
			// Specify the connection string
			
			AConnection.ConnectionString = "Provider=MSDASQL.1;" + "Data Source=" + g_DBSchema + ";" + "User ID=" + g_DBUserName + ";" + "Password=" + g_DBPassword + ";";
			//Suppress the login dialog box
			AConnection.LoginPrompt = false;
			AConnection.Open();
			// Execute a simple query
			RecSet = AConnection.Execute_(strInputValue);

			var recordArray = ParseRecSetToArray(RecSet); //Store DB data to array
			compareData = ReadExcelData(g_sTestScriptPath + g_sAutomationTestCase, strComparesheet); //Read Excel Test data sheet

			var resultArray = GetCompareResultArray(recordArray, compareData); //Get compare results array
			intStepCounter = WriteResultArray(g_sTestScriptPath + g_sAutomationTestCase, strComparesheet, resultArray); //Write the Result Array to Test Data

			WriteSQLResultToExcel(RecSet);
			VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
			g_objXLWB.Close();
			AConnection.Close();
		}
		g_stepnum = strStep;

	} catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}
}

//*************************************************************************************************
// Function Name        : VerifyPlaceOrderEnabled
// Function Description : To verify the Place Order button is enabled for a patient in patient portal.                  
// Inputs               : None    
// Returns              : None    
//*************************************************************************************************
function VerifyPlaceOrderEnabled(){
try{
var intIniRwNo;
      var strInputValue;
      var strStep;
      var objTable;
      var TotalRowCount;
      var PageNumber;
      var Rownumber;
      var InputValue;
      intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
      strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
      var Flag = 0;
      objTable = BuildWebObj(g_dicColNames_obj.item("CS_PL_SASO_SearchResultsTable"));
      objTable=WaitForObject(objTable, true);
      TotalRowCount = objTable.rows.length;
//      var LastLink = BuildWebObj(g_dicColNames_obj.item("CS_PL_SASO_LastLink"));
//      LastLink = eval(LastLink);
//      var TotalLinks = LastLink.ChildCount;
      Wait(3);
       for (Rownumber = 1;Rownumber < TotalRowCount; Rownumber++) 
        { 
         PatientName = objTable.Cell(Rownumber,0).Link(0).contentText; 
         
         Modality = objTable.Cell(Rownumber,1).Link(0).contentText;
         if (Modality == "CAPD"){
         objTable.Cell(Rownumber,0).Link(0).Click();
         Wait(3);
         BaxterPID = BuildWebObj(g_dicColNames_obj.item("PatientDetails_BaxterPid"));
         BaxterPID = WaitForObject(BaxterPID, false);
         BaxID = BaxterPID.contentText;
         WriteExcel(intIniRwNo-1,c_intInputVal1Col,g_TestDataSheetName,BaxID);
         SecMenuOrders = BuildWebObj(g_dicColNames_obj.item("SecMenuNav_Orders"));
         SecMenuOrders = WaitForObject(SecMenuOrders, false);
         SecMenuOrders.Click();
         Wait(3);
         PlaceOrderButton = BuildWebObj(g_dicColNames_obj.item("Orders_PlaceOrderButton"));
         PlaceOrderButton = WaitForObject(PlaceOrderButton, false);
         if (PlaceOrderButton.Enabled)
         {
         OrdersHistoryTable = BuildWebObj(g_dicColNames_obj.item("OrdersHistoryTable"));
         OrdersHistoryTable = WaitForObject(OrdersHistoryTable, false);
         if (OrdersHistoryTable.contentText != "No Recent Order History."){
           WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheetName,PatientName);
           fnInsertResult("VerifyPlaceOrderEnabled",strStep,"Place Order Button shall be enabled for "+PatientName+" ","Place Order Button is enabled for "+PatientName+"","PASS",strStepType);
           Flag = 1;
           break;
           }
           else {
           NavToAdvancedPatientSearch();
           }
         }  
         else
         {
         NavToAdvancedPatientSearch();
         }
         }
         else
         {
         NavToAdvancedPatientSearch();
         }       
        }           
      
      if (Flag != 1){
      PatientName = null;
      WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheetName,PatientName);
      
      fnInsertResult("VerifyPlaceOrderEnabled",strStep,"Place Order Button shall be enabled" ,"Place Order Button is not enabled for patients","FAIL",strStepType);
      g_intFailCnt = g_intFailCnt + 1;
      }
          
    g_stepnum = strStep;            
  }
  catch(e)
  {
  Log.Error(g_strFuncCall+e.description);
  }
  finally{
    if(Flag != 1){
      StopCurrentTest();
    }
    g_stepnum = strStep;    
 }  
}
//*************************************************************************************************
// Function Name        : NavToAdvancedPatientSearch
// Function Description : To navigate back to the Advanced Patient Search screen                 
// Inputs               : None    
// Returns              : None    
//*************************************************************************************************
function NavToAdvancedPatientSearch(){
try{
 
 CustomerService_PrimaryMenu =  BuildWebObj(g_dicColNames_obj.item("CustomerService_PrimaryMenu"));
 CustomerService_PrimaryMenu = WaitForObject(CustomerService_PrimaryMenu, true);
 CustomerService_PrimaryMenu.Click();
 Wait(3);
 CS_PL_ShowAdvancedSearchOptionsLink =  BuildWebObj(g_dicColNames_obj.item("CS_PL_ShowAdvancedSearchOptionsLink"));
 CS_PL_ShowAdvancedSearchOptionsLink = WaitForObject(CS_PL_ShowAdvancedSearchOptionsLink, true);
 CS_PL_ShowAdvancedSearchOptionsLink.Click();
 Wait(3);
 CS_PL_ASO_ToDo =  BuildWebObj(g_dicColNames_obj.item("CS_PL_ASO_ToDo's"));
 CS_PL_ASO_ToDo = WaitForObject(CS_PL_ASO_ToDo, true);
 CS_PL_ASO_ToDo.ClickItem("Scheduled Order Past Due");
 Wait(3);
 CS_PL_ASO_SearchButton =  BuildWebObj(g_dicColNames_obj.item("CS_PL_ASO_SearchButton"));
 CS_PL_ASO_SearchButton = WaitForObject(CS_PL_ASO_SearchButton, true);
 CS_PL_ASO_SearchButton.Click();
 Wait(3);
 }
catch(e)
  {
  Log.Error(g_strFuncCall+e.description);
  } 
}
//*************************************************************************************************
// Function Name        : RegisterPatientPortalUser
// Function Description :
// Inputs               :
// Returns              : None
//*************************************************************************************************
function RegisterPatientPortalUser(){
try{
    var intIniRwNo;       //Variable for storing initial row number
    var intEndRwNo;       //Variable for storing end row number
    var strObjectName;    //Variable for storing object name
     
    var strInputValue;    //Variable for storing input value 
    var strStep;        //Variable for storing action 
    var intStepCounter = 0;   //Variable for storing flag

    //Reads initial row and end row for a keyword
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    strInputValue1 = ReadExcel(intIniRwNo,c_intInputVal1Col);  //Reads the input value from c_intInputValCol
    objName = BuildWebObj(g_dicColNames_obj.item("PatientUserTable"));
    objName = WaitForObject(objName, true);
    TotalRowCount = objName.rows.length;
      Wait(3);
       for (Rownumber = 1;Rownumber < TotalRowCount; Rownumber++) 
        { 
         PatientName = objName.Cell(Rownumber,0).contentText; 
         if (aqString.Compare(trim(PatientName),trim(strInputValue1), false) == 0){
         objName.Cell(Rownumber,4).Panel(0).Panel(0).Link(0).Click();
         fnInsertResult("RegisterPatientPortalUser",strStep,"The User shall be able to click RegisterPatientUser link for "+PatientName+"." ,"The User is able to click RegisterPatientUser link for "+PatientName+".","PASS",strStepType);
         }
         else
         {
         fnInsertResult("RegisterPatientPortalUser",strStep,"The User shall be able to click RegisterPatientUser link for "+PatientName+"." ,"The User is not able to click RegisterPatientUser link for "+PatientName+".","PASS",strStepType);
         g_intFailCnt = g_intFailCnt + 1;
         }
        }
    
}
catch(e)
  {
  Log.Error(g_strFuncCall+e.description);
  }
}
//*************************************************************************************************
// Function Name        : SearchPatientInPatientPortal
// Function Description : To search Patient from patient search criteria               
// Inputs               : Patient Name      
// Returns              : None
//**************************************************************************************************
function SearchPatientInPatientPortal(){
  try{
    var intIniRwNo;       //Variable for storing initial row number
    var intEndRwNo;       //Variable for storing end row number
    var strObjectName;    //Variable for storing object name
    var objName;          //Variable for storing object 
    var strInputValue;    //Variable for storing input value 
    var strAction;        //Variable for storing action 
    var strUserName;      //Variable for storing UserName
    var intStepCounter = 1  //Variable for storing flag  
    //Reads initial row and end row for a keyword
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){ 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value rom c_intInputValCol    
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol    
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol     
      if (strObjectName != null){
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));        
      }   
      //This function performs the strActions specified in the excel 
      intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      
    } 
    
    g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//************************************************************************************************
// Function Name        : PerformUserActions
// Function Description : To execute a Keyword  which are related to user actions
// Inputs               : None    
// Returns              : None
//*************************************************************************************************
function PerformUserActions(){

  try{
    var iKeywordStatus = 0;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)    
     
    for (intIterCnt = g_intStrtRow ; intIterCnt <= g_intEndRow ; intIterCnt++){
      iKeywordStatus = iKeywordStatus + RunStep(intIterCnt);
    }//For Ends 
     
    g_stepnum = ReadExcel(intIterCnt-1,c_intStepCol);  //Last Executed Step No
    
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }   
}


//************************************************************************************************
// Function Name        : RunKeyword
// Function Description : To execute a Keyword                   
// Inputs               : ErrorMessage    
// Returns              : None
//*************************************************************************************************
function RunKeyword(ErrorMessage){

  try{
    var iKeywordStatus = 0;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)    
     
    for (intIterCnt = g_intStrtRow ; intIterCnt <= g_intEndRow ; intIterCnt++){
      iKeywordStatus = iKeywordStatus + RunStep(intIterCnt);
    }//For Ends 
     
    g_stepnum = ReadExcel(intIterCnt-1,c_intStepCol);  //Last Executed Step No
    
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally {
    if (iKeywordStatus>0){
      Log.Error(ErrorMessage);
      StopCurrentTest();
    }
  }   
}

//************************************************************************************************
// Function Name        : RunStep
// Function Description : To execute a set of steps which are part of a Keyword                   
// Inputs               : iRowToExecute - Row Number    
// Returns              : None
//*************************************************************************************************
function RunStep(iRowToExecute){

    var sObjectName= "";     //Variable for storing object name
    var oRunObject = "";           //Variable for storing object 
    var sInputValue = "";     //Variable for storing input value 
    var sActionToPerform = "";         //Variable for storing action
    var sStepNo = "";           //Variable for storing step number
    var iStepStatus = 1;
     
     try{
     
      sObjectName= ReadExcel(iRowToExecute,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      sInputValue = ReadExcel(iRowToExecute,c_intInputValCol);  //Reads the input value from c_intInputValCol
      sStepNo = ReadExcel(iRowToExecute,c_intStepCol);  //Reads the step from c_intStepCol      
      sActionToPerform = ReadExcel(iRowToExecute,c_intActionCol);  //Reads the action from c_intActionCol
     
       if (sObjectName!= null){
        //Build object from the given object name
        oRunObject = BuildWebObj(g_dicColNames_obj.item(sObjectName));
        
        //This function performs the strActions specified in the excel 
        iStepStatus = DataEntry(sActionToPerform,oRunObject,sInputValue,sObjectName,sStepNo);
      }
     }catch(e){
          Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
     }finally{
          VerificationPoint(g_strFuncCall,sStepNo,iStepStatus,iRowToExecute);
     }
     
     return iStepStatus;
}

//*************************************************************************************************
// Function Name        : ApplyFilterOnCalculations
// Function Description : To Run the Query and filter the data in CalculationData File.
// Inputs               : N/A     
// Returns              : Record set    
//*************************************************************************************************
 function ApplyFilterOnCalculations(){
    var sSheetName= "";     //Variable for storing object name
    var sWhereClause = "";     //Variable for storing input value
    var sWorkBookName = "";    //Variable for storing input value1  
    var sStepNo = "";           //Variable for storing step number
    var iStepStatus = 1;      //Initialize the status to Fail
    var oConnection;
    var sPrecisionQuery = "";
    
    ExpectedMessage = "Calculations Record shall be filtered.";
    ActualMessage = "Filter Applied and no calculation record was found matching the criteria.";
     
     try{
     
      sSheetName = ReadExcel(g_intStrtRow,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      sWhereClause = ReadExcel(g_intStrtRow,c_intInputValCol);  //Reads the input value from c_intInputValCol
      sWorkBookName = ReadExcel(g_intStrtRow,c_intInputVal1Col);  //Reads the input value from c_intInputValCol
      sStepNo = ReadExcel(g_intStrtRow,c_intStepCol);  //Reads the step from c_intStepCol      
      TotalCalculationRecordsProcessed = 0; //Reset the processsed record count.
      
      //Execute the Query and store the record Set      
       if (!(IsNullorUndefined(sSheetName) && IsNullorUndefined(sWhereClause)&& IsNullorUndefined(sWorkBookName))){
           oConnection = oCDConnection;
           SqlQuery = BuildQuery(sWhereClause,sSheetName);
           SqlQuery = SqlQuery.replace(" WHERE "," WHERE RECORDNU is not NULL AND ");
           oCDRecordSet = RunSQLQuery(oConnection,SqlQuery);
          if (oCDRecordSet.RecordCount>0) {
            iStepStatus = 0;//Pass
            CurrentRecordNU = GetValueFromRecordSet(oCDRecordSet,"RecordNU");
            ActualMessage = "Filter Applied and " + oCDRecordSet.RecordCount + " records found. Current Calculation record is RECORDNU = " + CurrentRecordNU;
            Log.AppendFolder("RECORDNU = " + CurrentRecordNU);
            //Get the Precision record Set
            sPrecisionQuery = BuildQuery("LastName=DecimalPrecision",sSheetName);
            oCDPrecisionRecordSet = RunSQLQuery(oCDConnection,sPrecisionQuery);            
          }
       }
       
       g_stepnum = sStepNo;     
     }catch(e){
          Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
     }finally{
          if (iStepStatus == 0 ){
            Log.Message(sStepNo + " - " + ActualMessage);
          }else{
            Log.Error(sStepNo + " - " + ActualMessage);
            //Hike the Global Counter variables by 1000 to skip the next iterations
            StopCurrentTest();
          }     
          VerificationPoint(g_strFuncCall,sStepNo,iStepStatus,g_intStrtRow);
     }
 }
 
//*************************************************************************************************
// Function Name        : MoveToNextCalculation
// Function Description : To Loop the execution if the record exists in Calculation Data record Set.
// Inputs               : None
// Returns              : intStepCounter - Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
//**************************************************************************************************  
  function MoveToNextCalculation(){
  
    var intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
    var strInputValue = "";
    var strStep = "";      //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    ExpectedMessage = "Next calculation record shall be processed if it exists";
    ActualMessage = "Next calculation record is not processed although it exists";
    intIterCnt = intIniRwNo;
    try{
      // Processes data
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol

      LastExchangeStartTime = "";//Clear the global variables used in previous record.
      LastDwellDuration = ""; //Clear the global variables used in previous record.    
      oRDRecordSet = null; // Set the regimen record set to null, so it is fetched again for the next record. 
      Log.PopLogFolder(); //Close the log of the previous record.
      oCDRecordSet.MoveNext();    
      if(!oCDRecordSet.EOF){
        if (!IsNullorUndefined(strInputValue) && !IsEmpty(strInputValue) && strInputValue > 2){
            intRowIteration_TC = strInputValue-1;
            intStepCounter = 0;
            CurrentRecordNU = GetValueFromRecordSet(oCDRecordSet,"RecordNU");
            ActualMessage = "Next Record found. Current Calculation record is RECORDNU = " + CurrentRecordNU;
            
            //Update the Step No, for the first time when this method is called.
            if (TotalCalculationRecordsProcessed == 0){
                var TotalRecords = oCDRecordSet.RecordCount;
                if (TotalRecords > 1){
                  var StepsBeforeTheLoop = strInputValue - 2;
                  var TotalStepsExecutedTillNow = g_iPass_Count + g_iFail_Count + 1; //Including the current step
                  var ToolsStepsInTheLoop = TotalStepsExecutedTillNow - StepsBeforeTheLoop;
                  var TotalStepsToAdd = ToolsStepsInTheLoop * (TotalRecords-1);
                  g_TotalSteps = Number(g_TotalSteps) + Number(TotalStepsToAdd);
                }
            }
            
            TotalCalculationRecordsProcessed++;
            Log.AppendFolder("RECORDNU = " + CurrentRecordNU);
        }
      }else{
        ActualMessage = "Next record matching the filter criteria does not exist.";
        intStepCounter = 0;
      }
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }finally{
      if (intStepCounter == 0 ){
        Log.Message(strStep + " - " + ActualMessage);
      }else{
        Log.Error(strStep + " - " + ActualMessage);
      }
      VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo);
    }
    return intStepCounter;
  } 

//*************************************************************************************************
// Function Name        : DateFormatter
// Function Description : To convert the from source format into destination format.
// Inputs               : DateInString, SourceFormat, DestinationFormat
// Returns              : Date in Destination format
//**************************************************************************************************  
function DateFormatter(DateInString, SourceFormat, DestinationFormat){
    var ReturnValue = DateInString;
    var Delimiter = null;
    var Day = null;
    var Month = null;
    var Year = null;
    
    try{
    
    //Check if the Input is correct.
    if (IsNullorUndefined(DateInString) || IsEmpty(DateInString)){
        return ReturnValue;
    }
    if (IsNullorUndefined(SourceFormat) || IsEmpty(SourceFormat)){
        return ReturnValue;
    }
    if (IsNullorUndefined(DestinationFormat) || IsEmpty(DestinationFormat)){
        return ReturnValue;
    }        
    
    DateInString = Trim(DateInString).toLowerCase();
    SourceFormat = Trim(SourceFormat).toLowerCase();
    DestinationFormat = Trim(DestinationFormat).toLowerCase();
    
    //Remove all format specifier and then take the remaining as delimiter.
    Delimiter = SourceFormat.replace("dd","");
    Delimiter = Delimiter.replace("mm","");
    Delimiter = Delimiter.replace("yyyy","");
    Delimiter = aqString.GetChar(Delimiter,0);
    
    //Split to get day, month and year.
    DateInStringSplitted = DateInString.split(Delimiter);
    SourceFormatSplitted = SourceFormat.split(Delimiter);
    for (iLoop=0; iLoop<3; iLoop++){
        if (SourceFormatSplitted[iLoop] == "dd"){
            Day = DateInStringSplitted[iLoop].toString();
        }else if (SourceFormatSplitted[iLoop] == "mm"){
            Month = DateInStringSplitted[iLoop].toString();
        }else if (SourceFormatSplitted[iLoop] == "yyyy"){
            Year = DateInStringSplitted[iLoop].toString();
        }                
    }
    
    //Add Leading zeros
    Day = Day.length > 1 ? Day : '0' + Day;
    Month = Month.length > 1 ? Month : '0' + Month;
    
    //Construct the return string.
    ReturnValue = DestinationFormat.replace("dd",Day);
    ReturnValue = ReturnValue.replace("mm",Month);
    ReturnValue = ReturnValue.replace("yyyy",Year);    
    
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue + " " + DestinationFormat;
}
/*************************************************************************************************
   Function Name        : IsSorted
   Function Description : To check if the array is sorted or not.              
   Inputs               : Array and Sorting Type.
   Returns              : true if the array is sorted. False if the array is not sorted.   
*************************************************************************************************/     
function IsSorted(ArrayToCheck, SortingType, ElementType) {
  ReturnValue = false;
  try{
  
    //Ascending Order    
   var ExpectedArray = new Array();
   var PassCount=0;
   var FailCount=0;
   SortingType = Trim(SortingType).toUpperCase();
   ElementType = Trim(ElementType).toUpperCase();
   
   //Remove '---' if the Array has non numeric data and replace it with 0
   if (CompareText(ElementType,"NUMERIC")){
     ArrayToCheck= ReplaceNonNumericVariable(ArrayToCheck);
   }
   
   //Get the values before sorting.
   for(var i=0;i<ArrayToCheck.length;i++){
      ExpectedArray[i]=ArrayToCheck[i];
   }
   
   //Sort the values which are retrived from Screen
   if (CompareText(ElementType,"NUMERIC")){
      ExpectedArray.sort(function(a, b){return a-b});
   }else if (CompareText(ElementType,"DATE")){
      ExpectedArray.sort(function(a, b){return new Date(a) - new Date(b)}); 
   }else {
      ExpectedArray.sort();  
   }
   if (CompareText("DESCENDING",SortingType)){
      ExpectedArray = ExpectedArray.reverse();  
   }
     
  //Compare the Sorted array and Unsorted Array-Ascending
  for(var i=0;i<ArrayToCheck.length;i++){
    if(ArrayToCheck[i]==ExpectedArray[i]) {
      PassCount++;
    }else{
      FailCount++;
    }
  }
  
  if (PassCount == ArrayToCheck.length){
      ReturnValue = true;
  }
       
 }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }  
  return ReturnValue;
} 

  
//*************************************************************************************************
//   Function Name        : ReplaceNonNumericVariable
//   Function Description : To check if the array has non numeric data and replace with 0.              
//   Inputs               : Array.
//   Returns              : Numeric array.   
//************************************************************************************************* 
function ReplaceNonNumericVariable(ArrayToCheck){
  try{
      for(var i=0;i<ArrayToCheck.length;i++){
        if (CompareText("---",ArrayToCheck[i])){
          ArrayToCheck[i] = 0;  
        }
      }
   }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   }  
   return ArrayToCheck;   
}

//*************************************************************************************************
//   Function Name        : VerifyColumnSorting
//   Function Description : To check if Column is sorted              
//   Inputs               : None.
//   Returns              : N/A   
//************************************************************************************************* 
function VerifyColumnSorting(){

    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";      //Variable for storing step number
    var StepStatus = "Fail";  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    var ChildObjectArray = null; 
    var CurrentChildObject = null;
    var ElementType="";
    var InputArray=null;
    var strStepType = null;
   try{
   
    strObjectName = ReadExcel(g_intStrtRow,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    strInputValue = ReadExcel(g_intStrtRow,c_intInputValCol);  //Reads the input value from c_intInputValCol
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    InputArray=strInputValue.split(";");
    strInputValue=InputArray[0];
    ElementType=InputArray[1];
    strStep = ReadExcel(g_intStrtRow,c_intStepCol);  //Reads the step from c_intStepCol
    strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
    ExpectedResult = strObjectName + " column shall be sorted in " + strInputValue.toLowerCase() + " order."
                  
    if (!IsNullorUndefined(strObjectName)){
        ChildObjectArray = GetChildObjectsByXPath(g_dicColNames_obj.item(strObjectName));
        for(var i=0;i<ChildObjectArray.length;i++){
            CurrentChildObject = ChildObjectArray[i];
            ScrollIntoView(CurrentChildObject);
            ChildObjectArray[i] = GetText(ChildObjectArray[i]);
        }
    }
    
     if (IsSorted(ChildObjectArray,strInputValue,ElementType)){
        ActualResult = strObjectName + " column is sorted in " + strInputValue.toLowerCase() + " order."
        StepStatus = "Pass";
     }else{
        ActualResult = strObjectName + " column is not sorted in " + strInputValue.toLowerCase() + " order."
        StepStatus = "Fail";
     }
        
   }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   }finally{
      g_stepnum = strStep;  
      fnInsertResult("VerifyColumnSorting",strStep,ExpectedResult,ActualResult,StepStatus, strStepType);
   }
 } 
 
//*************************************************************************************************
//   Function Name        : SaveFile
//   Function Description : To download the file and save it to the path sepecified              
//   Inputs               : Path of the file to be saved
//   Returns              : N/A   
//************************************************************************************************* 
function SaveFile(FilePath){
  var ReturnValue = false;
  var LocalBrowserName = Trim(g_strBrowserName.toUpperCase())
  var LocalBrowser = Trim(g_strBrowser.toUpperCase())
  
  try{    
      //Delete any existing file with same name. 
      if(aqFileSystem.Exists(FilePath)){
          aqFileSystem.DeleteFile(FilePath); //Delete the reports pdf if a file of same name exists
      } 
      var ArrayProp = ("Caption","ObjectType")
      var ArrayVal = ("File name:","Edit")
      var SaveButton = ("Save","Button")
      var BrowserProcessName = g_strBrowser;
      
      Wait(10)//Wait for the PDF to load inside the tab
      if(CompareText(LocalBrowserName,"EDGE")||CompareText(LocalBrowser,"EDGE")){ 
          Sys.Browser("edge").BrowserWindow(0).Keys("^s"); 
          BrowserProcessName = "PickerHost";
      }else if(CompareText(LocalBrowserName,"IEXPLORE")||CompareText(LocalBrowser,"IEXPLORE")){   
          var AdobeProcess = Sys.WaitProcess("AcroRd32",5000, 2);
				  if (IsExists(AdobeProcess.WaitDialog("Make Adobe Reader my default PDF application.", 5000))){
            //Select the Checkbox so that the popup is not displayed again.
            AdobeProcess.Dialog("Make Adobe Reader my default PDF application.").FindChild("ObjectIdentifier","Do not show this message again",10).Click();
            //Click cancel so the OS does not attempt to set Adobe as the default app. This causes problem in Windows 10 machines.
            AdobeProcess.Dialog("Make Adobe Reader my default PDF application.").FindChild("ObjectIdentifier","Cancel",10).Click();
          }
          Sys.Browser("iexplore").BrowserWindow(0).FindChild("WndCaption", "AVScrollView", 20).Click();
          Sys.Browser("iexplore").BrowserWindow(0).FindChild("WndCaption", "AVScrollView", 20).Keys("^!s");  
      }      
      
      var DialogObject = Sys.Process(BrowserProcessName).WaitDialog("*",5000);
      if (!DialogObject.Exists && Sys.WaitProcess("AcroRd32").Exists){
          DialogObject = Sys.Process("AcroRd32").WaitDialog("*",5000);
          if (!DialogObject.Exists){ 
            Log.Error("Save As Dialog is not present");
          } 
      } 
      
      DialogObject.FindChild(ArrayProp,ArrayVal,20).Keys(FilePath);  
      DialogObject.FindChild(ArrayProp, SaveButton, 20).Click(); 
       
      Wait(5); //Wait for the file to be saved.
      if(aqFileSystem.Exists(FilePath)){
          ReturnValue = true;
      }
      
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  return ReturnValue;
}

//*************************************************************************************************
// Function Name        : AcceptTermsAndConditions
// Function Description : To accept terms and conditions                   
// Inputs               : None      
// Returns              : None
//*************************************************************************************************
function AcceptTermsAndConditions(){
  try{
    // Block to handle Terms and Conditions page if exists
    var objAccept = BuildWebObj(g_dicColNames_obj.item("TermsAndConditions_Accept"));  
    objAccept = WaitForObject(objAccept, false);
    if(IsExists(objAccept)){
      objAccept.Click();
      var objSubmit = BuildWebObj(g_dicColNames_obj.item("TermsAndConditions_Submit"));        
      objSubmit = WaitForObject(objSubmit, true);
      objSubmit.Click();
    }  
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest); 
  }  
}
//*************************************************************************************************
// Function Name        : ClickMakePrimaryLink
// Function Description : Click on Make Primary link in DeviceSettings page                  
// Inputs               : Device Program name  
// Returns              : None        
//************************************************************************************************
function ClickMakePrimaryLink(){ 
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    //Read object name from excel 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
     if (strObjectName != null){
      strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strInputValue1 = ReadExcel(intIniRwNo,c_intInputVal1Col);
      //Read strStep from excel
      strStep = ReadExcel(intIniRwNo,c_intStepCol);
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
      //function call
      ListTable(strObjectName,strInputValue,2,strInputValue1,strStep,strStepType);
     }
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}


//*************************************************************************************************
// Function Name        : TreatmentDashboardPatientName
// Function Description : To verify existence of Patient in the treatment dashboard                  
// Inputs               : None
// Returns              : None        
//************************************************************************************************
function TreatmentDashboardPatientName(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";            //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));
    strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
    strInputValue1 = ReadExcel(intIniRwNo,c_intInputVal1Col);  //Reads the input value from c_intInputVal1Col       
    strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol      
    strAction = ReadExcel(intIniRwNo,c_intActionCol);  //Reads the action from c_intActionCol   
    //strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    var splitarray = new Array();  // Variable to store the no of records split string
    var pagearray = new Array(); // variable to store the split value of pagecount if it is decimal
    var Flagrow = 0; // variable to store the flag row number
    var Flagcol = 0; //varible  for flag column number
    var iIndex = 0; // varible to store the page nagtion index
    var pageno = 0; //varible to store the total current page number
    var table = BuildWebObj(g_dicColNames_obj.item("TreatmentDashBoard_Table"));  //Build object from the given object name 
    var tableobject = WaitForObject(table, true);
    var rows = tableobject.rows.length;
    var tablecolumns = tableobject.ColumnCount(0);
    //To get the flag column
    for (var col = 1;col < tablecolumns; col++) {       
      Columnvalue = tableobject.Cell(0, col).ContentText;
      Columnvalue = Columnvalue.replace(/(\r\n|\n|\r)/gm," ");  //Replaces all the line breaks
      if(aqString.Find(Columnvalue,(strInputValue1))!= -1){
        Flagcol = col;
        break;
      } 
    }
    // to get the no of pages based on the no of patients in the records string
    var Pagenation = BuildWebObj(g_dicColNames_obj.item("TreatmentDashBoard_NextPageNavigation")); 
    var TotalPatientCount = WaitForObject(objName, true);
    splitarray = (TotalPatientCount.ContentText).split("of");
    if(splitarray[1]>10){
      pagecount = splitarray[1]/10;
      if(aqString.Find(pagecount,("."))!= -1){
        pagecount = pagecount.toString();
        pagearray = pagecount.split(".")
        if(pagearray[1]>0){
        pagecount = Number(pagearray[0])+1
        }
      }
    }
    else{
      pagecount=1;
    }
    // to set the index if page count exceeds 5
    if(pagecount<5){
      iIndex = pagecount-1;
    }
    else{
      iIndex = 4;
    }
    for (pageno=1; pageno<=pagecount; pageno++){ //click the page no sequentially
      if(pagecount !=1){
        if (pageno > 4){
          iIndex = 1;
        }
        if(pageno == pagecount){
          iIndex = 0;
        }
        pagenation = Pagenation+"."+"Child("+iIndex+")";
        pagenation = WaitForObject(pagenation, true); 
        pagenation.Click();
        Delay(3000);
        iIndex--;             
      }
      
      for (var r = 1;r < rows; r++) {       // search for the patient row in each page
        PatientName = tableobject.Cell(r, 0).ContentText;
        if(aqString.Find(PatientName,trim(strInputValue))!= -1){
          //flagrow = row no contains the flag.
          //Flagrow = r;
          intStepCount = 0;
          break;
        } 
        else
        {
          intStepCount = 1;
        } 
      }
      }
    //This function performs the strActions specified in the excel 
    intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);        
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIniRwNo);
    g_stepnum = strStep;         
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}  


//*************************************************************************************************
// Function Name        : GetTimeStamp
// Function Description : To get the time stamp in the desired format.                 
// Inputs               : TimeStampFormat  
// Returns              : Current Time in the requested format.  
//**************************************************************************************************
function GetTimeStamp(TimeStampFormat){
  var ReturnValue = "";
  try{
      if (IsNullorUndefined(TimeStampFormat) || IsEmpty(TimeStampFormat)){
          ReturnValue = aqConvert.DateTimeToFormatStr(aqDateTime.Now(),"%d%b%Y-%I%M%S%p"); //Default Format
      }else{
          ReturnValue = aqConvert.DateTimeToFormatStr(aqDateTime.Now(),TimeStampFormat); //Desired format
      }
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }  
  return ReturnValue;
} 

//*************************************************************************************************
// Function Name        : WriteTreatmentuploadTimestamp
// Function Description : To write the timestamp of treament file upload in testdata sheet            
// Inputs               : None
// Returns              : None         
//************************************************************************************************ 
function WriteTreatmentuploadTimestamp(intRow){
 try{
    
    //Build object
    objName = BuildWebObj(g_dicColNames_obj.item("Treatment_Timestamp"));  //Build object from the given object name
    var timestamp = WaitForObject(objName, false);   
    if(IsExists(timestamp)){
      timestamp = timestamp.Text;  //Get the timestamp of treatment file upload
    }else{
	  timestamp = "NULL"
	}
      WriteExcel(intRow,5,g_TestDataSheetName,timestamp);  //Write timestamp in testdata sheet
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}
//*************************************************************************************************
//   Function Name        : VerifyDateFormat
//   Function Description : To verify date format displayed in the page 
//   Inputs               : None.
//   Returns              : N/A   
//************************************************************************************************* 
 function VerifyDateFormat() {
    try{
      var intIniRwNo;        //Variable for storing initial row number
      var intEndRwNo;        //Variable for storing end row number
      var strObjectName;     //Variable for storing object name
      var objName;           //Variable for storing object 
      var strInputValue;     //Variable for storing input value 
      var strInputValue1;     //Variable for storing input value1
      var strAction;         //Variable for storing action 
      var strStep = "";           //Variable for storing step number
      var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
      intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
      intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
        strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
        if (strObjectName != null){
          objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
          strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
          strInputValue1 = ReadExcel(intIterCnt, c_intInputVal1Col);
          strInputValue1 = GetFormattedDate(strInputValue, strInputValue1);
          strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
          strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol    
          //This function performs the strActions specified in the excel 
          intStepCounter = DataEntry(strAction,objName,strInputValue1,strObjectName,strStep);        
          VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);     
        }
      }  
      g_stepnum = strStep;
    }
   catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   } 
 }
 
 //*************************************************************************************************
//   Function Name        : RandomNumberGenerator
//   Function Description : To concatenate a random number to the specified text 
//   Inputs               : None.
//   Returns              : N/A   
//*************************************************************************************************
 function RandomNumberGenerator() {
    try{
      var rand;
      var intIniRwNo;        //Variable for storing initial row number
      var intEndRwNo;        //Variable for storing end row number
      var strObjectName;     //Variable for storing object name
      var objName;           //Variable for storing object 
      var strInputValue;     //Variable for storing input value 
      var strInputValue1;     //Variable for storing input value1
      var strAction;         //Variable for storing action 
      var strStep = "";           //Variable for storing step number
      var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
      intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
      intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
        strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
        if (strObjectName != null){
          objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
          strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
          rand =  Math.round(Math.random()*(randomNumberCeiling - randomNumberFloor) + randomNumberFloor);
          strInputValue1 = rand + strInputValue;
          WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheetName,strInputValue1);
          strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
          strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol    
          //This function performs the strActions specified in the excel 
          intStepCounter = DataEntry(strAction,objName,strInputValue1,strObjectName,strStep);
          VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);     
        }
      }  
      g_stepnum = strStep;
    }
   catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   } 
 }  
 
 
 //*************************************************************************************************
// Function Name        : CheckNextInputData
// Function Description : Check if data is available for next iteration               
// Inputs               : None     
// Returns              : None
//**************************************************************************************************

function CheckNextInputData(){
try{
  var intIniRwNo;        //Variable for storing initial row number
  var intEndRwNo;        //Variable for storing end row number
  var strStep;
  intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
 // intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
  strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol
  strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
  if (intRowno != g_intPreRowCnt)
  {
    intRowIteration_TC = 1;
    intRowno++;
  }
  fnInsertResult ("CheckNextInputData",strStep,"The next set of input values shall be verified.","The next set of input values are verified","PASS",strStepType);
}

catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
}
}



//*************************************************************************************************
// Function Name        : ReadUserInputData
// Function Description : Reads the input data and write the data in corresponding testscript               
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function ReadUserInputData(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strStep;
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol
    strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
    g_objUserInputSheet   =  g_objTestScriptBook.Sheets("UserInputData"); //Open TestCases sheet in excel workbook
    g_intPreRowCnt  =  g_objUserInputSheet.UsedRange.Rows.Count; //Variable to store maximum used row count in excel sheet
    g_intPreColCnt  =  g_objUserInputSheet.UsedRange.Columns.Count; //Variable to store maximum used column count in excel sheet
    execution_flag = g_objUserInputSheet.cells(intRowno, c_intPreExecuteStatusCol).value; //Variable to store execute status
    PreTestcaseName =  g_objUserInputSheet.cells(intRowno, c_intPreTestcaseNameCol).value; //Variable to store precondition
    StopOnFailure = false; //To avoid stopping of test so that it will continue to execute next loop
    if ((execution_flag == 'Y')&&(PreTestcaseName == g_TestDataSheetName)){ 
      switch(PreTestcaseName){
       case "ActivateClinic":       
        g_ClinicName = g_objUserInputSheet.cells(intRowno, c_intPreClinicNameCol).value;
        g_CompAdmeid = g_objUserInputSheet.cells(intRowno, c_intPreCompAdmeidCol).value;
        g_CompAdmePwd = g_objUserInputSheet.cells(intRowno, c_intPreCompAdmPwdCol).value;
        g_AdminRoles = g_objUserInputSheet.cells(intRowno, c_intAdminRolesCol).value;
        g_CompanyName = g_objUserInputSheet.cells(intRowno, c_intPreCompanyNameCol).value;
        g_ClinicID = g_objUserInputSheet.cells(intRowno, c_intClinicid).value;
        g_objTestScriptSheet.Cells(16,3).Value = g_ClinicName;
        g_objTestScriptSheet.Cells(4,3).Value = g_CompAdmeid;   
        g_objTestScriptSheet.Cells(5,3).Value = g_CompAdmePwd; 
        g_objTestScriptSheet.Cells(14,3).Value = g_AdminRoles; 
        g_objTestScriptSheet.Cells(8,3).Value = g_CompanyName;  
        g_objTestScriptSheet.Cells(10,3).Value = g_ClinicID;
        g_objTestScriptBook.Save();
        fnInsertResult ("ReadUserInputData",strStep,"The Input values for "+PreTestcaseName+" shall be updated.","The Input values for "+PreTestcaseName+" are updated.","PASS",strStepType);
        break;      
       case "ActivateCompany":     
        g_CompanyName = g_objUserInputSheet.cells(intRowno, c_intPreCompanyNameCol).value;
        g_CompanyID = g_objUserInputSheet.cells(intRowno, c_intCompanyid).value;
		    g_CompanySite = g_objUserInputSheet.cells(intRowno, c_intCompanySite).value;
        g_objTestScriptSheet.Cells(11,3).Value = g_CompanyName;
        g_objTestScriptSheet.Cells(17,5).Value = g_CompanyID;
        g_objTestScriptSheet.Cells(10,2).Value = g_CompanySite;
        g_objTestScriptBook.Save();
        fnInsertResult ("ReadUserInputData",strStep,"The Input values for "+PreTestcaseName+" shall be updated.","The Input values for "+PreTestcaseName+" are updated.","PASS",strStepType);
        break;      
       case "CreateCompanyAdmin":
        g_CompanyName = g_objUserInputSheet.cells(intRowno, c_intPreCompanyNameCol).value;
        g_CompAdmeid = g_objUserInputSheet.cells(intRowno, c_intPreCompAdmeidCol).value;
        g_CompAdmName = g_objUserInputSheet.cells(intRowno, c_intPreCompAdmNameCol).value;
        g_AdminRoles = g_objUserInputSheet.cells(intRowno, c_intAdminRolesCol).value;
        g_objTestScriptSheet.Cells(10,3).Value = g_CompAdmeid;
        g_objTestScriptSheet.Cells(16,3).Value = g_CompanyName;
        g_objTestScriptSheet.Cells(9,3).Value = g_CompAdmName;
        g_objTestScriptSheet.Cells(19,3).Value = g_AdminRoles;
        g_objTestScriptBook.Save();
        fnInsertResult ("ReadUserInputData",strStep,"The Input values for "+PreTestcaseName+" shall be updated.","The Input values for "+PreTestcaseName+" are updated.","PASS",strStepType);
        break;
       case "CreateClinicAdmin":
        g_CompAdmeid = g_objUserInputSheet.cells(intRowno, c_intPreCompAdmeidCol).value;
        g_CompAdmPwd = g_objUserInputSheet.cells(intRowno, c_intPreCompAdmPwdCol).value;
        g_ClinAdmeid = g_objUserInputSheet.cells(intRowno, c_intPreClinAdmeidCol).value;
        g_ClinAdmName = g_objUserInputSheet.cells(intRowno, c_intPreClinAdmNameCol).value;
        g_ClinicName = g_objUserInputSheet.cells(intRowno, c_intPreClinicNameCol).value;
        g_AdminRoles = g_objUserInputSheet.cells(intRowno, c_intAdminRolesCol).value;
        g_objTestScriptSheet.Cells(4,3).Value = g_CompAdmeid;
        g_objTestScriptSheet.Cells(5,3).Value = g_CompAdmPwd;
        g_objTestScriptSheet.Cells(13,3).Value = g_ClinAdmeid;
        g_objTestScriptSheet.Cells(10,3).Value = g_ClinAdmName;
        g_objTestScriptSheet.Cells(20,3).Value = g_ClinicName;
        g_objTestScriptSheet.Cells(51,3).Value = g_AdminRoles;
        g_objTestScriptBook.Save();
        fnInsertResult ("ReadUserInputData",strStep,"The Input values for "+PreTestcaseName+" shall be updated.","The Input values for "+PreTestcaseName+" are updated.","PASS",strStepType);
        break;
       case "CreateClinicUser":
        g_ClinAdmeid = g_objUserInputSheet.cells(intRowno, c_intPreClinAdmeidCol).value;
        g_ClinUsereid = g_objUserInputSheet.cells(intRowno, c_intPreClinUsereidCol).value;
        g_ClinAdmName = g_objUserInputSheet.cells(intRowno, c_intPreClinAdmNameCol).value;      
        g_AdminRoles = g_objUserInputSheet.cells(intRowno, c_intAdminRolesCol).value;
        g_objTestScriptSheet.Cells(4,3).Value = g_ClinAdmeid;
        g_objTestScriptSheet.Cells(12,3).Value = g_ClinUsereid;
        g_objTestScriptSheet.Cells(10,3).Value = g_ClinAdmName;      
        g_objTestScriptSheet.Cells(18,3).Value = g_AdminRoles;
        g_objTestScriptBook.Save();
        fnInsertResult ("ReadUserInputData",strStep,"The Input values for "+PreTestcaseName+" shall be updated.","The Input values for "+PreTestcaseName+" are updated.","PASS",strStepType);
        break;  
        case "CreatePatient":     
        g_ClinAdmeid = g_objUserInputSheet.cells(intRowno, c_intPreClinAdmeidCol).value;
        g_PatientFN = g_objUserInputSheet.cells(intRowno, c_intPatientFN).value;
        g_PatientLN = g_objUserInputSheet.cells(intRowno, c_intPatientLN).value;
        g_objTestScriptSheet.Cells(4,3).Value = g_ClinAdmeid;
        g_objTestScriptSheet.Cells(9,3).Value = g_PatientFN;
        g_objTestScriptSheet.Cells(10,3).Value = g_PatientLN;
        g_objTestScriptBook.Save();
        fnInsertResult ("ReadUserInputData",strStep,"The Input values for "+PreTestcaseName+" shall be updated.","The Input values for "+PreTestcaseName+" are updated.","PASS",strStepType);
        break;  
        case "DeleteUser":     
        g_ClinAdmeid = g_objUserInputSheet.cells(intRowno, c_intPreClinAdmeidCol).value;
        g_LRAeid = g_objUserInputSheet.cells(intRowno, c_intLRAeid).value;
        g_LRApwd = g_objUserInputSheet.cells(intRowno, c_intLRApwd).value;
        g_objTestScriptSheet.Cells(8,3).Value = g_ClinAdmeid;
        g_objTestScriptSheet.Cells(4,3).Value = g_LRAeid;
        g_objTestScriptSheet.Cells(5,3).Value = g_LRApwd;
        g_objTestScriptBook.Save();
        fnInsertResult ("ReadUserInputData",strStep,"The Input values for "+PreTestcaseName+" shall be updated.","The Input values for "+PreTestcaseName+" are updated.","PASS",strStepType);
        break; 
        case "DeleteClinic":     
        g_ClinicName = g_objUserInputSheet.cells(intRowno, c_intPreClinicNameCol).value;
        g_objTestScriptSheet.Cells(2,4).Value = g_ClinicName;
        g_objTestScriptBook.Save();
        fnInsertResult ("ReadUserInputData",strStep,"The Input values for "+PreTestcaseName+" shall be updated.","The Input values for "+PreTestcaseName+" are updated.","PASS",strStepType);
        break;         
                   
        }  
       }
       else if (intRowno == g_intPreRowCnt){
        intRowno = 2;
        intRowIteration_TC = intMaxRowCnt_TC+ 1000;
        intIniRwNo = 1000;
       }
       else{
         intRowIteration_TC = 1;
         intRowno++;
       }       
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
}


//*************************************************************************************************
// Function Name        : RolesAssignation
// Function Description : Assign roles to Clinic/User          
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function RolesAssignation(){
  try{
      var intIniRwNo;        //Variable for storing initial row number
      var intEndRwNo;        //Variable for storing end row number
      var strObjectName;     //Variable for storing object name
      var objName;           //Variable for storing object 
      var strInputValue;     //Variable for storing input value 
      var strStep;
      intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
      intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
      strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name     
      strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
      strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value rom c_intInputValCol
      var InputValues = strInputValue.split(";")     
      var RoleID = new Array();
      var FailFlag = true;
      RoleID["BasicClinicalAccess"] = "role_basic_clinical_access";
      RoleID["DeviceManager"] = "role_amia_device_management";
      RoleID["ClariaDeviceManager"] = "role_homechoice_claria_device_management";
      RoleID["ClinicalSettingsManager"] = "role_amia_device_template_management";
      RoleID["ClariaClinicalSettingsManager"] = "role_claria_device_template_management";
      RoleID["ClinicUserManager"] = "role_user_management";
      RoleID["PatientManager"] = "role_offline_patient_management";
      RoleID["Clinic_BasicClinicalAccess"] = "role_BasicClinicalAccess";
      RoleID["Clinic_DeviceManager"] = "role_AmiaDeviceManagement";
      RoleID["Clinic_ClariaDeviceManager"] = "role_HomechoiceClariaDeviceManagement";
      RoleID["Clinic_ClinicalSettingsManager"] = "role_AmiaDeviceTemplateManagement";
      RoleID["Clinic_ClariaClinicalSettingsManager"] = "role_ClariaDeviceTemplateManagement";
      RoleID["Clinic_ClinicUserManager"] = "role_UserManagement";
      RoleID["Clinic_PatientManager"] = "role_OfflinePatientManagement";
      RoleID["CompanyAdministrator"] = "companyRolesSelected1";
      RoleID["PatientAdministrator"] = "companyRolesSelected2";
      RoleID["Clinic_BasicCustomerServiceAccess"] = "role_basic_customer_service_access";
      RoleID["Clinic_PatientManagement"] = "role_patient_management";
      RoleID["Clinic_PatientTemplateManagement"] = "role_patient_template_management";
      RoleID["Clinic_eSignature"] = "role_esignature";
      RoleID["Clinic_secondaryeSignature"] = "role_secondary_esignature";
      RoleID["Clinic_InvoiceManagement"]= "role_invoice_management";
      RoleID["Clinic_PatientPortalUserManagement"]= "role_patient_portal_user_management";

      objName = eval(objName); 
      ChildObj = objName.FindAllChildren("objectType","Checkbox",100);
      ChildObj = (new VBArray(ChildObj)).toArray();
        for(var intCounter = 0; intCounter <= (ChildObj.length)- 1; intCounter++){
          ObjectID = Trim(ChildObj[intCounter].ObjectIdentifier); 
          var FindFlag = false;
          for (i=0;i <= (InputValues.length)-1;i++){          
            if(ObjectID == RoleID[InputValues[i]]) {
               FindFlag = true;
               if (!ChildObj[intCounter].checked){              
                ChildObj[intCounter].Click(); 
                Wait(2);         
                if (!ChildObj[intCounter].checked){   
                  FailFlag = false;
                } 
                break;
                }                    
              }
            }
          if ((FindFlag == false)&& (ChildObj[intCounter].checked)){        
            ChildObj[intCounter].Click(); 
            Wait(2);
             if (ChildObj[intCounter].checked){   
              FailFlag = false;
             }     
          }
       }        
       if (FailFlag == true){
         fnInsertResult ("RolesAssignation",strStep,"Roles for Clinic Admin/User shall be assigned","Roles for Clinic Admin/User are assigned","PASS",strStepType);
       }else{
         fnInsertResult ("RolesAssignation",strStep,"Roles for Clinic Admin/User shall be assigned","Roles for Clinic Admin/User are not assigned","FAIL",strStepType);
         g_intFailCnt = g_intFailCnt + 1;
       }          
      g_stepnum = strStep;
   }
   catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }    
}

//*************************************************************************************************
// Function Name        : AcceptInvalidSSL
// Function Description : To accept invalid security certificate
// Inputs               : None
// Returns              : None
//************************************************************************************************
function AcceptInvalidSSL(){
  try{
    oPageObject = WaitForObject(g_PageObject, false);//Added since the oPageObject get disconnected from its client.
    switch (trim(g_strBrowser.toUpperCase())){
      //If the invalid SSL message is displayed, then click override link
      case "IEXPLORE":
        if(oPageObject.FindChild("idStr","infoBlockIDImage",5000).Exists){
          oPageObject.FindChild("idStr","infoBlockIDImage",2000).Click(); //In latest IE11 Version, Override link is displayed only after clicking more info.       
          oPageObject.FindChild("idStr","overridelink",2000).Click();
          Wait(2);
          WaitForPageSync();
        }
        break;
      case "EDGE":
        if(oPageObject.FindChild("idStr","invalidcert_continue",5000).Exists){
          oPageObject.FindChild("idStr","invalidcert_continue",2000).Click();
          Wait(2);
          WaitForPageSync();
        } 
        break;
      case "FIREFOX":
        var advancedBtn = oPageObject.FindChild("idStr","advancedButton",5);
        if(IsExists(advancedBtn)) {
          oPageObject.FindChild("idStr","advancedButton",5).Click();
          Wait(1);
          oPageObject.FindChild("idStr","exceptionDialogButton",5).Click();
          Wait(1);  
          var dialogPage = Sys.Browser(g_strBrowser).UIPage("chrome://pippki/content/exceptionDialog.xul");
          arrObject = dialogPage.Find("name", "button('Confirm Security Exception')", 100, true);
          arrObject.Click();
        }
        break;        
      }
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  } 
}


//*************************************************************************************************
// Function Name        : UpdateSummaryStatus
// Function Description : To increment Pass/Fail count for keywords with step number having dot (.)
// Inputs               : None
// Returns              : None
//************************************************************************************************
function UpdateSummaryStatus(TotalPassFailCnt){
  try{
    if(TotalPassFailCnt == 0){      
      g_iPass_Count = g_iPass_Count + 1;    
    }else{
      g_iFail_Count = g_iFail_Count + 1;  
    } 
  
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);  
  } 
}


//*************************************************************************************************
// Function Name        : LaunchGmail
// Function Description : To launch the Gmail in a different browser
// Inputs               : None
// Returns              : None
//*************************************************************************************************
function LaunchGmail() {
	try {
		var intIniRwNo; //Variable for storing initial row number
		var strObjectName; //Variable for storing object name
		var objName; //Variable for storing object
		var strStep; //Variable for storing step number
    var strAction;
    var strInputValue;
    strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol
    strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
    strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol 
		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intIterCnt = intIniRwNo;
		intStepCounter = 1; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    ExpectedMessage = "Gmail shall be launched";
    ActualMessage = "Gmail is not launched";  
    Browsers.Item(g_strBrowser).Navigate("https://mail.google.com/");  //Launch the url
    Sys.Browser().BrowserWindow(0).Maximize(); //Maximize the browser
    
		//If it is on the about gmail page, then click on Sign In and Navigate to the Login page 
		objName = BuildWebObj(g_dicColNames_obj.item("COM_LGM_LoginGmail_SignIn"));
    objName = WaitForObject(objName, false);
		if (IsExists(objName)) {
			objName.Click();
      Wait(1);
      WaitForPageSync();
		}    
    
    
    //If the gmail account is already logged in, then logout 
		objName = BuildWebObj(g_dicColNames_obj.item("COM_LGM_LogoutGmail_AccountOptions"));
    objName = WaitForObject(objName, false);
		if (IsExists(objName)) {
			objName.Click(); //Click on the MyAccount, to display the Sign out link 
      Wait(1);
      objName = BuildWebObj(g_dicColNames_obj.item("COM_LGM_LogoutGmail_SignOut"));
      objName = WaitForObject(objName, true);
      if (IsExists(objName)) {
          objName.Click();
          Wait(1);
          WaitForPageSync();
      }
		}


    //If it is directly asking password of the account used last time, then choose different account   
		objName = BuildWebObj(g_dicColNames_obj.item("COM_LGM_LoginGmail_ChooseAccountBtn"));
    objName = WaitForObject(objName, false);
		if (IsExists(objName)) {
			objName.Click();//Click the down arrow to navigate to choose account page 
      Wait(1);
      WaitForPageSync();
  		objName = BuildWebObj(g_dicColNames_obj.item("COM_LGM_LoginGmail_UseAnotherAccountBtn"));
      objName = WaitForObject(objName, true);
  		if (IsExists(objName)) {
  			objName.Click(); //Click choose different account
        Wait(2);
  		}      
		}
    
    //Check if the Username field is displayed.
		objName = BuildWebObj(g_dicColNames_obj.item("COM_LGM_LoginGmail_UserNameTxt"));
    objName = WaitForObject(objName, true);
		if (IsExists(objName) && IsVisible(objName)) {
			    ActualMessage = "Gmail is not launched";
          intStepCounter = 0;
		}
    
	}catch (e) {
		Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}finally{
    VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
		g_ExecutedStepNumber = strStep;
  }
}

//*************************************************************************************************
// Function Name        : LoginToGmail
// Function Description : To login to Gmail
// Inputs               : Username,Password
// Returns              : None
//*************************************************************************************************
function LoginToGmail() {
	try {
		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing end row number
		var strObjectName; //Variable for storing object name
		var objName; //Variable for storing object
		var strInputValue; //Variable for storing input value
		var strAction; //Variable for storing action
		var strStepNumber; //Variable for storing step number
		intStepCounter = 0; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword

		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
			if (strObjectName != null) {
				objName = BuildWebObj(g_dicColNames_obj.item(strObjectName)); //Build object from the given object name
				strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
				strStepNumber = ReadExcel(intIterCnt, c_intStepCol); //Reads the step from c_intStepNumberCol
				strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
				intStepCounter = DataEntry(strAction, objName, strInputValue, strObjectName, strStepNumber);
				VerificationPoint(g_strFuncCall, strStepNumber, intStepCounter, intIterCnt);
			}
		}
		
	}catch (e) {
		  Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}finally{
      g_ExecutedStepNumber = strStepNumber;
  }
}

//*************************************************************************************************
// Function Name        : VerifyGmail
// Function Description : To verify the email
// Inputs               : Search String
// Returns              : None
//*************************************************************************************************
function VerifyGmail() {
	try {
		var intIniRwNo; //Variable for storing initial row number
		var intEndRwNo; //Variable for storing end row number
		var strObjectName; //Variable for storing object name
		var objName; //Variable for storing object
		var strInputValue; //Variable for storing input value
		var strAction; //Variable for storing action
		var strStepNumber; //Variable for storing step number
		var intStepCounter = 0; //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
		intIniRwNo = g_intStrtRow; //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
		intEndRwNo = g_intEndRow; //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
		var CurrentDate = aqDateTime.Today();
		var AfterValue = "after:" + aqConvert.DateTimeToFormatStr(aqDateTime.AddDays(CurrentDate,-1),"%Y/%m/%d");
		var BeforeValue = "before:" + aqConvert.DateTimeToFormatStr(CurrentDate,"%Y/%m/%d");
    
		for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++) {
			strObjectName = ReadExcel(intIterCnt, c_intObjNameCol); //Reads the object name from c_intObjNameCol
			if (strObjectName != null) {
				objName = BuildWebObj(g_dicColNames_obj.item(strObjectName)); //Build object from the given object name
				strInputValue = ReadExcel(intIterCnt, c_intInputValCol); //Reads the input value from c_intInputValCol
				strStepNumber = ReadExcel(intIterCnt, c_intStepCol); //Reads the step from c_intStepNumberCol
				strAction = ReadExcel(intIterCnt, c_intActionCol); //Reads the action from c_intActionCol
        if (ContainsText(strObjectName,"COM_GM_Gmail_SearchTxt")){
          if (ContainsText(strInputValue.toUpperCase(),"AFTER")){ //Remove the existing after criteria
              IndexOfAfter = aqString.Find(strInputValue,"After",0,false); //Find with Case In-sensitive
              strInputValue = strInputValue.substring(0,IndexOfAfter-1);
          } 
          strInputValue = strInputValue + " " + AfterValue + " " + BeforeValue;
        } 
				intStepCounter = DataEntry(strAction, objName, strInputValue, strObjectName, strStepNumber);
				VerificationPoint(g_strFuncCall, strStepNumber, intStepCounter, intIterCnt);
			}
		}
	}catch (e) {
		  Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
	}finally{
      g_ExecutedStepNumber = strStepNumber;
  }
}
  
//*************************************************************************************************
// Function Name        : GetAuthKeyFromURL
// Function Description : To get the url of the current active page.
// Inputs               : None
// Returns              : None
//************************************************************************************************
function GetAuthKeyFromURL(){
    var StepStatus = "Fail";  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
    var ReturnValue = "NULL";
    var BaseURL = "NULL";
    var SplitString = "";
    var count = 0;
    WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheetName,ReturnValue);  //Initial write null to remove previous value
    ExpectedResult = "PAuth key shall be extracted from URL";
    ActualResult = "PAuth key is not extracted from URL";
    try{
      strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
      var CurrentURL = Sys.Browser(g_strBrowser).page("*").URL;
      var KeyToSearch = "p_auth=";
      //Get P_auth and the base URL from the current URL
      if (ContainsText(CurrentURL,KeyToSearch)){
        ReturnValue =  CurrentURL.split(KeyToSearch);
        //Get the Base URL
        BaseURL = ReturnValue[0];
        LastSlashIndex = aqString.FindLast(BaseURL,"/");
        BaseURL = BaseURL.substring(0,LastSlashIndex);
        //Get the PAuth Key
        ReturnValue = ReturnValue[1].substring(0,8);
      }
      
      if (!(CompareText(ReturnValue,"NULL"))){
       ActualResult = "PAuth key is extracted from URL";
       StepStatus = "Pass";
      } 
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);  
    }finally{
      WriteExcel(intIniRwNo,c_intInputValCol,g_TestDataSheetName,BaseURL);  //write the Base URL details in test data sheet
      WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheetName,ReturnValue);  //write the p_auth details in test data sheet
      fnInsertResult("GetAuthKeyFromURL",strStep,ExpectedResult,ActualResult,StepStatus, strStepType);
   } 
  }
  
//*************************************************************************************************
// Function Name        : CreateDeviceTemplate
// Function Description : To Create Device Templates (Device Program, Patient Settings and System Settings)                 
// Inputs               : None
// Returns              : None 
//*************************************************************************************************
function CreateDeviceTemplate(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";       //Variable for storing step number
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var TotalStepCounter = 0;  //Variable for storing result for total number of steps in the keyword
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
      
    for (intIterCnt = intIniRwNo; intIterCnt <= intEndRwNo; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol     
      strInputValue = ReadExcel(intIterCnt, c_intInputValCol);  //Reads the input value from c_intInputValCol      
      strStep = ReadExcel(intIterCnt, c_intStepCol);  //Reads the step from c_intStepCol
      strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol
      VerificationObj = BuildWebObj(g_dicColNames_obj.item(strObjectName));
      if (ContainsText(strObjectName,"CreateNew") && CompareText(strAction,"VerifyExist")){
        var ObjectPreset = WaitForObject(VerificationObj,false);
        if(!IsExists(ObjectPreset)){
            intStepCounter = 0;
            ActualMessage = "Create New link shall not exist if Device Template is present.";
            ExpectedMessage = "Create New link does not exists.";
            TotalStepCounter = TotalStepCounter + intStepCounter; //Add the intStepCounter for all steps  
            VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);  
            break;  
        }else{
            //This function performs the strActions specified in the excel
            intStepCounter = DataEntry(strAction,VerificationObj,strInputValue,strObjectName,strStep); 
            TotalStepCounter = TotalStepCounter + intStepCounter; //Add the intStepCounter for all steps  
            VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);           
        } 
      }else{  
        //This function performs the strActions specified in the excel
        intStepCounter = DataEntry(strAction,VerificationObj,strInputValue,strObjectName,strStep); 
        TotalStepCounter = TotalStepCounter + intStepCounter; //Add the intStepCounter for all steps  
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);      
      }
    }      
  }catch(e){     
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{
    UpdateSummaryStatus(TotalStepCounter) ;
    g_stepnum = strStep;
  } 
}  

//*************************************************************************************************
// Function Name        : OpenPassswordResetURL
// Function Description : To navigate the browser to Password Reset URL                 
// Inputs               : None
// Returns              : None 
//*************************************************************************************************
function OpenPassswordResetURL(URLToLaunch){
  var ResetPasswordObjectProperty = BuildWebObj(g_dicColNames_obj.item("ResetPassword_NewPassword"));  //Build object from the given object name
  ResetPasswordObjectProperty = ResetPasswordObjectProperty.split("*");
  ResetPasswordObjectProperty = ResetPasswordObjectProperty[0] + g_dicColNames_obj.item("ResetPassword_NewPassword");               
  var ResetPasswordObject = null ;
  try{
      if (!IsNullorUndefined(URLToLaunch)){
          for (var RetryCount = 0; RetryCount<5; RetryCount++){
              Browsers.Item(g_strBrowser).Navigate(URLToLaunch);  //Launch the url
              AcceptInvalidSSL();
              Wait(1);
              WaitForPageSync();
              Log.Message("URL after navigation " + RetryCount, oPageObject.URL); 
          
              //Check if the reset password page is displabyed.
              ResetPasswordObject = WaitForObject(ResetPasswordObjectProperty, false);
              if (IsExists(ResetPasswordObject)){
                  break;
              } 
          }
      }else{
        Log.Error("URL to Navigate is NULL or Undefined")
      } 
  }catch(e){     
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }  
} 