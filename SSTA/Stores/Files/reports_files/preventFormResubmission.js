/**
 * BAXTER CONFIDENTIAL - Highly Restricted: Do not distribute without prior approval
 * 
 * Project: New Horizons
 * 
 * Copyright © 2012, 2013, 2014 Baxter Healthcare Corporation. All rights reserved.
 */
var global_submit = {};

function preventFormResubmission() {
	var hasReauth = ($('#reauthenticationPasswordInput').length > 0);

	$('form').each(function(){
		global_submit[$(this).attr('id')] = false;
		
		$(this).submit(function(event){
			if (hasReauth) {
				return;
			}
			if (!global_submit[$(this).attr('id')]) {
				global_submit[$(this).attr('id')] = true;
				return;
			}
			event.preventDefault();
		});
	});
}

$(document).ready(function() {
	preventFormResubmission();
});