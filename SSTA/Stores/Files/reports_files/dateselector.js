/**
 * BAXTER CONFIDENTIAL - Highly Restricted: Do not distribute without prior approval
 * 
 * Project: New Horizons
 * 
 * Copyright © 2012, 2013, 2014 Baxter Healthcare Corporation. All rights reserved.
 * 
 */

/**
 * Redefine the locale and format for the date selector.
 * 
 * @param datePickId the input field element id.
 * @param imgConfig the icon showed for the input field.
 * @returns {DateSelector} redefined date selector.
 */
function DateSelector(datePickId, imgConfig)
{
	this.datePickId = datePickId;
	this.config = imgConfig;
	
	$.datepicker.setDefaults($.datepicker.regional['']);
	if (this.config != undefined)
	{
		this.config['showOn'] = 'both';
		this.config['buttonImageOnly'] = true;
		$('#' + this.datePickId).datepicker(this.config);
	}
	$('#' + this.datePickId).datepicker();
}

/**
 * 
 * @param aLocale locale string like 'zh_CN', fr_FR
 */
DateSelector.prototype.setLocale = function(aLocale)
{
	// jQuery need locale format in zh-CN
	var inputLocale = new String(aLocale.replace(/_/, '-'));
	
	var supportedLocale = this.findSupportedLocale(inputLocale);
	
	if (supportedLocale == undefined)
	{
		var indHyphen = inputLocale.indexOf('-', 0);
		if (indHyphen != -1)
		{
			inputLocale = inputLocale.substring(0, indHyphen);
			supportedLocale = this.findSupportedLocale(inputLocale);
		}
	}
	
	var locale =  (supportedLocale == undefined ? '' : supportedLocale);
	$('#' + this.datePickId).datepicker("option", $.datepicker.regional[locale]);
};

DateSelector.prototype.setFormat = function(aFormat)
{
	$('#' + this.datePickId).datepicker("option", "dateFormat", aFormat);
};

DateSelector.prototype.openHiddenField = function(hiddenFieldId, format) 
{
	  $('#' + this.datePickId).datepicker("option", "altField", '#' + hiddenFieldId);
      $('#' + this.datePickId).datepicker("option", "altFormat", format);
};

DateSelector.prototype.findSupportedLocale = function(aLocale)
{
	var supportedLocale = undefined;
	
	// loop the full name first
	for (var o in $.datepicker.regional)
	{
		if (o == aLocale)
		{
			supportedLocale = aLocale;
			break;
		}
	}
	
	return supportedLocale;
};