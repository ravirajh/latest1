/**
 * BAXTER CONFIDENTIAL - Highly Restricted: Do not distribute without prior approval
 * 
 * Project: New Horizons
 * 
 * Copyright © 2012, 2013, 2014, 2016 Baxter Healthcare Corporation. All rights reserved.
 * 
 */
/**
 *
 * Validate all fields before submit the form
 * 
 */

/*******************************************************************************
 * Sample Validation object definition:

            "validation": {
              "isRequired": true,
              "maxLength": 20,
              "validationRules": [
                {
                  "ruleOperator": "LessEqual",
                  "targetAttributeName": "endDate"
                }
              ]
            }

*******************************************************************************/

var NHFormValidator = {
	ERRORTYPES : {
		isRequired: 'isRequired',
		LessEqual: 'LessEqual',
		FutureDateCheck: 'FutureDateCheck',
		DateRangeCheck: 'DateRangeCheck',
		others: 'others',
	},
	ERRORTYPEPRIORITY: ['isRequired', 'LessEqual', 'FutureDateCheck', 'DateRangeCheck', 'others'],
	_resultInfoContainer: {},

	/**
	 * Validate the form with a list of validation definitions.
	 *
	 * @param form
	 * @param validationList
	 */
	validate: function(inputMap, validationList) {
		// Reset to empty container
		this._resultInfoContainer = {};

		// Is used to keep validation resultInfo in validation order.
		this._resultInfoContainer.resultInfoList = [];

		$(validationList).each(function(index, validationObject) {
			// Validate all fields and mark error if found
			NHFieldValidator.validate(inputMap, validationObject);
		});

		return this._resultInfoContainer.resultInfoList.length == 0;
	},

	addResultInfo: function(errorMessage, inputId, errorType, targetAttributeName) {
		var resultInfo = this._createResultInfo(errorMessage, inputId, errorType, targetAttributeName);

		this._resultInfoContainer.resultInfoList.push(resultInfo);

		if (!this._resultInfoContainer[resultInfo.errorType]) {
			this._resultInfoContainer[resultInfo.errorType] = {};
		}

		this._resultInfoContainer[resultInfo.errorType][resultInfo.inputId] = resultInfo;
	},

	/**
	 * Retrieve the high priority error type object which contains related resultInfo objects from this._resultInfoContainer.
	 * @returns Object as below structure:
	 * 
	 * {			
			patientId : { errorMessage="You hav.....", inputId="patientId", errorType="isRequired"},
			dateInputDisplay_reportEndDate : { errorMessage="You hav.....", inputId="dateInputDisplay_reportEndDate", errorType="isRequired"}
		}
	 */
	getHighPriorityErrorTypeResultInfos: function() {
		for (var i = 0; i < NHFormValidator.ERRORTYPEPRIORITY.length; i++) {
			var errorType = NHFormValidator.ERRORTYPEPRIORITY[i];
			if (this._resultInfoContainer[errorType]) {
				return this._resultInfoContainer[errorType];
			}
		}

		return null;
	},

	getDisplayErrorMessage: function() {
		var errorTypeResultInfos = this.getHighPriorityErrorTypeResultInfos();
		if (errorTypeResultInfos) {
			for (var inputId in errorTypeResultInfos) {
				if (errorTypeResultInfos[inputId]) {
					return errorTypeResultInfos[inputId].errorMessage;
				}
			}
		}
		return "";
	},

	getResultInfoList: function() {
		return this._resultInfoContainer.resultInfoList;
	},

	_createResultInfo: function(errorMessage, inputId, errorType, targetAttributeName) {
		var resultInfo = {};
		resultInfo.errorMessage = errorMessage;
		resultInfo.inputId = inputId;
		if (!errorType) {
			errorType = NHFormValidator.ERRORTYPES.OTHERS;
		}
		resultInfo.errorType = errorType;
		resultInfo.targetAttributeName = targetAttributeName;
		return resultInfo;
	}
};

var NHFieldValidator = {

	/**
	 * Validate the form with specified validation definition.
	 *
	 * @param form
	 * @param validationList
	 */
	validate: function(inputMap, validationObject) {
		var isRequired = validationObject.isRequired;
		var validationRules = validationObject.validationRules;

		var curReportFieldValue = inputMap[validationObject.curReportFieldId];

		var valid = true;

		if (isRequired) {
			valid = NHValidator.NotEmpty.validate(curReportFieldValue);
		}

		if (!valid) {
			// Set error style
			//var displayInputName = validationObject.targetInputName;
//			$('input[name="' + displayInputName + '"]').css("background-color","#FEB1B1");
			// Extract the error display logic to jsp.
			/**
			$('input[name$="' + validationObject.curReportFieldId + '"]').css("background-color","#FEB1B1");
			$("#emptyMessage").attr("class", "error-message");
			$("#emptyMessage").show();
			**/
			NHFormValidator.addResultInfo(validationObject.errorMessageForIsRequired, validationObject.targetInputName,
					NHFormValidator.ERRORTYPES.isRequired, null);

			return;
		}

		// do the validate for all rules
		if(validationRules) {
			NHValidator.ValidationRule.validate(validationRules, inputMap, curReportFieldValue, validationObject.targetInputName);	
		}
	}
};

var NHValidator = {};

NHValidator.NotEmpty = {
	validate: function(value) {
		if (value && value.length > 0) {
			return true;
		}
		return false;
	}
};

/**
 * validator for a validation rule.
 * TODO Currently we just support less equal for date.Need support more
 * 
 * @param validationRules
 * @param inputMap
 *            map between input name and its vales
 * @param curValue
 *            current field value
 */
NHValidator.ValidationRule= {
	validate: function(validationRules, inputMap, curValue, targetInputName) {

		var ruleValidator = RuleValidator.createNew();

		$(validationRules).each(function(index,rule) {
			ruleValidator.validateWithAllValidtor(rule, inputMap, curValue);
		});

		//You may don't want to use $(validationRules).each(function) if you want to break the loop and also return this function at the first time you got one rule.valid = false;
		for (var i=0; i <validationRules.length ; i++) {
			var rule = validationRules[i];
			if (rule.valid !== undefined && rule.valid == false) {
				rule.valid = true;//reset valid value so that you can get correct result when you do the next validation
				//$("#invalidMessage").html(rule.errorMessage);// set the corresponding error message 

				NHFormValidator.addResultInfo(rule.errorMessage, targetInputName, NHFormValidator.ERRORTYPES[rule.ruleOperator], rule.targetAttributeName);

			}
		}

	}
};

/**
 * Util class. Register all available rule validator which will be call by RuleValidator class
 */
var RuleValidatorRegister = {
		getRegistedRuleValidtor : function() {
			var registedRuleValidtors = [];

			//Register less equal validtor
			var lessEqualRuleValidtor = LessEqualRuleValidtor.createNew();
			registedRuleValidtors.push(lessEqualRuleValidtor);

			//register date dateRangeCheck validtor
			var dateRangeCheckRuleValidtor = DateRangeCheckRuleValidtor.createNew();
			registedRuleValidtors.push(dateRangeCheckRuleValidtor);

			//register FutureDateCheckValidtor validtor
			var futureDateCheckValidtor = FutureDateCheckValidtor.createNew();
			registedRuleValidtors.push(futureDateCheckValidtor);

			return registedRuleValidtors;
		}
};

/**
 * Base class for each validator. Responsible for invoking all registered validator.
 */
var RuleValidator = {
	dateFormat : 'yy-mm-dd',
	createNew : function() {
		var ruleValidator = {};

		ruleValidator.validateWithAllValidtor = function(rule, inputMap, curValue) {
			var registedRuleValidtors = RuleValidatorRegister.getRegistedRuleValidtor();

			$(registedRuleValidtors).each(function(index, validtor) {
				validtor.validate(rule, inputMap, curValue);
			});
		};

		return ruleValidator;
	}	
};

/**
 * Separate validtor for each special checking.
 * Should implement validate function otherwise this validtor can not be invoked by the base validator.
 * 
 * This validtor is for "LessEqual"
 */
var LessEqualRuleValidtor = {
		createNew : function() {

			var lessEqualRuleValidtor = RuleValidator.createNew();

			//Should implement validate function otherwise this validtor can not be invoked by the base validator.
			lessEqualRuleValidtor.validate = function(rule,inputMap, curValue) {

				if (rule && rule.ruleOperator == 'LessEqual') {
					if (rule.targetAttributeName && curValue) {
						var targetAttributValue = inputMap[rule.targetAttributeName];
						if (targetAttributValue) {
							//var targetDate =  $("input[name='"+rule.targetAttributeName+"']").datepicker("getDate");
							var curValueDate = $.datepicker.parseDate(RuleValidator.dateFormat,curValue);
							var targetDate = $.datepicker.parseDate(RuleValidator.dateFormat,targetAttributValue);
							if (curValueDate.getTime() > targetDate.getTime()){
								rule.valid = false;
							}
						}
					}
				}
			};

			return lessEqualRuleValidtor;
		}
};

/**
 * Separate validtor for each special checking.
 * Should implement validate function otherwise this validtor can not be invoked by the base validator.
 * 
 * This validtor is for "DateRangeCheck"
 */
var DateRangeCheckRuleValidtor = {
	createNew : function() {
		var dateRangeCheckRuleValidtor = RuleValidator.createNew();

		/**
		 * Should implement validate function otherwise this validtor can not be invoked by the base validator. 
		 */
		dateRangeCheckRuleValidtor.validate = function(rule,inputMap, inputValue) {
			if (rule && rule.ruleOperator == 'DateRangeCheck') {
				if (rule.targetAttributeName && inputValue) {
					var targetAttributValue = inputMap[rule.targetAttributeName];

					var targetDate;

					var now = new Date();
					var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

					if ("NOW" == rule.targetAttributeName) {
						targetDate = today;
					} else if (targetAttributValue) {
						targetDate = $.datepicker.parseDate(RuleValidator.dateFormat, targetAttributValue);
					}

					if (targetDate) {
						var inputDate = $.datepicker.parseDate(RuleValidator.dateFormat, inputValue);

						var addDateRangeCheckDate = dateRangeCheckRuleValidtor._calculateDate(inputDate, rule);
						if (addDateRangeCheckDate.getTime() < targetDate.getTime()) {
							rule.valid = false;
						}

					}

				}
			}
		};

		/**
		 * Add the date range provided by rule.
		 */
		dateRangeCheckRuleValidtor._calculateDate = function(dateVar, rule) {
				if (!dateVar) return dateVar;

				var resultDate = new Date(dateVar);

				var dateRangeCheckYear = (!rule.dateRangeCheckYear) ? 0 : rule.dateRangeCheckYear;
				var dateRangeCheckMonth = (!rule.dateRangeCheckMonth) ? 0 : rule.dateRangeCheckMonth;

				resultDate.setMonth(dateRangeCheckYear * 12 + resultDate.getMonth() + new Number(dateRangeCheckMonth));

				// In normal case result date is equal to source date, but when adding one month to Date Jan 31, 2013, the result will be Mar 3, 2013.
				if(resultDate.getDate() < dateVar.getDate()) {
					// Set it to the last day of previous month, as zero is not permitted normally.
					resultDate.setDate(0);
				}

				var dateRangeCheckDay = (!rule.dateRangeCheckDay) ? 0 : rule.dateRangeCheckDay;
				resultDate.setDate(resultDate.getDate() + new Number(dateRangeCheckDay));

				return resultDate;
		 };

		return dateRangeCheckRuleValidtor;
	}	
};

/**
 * Separate validtor for each special checking.
 * Should implement validate function otherwise this validtor can not be invoked by the base validator.
 * 
 * This validtor is for "FutureDateCheck"
 */
var FutureDateCheckValidtor = {
		createNew : function() {
			var futureDateCheckValidtor = RuleValidator.createNew();

			//Should implement validate function otherwise this validtor can not be invoked by the base validator.
			futureDateCheckValidtor.validate = function(rule, inputMap, curValue) {
				if (rule && rule.ruleOperator == 'FutureDateCheck') {
					if (curValue) {
						var now = new Date();
						var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

						var curValueDate = $.datepicker.parseDate(RuleValidator.dateFormat,curValue);
						if (curValueDate && (curValueDate.getTime() > today.getTime())) {
							rule.valid = false;
						}
					}
				}
			};
			return futureDateCheckValidtor
		}
};
