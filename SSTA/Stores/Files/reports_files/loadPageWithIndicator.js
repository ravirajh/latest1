/**
 * BAXTER CONFIDENTIAL - Highly Restricted: Do not distribute without prior approval
 * 
 * Project: New Horizons
 * 
 * Copyright © 2012, 2013, 2014 Baxter Healthcare Corporation. All rights reserved.
 */

/**
 * A utility to show Page Load Indicator.
 * @author huaxin.zhang 
 */


/**
 * waiting indicator and close indicator
 */
jQuery.extend({
	/**
	 * Waiting indicator.
	 * 
	 * @param config It is a json object, inlcude basePath and basezIndex
	 * basePath is a image path, basezIndex is a layer parameter.
	 */
	showWaitingIndicator : function(config) {
		$.blockUI({
					message : '<img src="'+config.basePath+'/common/css/nyroModal/images/loading.gif" />',
					css : {
						border : 'none',
						padding : '5px',
						backgroundColor : '',
						'-webkit-border-radius' : '10px',
						'-moz-border-radius' : '10px',
						opacity : .6,
						color : '#fff'
					},
					baseZ : (config.basezIndex == null ? 1000: config.basezIndex)
				});
	},
	/**
	 * Close indicator
	 * 
	 * @param config It is a json object, include timeout.when close the overlayer
	 */
	closeWaitingIndicator : function(config) {
		setTimeout($.unblockUI, (config.timeout == null ? 400: config.timeout));
	}
});

jQuery(function($,underfined){
	/**
	 * A internal variable to store function and parameter used within this utility.
	 */
	_internal = {
			
			debugFlag : false,
			/**
			 * Customize loading waiting page.
			 */
			reRenderLoading : function(){
				$('.nyroModalLoad').css("border", "0px");
				$('.nyroModalLoad').append('<div style="margin-top:100px;color:#FFFFFF;font-size:12px">Loading Please Wait</div>');
			},
			
			/**
			 * log message.
			 * @param msg Message should be logged.
			 */
			debug: function(msg) {
				if (this.debugFlag && window.console && window.console.log)
				window.console.log(msg);
			},
			
			/**
			 * Enable log function.
			 * @param openThis True to open log function for this utility. 
			 */
			openDebug:function(openThis){
				if(openThis){
					this.debugFlag =  true;
					$.nmInternal({debug: true});
				}
			},
			
			/**
			 * Internal Constants for the loading types.
			 */
			loadPageType : {
				GOTO_NEW_PAGE : "browser will go to a new page",
				POPUP_AJAX : "popup a dialog filled with ajax request",
				POPUP_HIDDEN_DIV : "popup a hidden div"
			},
			
			isRequestFromForm : function(element, requestUrl){
				return $(element).is("form") && $(element).attr('action') && $(element).attr('action') === requestUrl;
			},
			
			callBackForGoto: {
				beforeShowLoad: function(){_internal.reRenderLoading();},
				initFilters:function(nm){nm.filters = ["basic","gotoNewPageFilter"];}
			},
			
			/**
			 * load page with nyroModal
			 * @param element
			 * @param paramSettings
			 * @param aLoadType 
			 */
			loadPageWithIndicator : function(element,paramSettings, aLoadType){
				
				var params4Nm = {};
				
				if(aLoadType === _internal.loadPageType.GOTO_NEW_PAGE ){
					_internal.debug("Go to new page");
					params4Nm = {
							modal:paramSettings.isModal,
							callbacks:_internal.callBackForGoto
					};
				}else if(aLoadType === _internal.loadPageType.POPUP_AJAX ){
					params4Nm = {
							modal:paramSettings.isModal,
							showCloseButton: paramSettings.showCloseButton,
							callbacks:{ beforeShowLoad: _internal.reRenderLoading}
					};
				}
				
				if(_internal.isRequestFromForm(element, paramSettings.href)){// request from form 
					$(element).nyroModal(params4Nm).nmCall();
				}else{
					$.nmManual(paramSettings.href,params4Nm);
				}
			},
			
	};
	
	_internal.openDebug(true);
	
	/**
	 * Customized filter for the case : browser will go to a new page.
	 */
	$.nmFilters({
		gotoNewPageFilter :{
			 init: function(nm) {
					 nm.loadFilter = 'gotoNewPageFilter';
				  	 nm.opener.off('click.nyroModal').on('click.nyroModal', function(e) {
				  		 nm.opener.trigger('nyroModal');
				  	 });
			  }
			}
	 });
	
	/**
	 * Constants for the loading types.
	 */
	$.extend({
		loadPageType : {
			GOTO_NEW_PAGE : _internal.loadPageType.GOTO_NEW_PAGE,
			POPUP_AJAX :  _internal.loadPageType.POPUP_AJAX,
			POPUP_HIDDEN_DIV : _internal.loadPageType.POPUP_HIDDEN_DIV
		}
	});
	
	/**
	 * jQuery call function to close the Popup.
	 */
	$.fn.closePopup =  function(){
		$.nmTop().close();
	};
	
	
//	$.fn.submitFormWithIndicator = function(){
//		
//	}
//	
//	
//	$.fn.popupWithIndicator = function(){
//		
//	}
	
	/**
	 * Main function to show loading indicator.
	 * @param params Parameters can be included e.g.
	 * {
	 * 		isModal:false,//True the popup will be at modal. Not required.
	 * 		loadPageType:$.loadPageType.POPUP_AJAX,//Must be a constant comes from $.loadPageType. Required
	 * 		event:e,//If the loadPageType = $.loadPageType.POPUP_AJAX, a event parameter should be specified.
	 * 		href:url,//The URL which will be requested by Ajax or browser. You can put URL in <a href='URL'/a>,
	 * 				if "loadPageType = $.loadPageType.POPUP_HIDDEN_DIV", the URL should be the id for the hidden div after '#'(e.g."#testDivId").
	 * } .
	 */
	$.fn.loadPageWithIndicator = function(params){
		var _settings = $.extend({
									isModal:true,
									showCloseButton:false,//will not show a close button at right top of popup by default
									href : $(this).attr('href') ||  $(this).attr('action')//default value for href.
									//elementType:link,
								},params);
		if(!_settings.href || _settings.href == '' || _settings.href == 'underfined'){
			_internal.debug(" Please specify URl you want to load ");
			return false;
		}
		_internal.debug(" Going to request url :  " + _settings.href);
		
		if(_settings.loadPageType === _internal.loadPageType.GOTO_NEW_PAGE){
			
			_internal.loadPageWithIndicator(this, _settings, _settings.loadPageType);
			
		}else if(_settings.loadPageType === _internal.loadPageType.POPUP_AJAX){
			if(_settings.event){
				
				_internal.debug("Prevent default event");
				
				_settings.event.preventDefault();// default event should be prevented when we use popup ajax type
				
				_internal.loadPageWithIndicator(this, _settings, _settings.loadPageType);
				
			}else{
				_internal.debug("Please specify default event");
				return false;
			}
		}
		else if(_settings.loadPageType === _internal.loadPageType.POPUP_HIDDEN_DIV){
			$.nmManual(_settings.href,
					{modal:_settings.isModal,showCloseButton: _settings.showCloseButton});
		}else{
			_internal.debug(" Please specify load page type " );
			return false;
		}
		return true;
	};
	
});
