--Commercial.Stock

DEFINE EXTERNALID = 01001-38689739;


Delete from NH_VAL_4C_Commercial.Stock where sales_order_line_id in (
  Select Sales_Order_Line_Id From Nh_Val_4c_Commercial.Sales_Order_Line Where Sales_Order_Id In (
    Select Sales_Order_Id From Nh_Val_4c_Commercial.Sales_Order Where Patient_Id In (
      Select patient_id From Nh_Val_4c_Corerenal.Patient where external_id = '&&EXTERNALID.')
    and nh_status = 'IN PROGRESS'));

--Commercial.SALES_ORDER_LINE
Delete From Nh_Val_4c_Commercial.Sales_Order_Line Where Sales_Order_Id In (
  Select Sales_Order_Id From Nh_Val_4c_Commercial.Sales_Order 
    Where Patient_Id In (
      Select patient_id From Nh_Val_4c_Corerenal.Patient where external_id = '&&EXTERNALID.')
    and nh_status = 'IN PROGRESS');

--Commercial.SALES_ORDER
Delete From Nh_Val_4c_Commercial.Sales_Order 
  Where patient_id in (
    Select patient_id From Nh_Val_4c_Corerenal.Patient where external_id = '&&EXTERNALID.')
    and nh_status = 'IN PROGRESS';

Commit;